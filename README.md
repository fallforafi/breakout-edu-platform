
# Breakout EDU Platform
Made with ❤️ Laravel 5.

# What is Breakout EDU Platform? 
The new Breakout EDU Platform allows teachers to create accounts rather than having to enter the generic password every time they access a game. All of the 300+ games that were previously available are still available for free on the new platform. Learn how to access them all here. This includes all the great games from the Breakout EDU Team like Time Warp, The Dot, Totally Radical 80s Time Travel Adventure, and Dr. Johnson and User Generated games.

Website: https://platform.breakoutedu.com


## Installation Steps

### 1. Require the Package

After taking pull run the following command: 

```bash
composer update
```

### 2. Add the DB Credentials & APP_URL

Next make sure to create a new database and add your database credentials to your .env file:

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

You will also want to update your website URL inside of the `APP_URL` variable inside the .env file:

```
APP_URL=http://localhost:8000
```

> Only if you are on Laravel 5.4 will you need to [Add the Service Provider.]

### 3. Run The Migration

Lastly, Run the php artisan migration

```bash
php artisan migrate
```
