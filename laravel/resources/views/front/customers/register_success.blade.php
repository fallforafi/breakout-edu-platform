@extends('layout')

@section('content')

<section class="account-area">
    <div class="container">
        <div class="reg-area text-center p50 mb50">
            <div class="img text-center">
                <img src="{{asset('')}}/front/images/thankyou.jpg" alt="" />
            </div>
            <div class="hed">
                <h2>Thank you <span><?php echo $user->firstName ?> <?php echo $user->lastName ?></span></h2>                    <span class="main-head">YOU HAVE UNLOCKED FULL PLATFORM ACCESS.</span> 
            </div>
        </div>
    </div>
</section>
@endsection