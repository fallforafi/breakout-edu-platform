<?php
$titles = Config('params.titles');
?>
<div id="errors-register"></div>
<form method="POST" class="form" id='register-form' action="{{ url('register') }}">
    <input type="hidden" name="role_id" value="2">

    <div class="form-group p0 col-sm-4">
        <label for="fname">First Name</label>
        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" required="required" value="{{ old('firstName') }}">

    </div>
    <div class="form-group p0 col-sm-4">
        <label for="lname">Last Name</label>
        <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" required="required" value="{{ old('lastName') }}">
    </div>
    <div class="form-group p0 col-sm-4">
        <label for="fname">Title</label>
        <select class="form-control" id="title" name="title">
            <?php foreach ($titles as $title) { ?>
                <option value="<?php echo $title; ?>"><?php echo $title; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="clearfix"></div>
    <div class="form-group p0 col-sm-12">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Email Address *" required="required" value="{{ old('email') }}">
    </div>
    <div class="clearfix"></div>
    <div class="form-group p0 col-sm-6">
        <label for="pwd">Password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="Password *" required="required">
    </div>
    <div class="form-group p0 col-sm-6">
        <label for="pwd">Confirm Password</label>
        <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" required="required">
    </div>
    <div class="clearfix"></div>
    <div class="form-group p0 col-sm-12">
        <label for="accessCode">Platform Access Code (optional)</label>
        <input type="text" class="form-control" id="code" name="code" placeholder="Platform Access Code" />
    </div>
    <div class="clearfix"></div>
    <div class="has-access-code text-center p0 col-sm-12">
        <a href="https://store.breakoutedu.com/"><i>Don’t have a Platform Access Code?</i></a>
    </div>

    <div class="clearfix"></div>
    <div class="signup-option-or p0 col-sm-12">
        <div class="hed crossline">
            <h4>OR</h4>
            <hr>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="sm-area mb20 col-sm-12 p0">
        @include('front/common/social_login_buttons')  
    </div>

<div class="clearfix"></div>
<p style='text-align:center;'>If signin up with social accounts, you can provide your access code once logged in.  </p>
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >
    <div class="clearfix"></div>
    <button type="button" id="signup-submit" class="btn btn-primary w100 signup__submit">SIGN UP <i class="fa fa-angle-right"></i></button>
    <p>By signing ​up, you agree to our <a href="{{ url('terms') }}" target="_blank">Terms of Service</a> and <a href="{{ url('page/privacy') }}" target="_blank">Privacy Policy</a></p>
</form>
<script>
    $("#signup-submit").click(function () {

        var fname = $('#firstName').val();
        var lname = $('#lastName').val();
        var email = $('#email').val();
        var code = $('#code').val();
        var title = $('#title').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();

        $.ajax({
            url: "<?php echo url("register"); ?>",
            type: 'post',
            dataType: 'json',
            data: {email: email, password: password, title: title, firstName: fname, lastName: lname, password_confirmation: password_confirmation, code: code, _token: $("#_token").val()},
            beforeSend: function () {
                $('#preloader').show();
            },
            complete: function () {
                $('#preloader').hide();
            },
            success: function (response) {

                if (response.error == 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-sticky"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="cont"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors-register').html(errorsHtml).show();
                } else
                {
                    $("#_token").val(response.token)
                    $('#register-form').submit();
                    return false;
                    if (response.valid_code == 1) {
                        window.location = "<?php echo url('dashboard'); ?>";
                        return false;
                    }
                    window.location = "<?php echo url('accesscode'); ?>";
                    return false;

                }

            },
            error: function (xhr, status, response) {
            }
        });

    });

</script>
