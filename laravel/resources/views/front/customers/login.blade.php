@extends('signup_login')
<?php
$title = 'Login';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<?php
$required = 'required';
?>
<section class="inr-signup-area">
    <div class="container">

        <div class="login-box col-sm-6 bg-white pul-cntr overload mt0 mb30">

            <div class="logo text-center col-sm-12 mb20">
                <a href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a>
            </div>

            <div class="hed text-center"><h2>Log in and get to work</h2></div>
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-sticky">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <div class="cont">
                    <ul>
                        <li><strong>Whoops!</strong> There were some problems with your input.</li>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <div class="alert__icon"><span></span></div>
                </div>
            </div>
            @endif
            <div class="clearfix"></div>

            @include("front/customers/login_form")

        </div>

    </div>
</section>
@endsection