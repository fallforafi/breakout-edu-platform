@extends('layout')
<?php
$title = 'Edit Profile';
$description = "";
$keywords = "";
$required = "required";
$titles = Config('params.titles');
?>
@include('front/common/meta')
@section('content')

@include('front/common/breadcrumb')

@include('front/common/errors')
<section class="game-preview-area clrlist">
    <div class="container">
        <div class="hed underline">
            <h2>Edit Profile</h2>
        </div>

        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="game-preview-box col-sm-9">
            <div class="tab-content">
                <div id="profile" class="tab-pane fade active in">
                    <div class="holder overload col-sm-12 pt20">


                        @include('front.common.errors')
                        <div class="profile__dtl p15 overload">
                            <div class="row">
                                {!! Form::model($user, ['files' => true,'class' => 'form','url' => ['updateprofile'], 'method' => 'post']) !!}


                                <div class="form-group col-sm-4">
                                    <label for="fname">Title</label>
                                    <select class="form-control" id="title" name="title">
                                        <?php foreach ($titles as $title) { ?>
                                            <option <?php if ($user->title == $title) {
                                            echo "selected='selected'";
                                        } ?> value="<?php echo $title; ?>"><?php echo $title; ?></option>
<?php } ?>
                                    </select>
                                </div>

                                <div class="form-group col-sm-4">
                                    {!! Form::label('First Name') !!}
                                    {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
                                </div>
                                <div class="form-group col-sm-4 ">
                                    {!! Form::label('Last Name') !!}
                                    {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!}
                                </div>

                                <div class="form-group col-sm-4">
                                    {!! Form::label('email') !!}
                                    {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!}
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12  clrlist mb20">
                            <button type="submit" class="btn btn-primary "> <i class="fa fa-floppy-o"></i> Update</button>
                        </div>

                    </div>

                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>  
@endsection
