@extends('layout')
<?php
$title = 'Edit Profileaaa';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')

@include('front/common/breadcrumb')

<section class="game-preview-area clrlist">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="game-preview-box col-sm-9">
            <div class="tab-content">

                <div id="profile" class="tab-pane fade active in">

                    <div class="holder">
                        @include('front.common.errors')
                        <div class="profile__dtl col-sm-12">

                            {!! Form::model($user, ['files' => true,'class' => 'form','url' => ['updateprofile'], 'method' => 'post']) !!}
                            <div class="form-group col-sm-4">
                                {!! Form::label('First Name') !!}
                                {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
                            </div>
                            <div class="form-group col-sm-4 ">
                                {!! Form::label('Last Name') !!}
                                {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!}
                            </div>

                            <div class="form-group col-sm-12">
                                {!! Form::label('email') !!}
                                {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 fit__sub__reset clrlist">
                        <button type="submit" class="fit__submit"> UPDATE</button>
                    </div>

                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>  
@endsection
