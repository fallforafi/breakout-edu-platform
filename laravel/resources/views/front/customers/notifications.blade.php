@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="notifications" class="">
                        <div class="reviews-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-bell-o"></i> Notifications</h3>
                            </div>
                            <div class="reviews__box">
                                <div class="review__box__desc">
                                    <p>Once you have assigned or been assigned a task on TaskMatch, you will see a summary of your notifications here.</p>
                                </div>
                            </div>
                            <div class="noti-recent-area clrlist">
                                <div class="recent-box col-sm-6">
                                    <div class="recent__inr">
                                        <div class="recent__hdr__dboard">
                                            <h4>Recent activity on TaskMatch</h4>
                                        </div>
                                        <div class="recent__cont">
                                            <div class="recent__list">
                                                <ul>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                       <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                       <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="recent-box col-sm-6">
                                    <div class="recent__inr">
                                        <div class="recent__hdr__dboard">
                                            <h4>Notifications</h4>
                                        </div>
                                        <div class="recent__cont">
                                            <div class="recent__list">
                                                <ul>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="recent__act__cont">
                                                            <div class="recent__usr__img">
                                                                <img src="{{ asset('front/images/default.png') }}" alt="default picture" />
                                                            </div>
                                                            <p>Conor Lyons posted <a href="#">Hang my TV</a></p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection                            