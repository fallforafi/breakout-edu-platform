@extends('signup_login')
<?php
$title = 'Sign up';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<?php
$required = 'required';
?>
<section class="inr-signup-area">
    <div class="container">

        <div class=" col-sm-6 pul-cntr mt50 mb50 bg-white overload">


            <div class="logo text-center col-sm-12 mb10">
                <a href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a>
            </div>

            <div class="hed text-center"><h2>Create New Account</h2></div>

            <div class="text-center p0 m0">
                <!-- <div class="inr__signup__form__title">
                     <h3>Sign in using your social accounts</h3>
                 </div>
     
                 <div class="social-login">
                     @include('front/common/social_login_buttons')
                 </div>
     
                 <div class="inr__signup__form__title col-sm-12 mb10">
                     <h3 class="change-click">Or Sign up using your email</h3>
                 </div>-->
            </div>
            @include('front/common/errors')


            <div class="inr__signup__form">
                @include('front/customers/signupform')	


            </div>
        </div>  

    </div>
</section> 

<script>
    var googleUser = {};
    var startApp = function () {
        gapi.load('auth2', function () {
            // Retrieve the singleton for the GoogleAuth library and set up the client.
            auth2 = gapi.auth2.init({
                client_id: '<?php echo env('GOOGLE_CLIENT_ID') ?>',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
            });
            attachSignin(document.getElementById('google_login'));
            attachSignin(document.getElementById('google_signup'));
        });
    };

    function attachSignin(element) {
        auth2.attachClickHandler(element, {},
                function (googleUser) {
                    email = googleUser.getBasicProfile().getEmail();
                    firstName = googleUser.getBasicProfile().getGivenName();
                    lastName = googleUser.getBasicProfile().getFamilyName();
                    checkMember(email, firstName, lastName, 'google');
                    //verify('google', "https://plus.google.com/" + googleUser.getBasicProfile().getId(), googleUser.getBasicProfile().getId());
                }, function (error) {
            // alert(JSON.stringify(error, undefined, 2));
        });
    }

    startApp();
</script>
@endsection