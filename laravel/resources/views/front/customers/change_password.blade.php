@extends('layout')
<?php
$title = 'Change Password';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')

@include('front/common/breadcrumb')

<section class="game-preview-area clrlist">
    <div class="container">
	
		<div class="hed underline">
				<h2><i class="fa fa-unlock-alt"></i> Change your password</h2>
			</div>
		
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="game-preview-box col-sm-9">
            <div class="tab-content dboard-tab-height">                                          
                <div id="changePassword" class="fade in">
                    <div class="holder overload pt20">
					 
                        @include('front.common.errors')
                        <div class="pwd__box col-sm-12">
						
                                {!! Form::open(array( 'class' => 'form','url' => 'postchangepassword', 'method' => 'post')) !!}
                                <div class="row">
                                    <?php if (Auth::user()->password != '1') { ?>
                                        <div class="form-group col-sm-4">
                                            <label for="firstName">Current Password</label>
                                            {!! Form::password('old_password', ['class'=>'form-control','id'=>'currentPwd','placeholder'=>'Current Password', $required]) !!}

                                        </div>
                                    <?php } ?>

                                    <div class="form-group col-sm-4">
                                        <label for="lastName">New Password</label>
                                        {!! Form::password('password', ['class'=>'form-control','id'=>'newPwd','placeholder'=>'New Password', $required]) !!}
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label for="lastName">Verify Password</label>
                                        {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'verfPwd','placeholder'=>'Confirm Password', $required]) !!}

                                    </div>
                                    <div class="form-group save-pwd-btn0  col-sm-12">
                                        <button type="submit" class="btn btn-primary"value="" ><i class="fa fa-floppy-o"></i> Save Password</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                      
                            <div class="clearfix"></div>
                        </div>
						</div>
						
						
                    </div>
                </div>    
            </div>
        </div>
    </div>

</section>  
@endsection