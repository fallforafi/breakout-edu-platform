
<div class="inr__signup__form__title text-center">
    <h3>Sign in using your social accounts</h3>
</div>

<div class="clearfix"></div>

<div class="social-login text-center"  >
    @include('front/common/social_login_buttons')  
</div>
<div class="clearfix"></div>

<div class="inr__signup__form__title text-center">
    <h3 class="change-click"> Or with your account </h3>
</div>

<div class="clearfix"></div>
<div class="inr__signup__form"  >

    <form method="POST" class="form" action="{{ url('postLogin') }}">
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
        </div>
        <div class="form-group">
            <label for="pwd">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
        </div>
        <div class="checkbox mar__btm">

            <div class="pul-lft col-sm-8 p0">

                <div class="clrlist">
                    <ul>
                        <li class="pr10">
                            <label><input type="checkbox" class="checkbox" name="remember"> Keep me Logged In</label>
                        </li>
                        <li>
                            <a class="forgot__clr fw300" href="{{url('password/email')}}">Forgot your password?</a>
                        </li>
                        <li class="col-sm-12 mt10 p0 fw400"><span>Don't have an account?</span>
                            <a data-ng-non-bindable="" class="pl5" href="{{url('')}}/signup">Sign up here</a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="col-sm-4 p0">
                <button type="submit" class="btn btn-primary pul-rgt">Sign in</button>
            </div>

        </div>
    </form>
    <div class="clearfix"></div>
</div>
<script>

    function login() {
        var form = $(".form").serialize();

        $.ajax({
            type: "POST",
            url: '<?php echo url("postLogin"); ?>',
            data: form,
            dataType: 'json'
        });
    }
</script>