@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="dashboard">
                        <div class="dboard-area">
                            <div class="tab-cont-title text-center">
                                <h3>Dashboard <i class="fa fa-arrow-circle-o-right"></i></h3>
                            </div>
                            <div class="dboard__links" id="swiperSlider">
                                <div class="dboard__links__title text-center">
                                    <h3>What would you like to do today {{ $user->firstName}}?</h3>
                                </div>
                                <div class="swiper-container dontfly s1">
                                    <div class="swiper-wrapper">
                                        @foreach($categories as $row)
                                        <div class="swiper-slide">
                                            <a href="#">
                                                <div class="links__images">
                                                    @if($row->image == '')
                                                    <img src="{{ asset('front/images/noimage.jpg') }}" alt="task" />   
                                                    @else
                                                    <img src="{{ asset('uploads/categories/'.$row->image) }}" alt="task">
                                                    @endif
                                                </div>
                                                <div class="dboard__links__name valigner">
                                                    <div class="valign">
                                                        <h4>{{$row->name}}</h4>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                    <!-- Add Pagination -->
                                    <div class="swiper-pagination hidden"></div>

                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next swiper-button-next1"></div>
                                    <div class="swiper-button-prev swiper-button-prev1"></div>

                                </div>	
                            </div>
                            <div class="tab-cont-title tab-cont-title--2 text-center">
                                <h3>My Tasks Summary</h3>
                            </div>
                            <div class="task-summ-area">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#taskRunning">Task Running</a></li>
                                    <li><a data-toggle="tab" href="#tasksPosted">Tasks Posted</a></li>
                                </ul>

                                <div class="tab-content task-tab-content" id="tasks_summary">


                                </div>
                            </div>
                            <div class="tab-cont-title tab-cont-title--2 text-center">
                                <h3>Is your profile complete?</h3>
                            </div>
                            <div class="comp-prof-area clrlist">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <div class="complete__box">
                                                <div class="complete__what">
                                                    <h3>Account</h3>
                                                    <div class="progress-radial progress-{{$account}} setsize">
                                                        <div class="overlay setsize">
                                                            <i class="fa fa-bank"></i>
                                                            <p>{{$account}}%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="complete__box">
                                                <div class="complete__what">
                                                    <h3>Profile</h3>   
                                                    <div class="progress-radial progress-{{ isset($profile)?$profile->profile:'0'}} setsize">
                                                        <div class="overlay setsize">
                                                            <i class="fa fa-user"></i>
                                                            <p>{{ isset($profile)?$profile->profile:'0'}}%</p>
                                                        </div>
                                                    </div>
                                                </div>                                                                       
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="complete__box">
                                                <div class="complete__what">
                                                    <h3>Payments</h3>
                                                    <div class="progress-radial progress-{{$payment}} setsize">
                                                        <div class="overlay setsize">
                                                            <i class="fa fa-money"></i>
                                                            <p>{{$payment}}%</p>
                                                        </div>
                                                    </div>
                                                </div>                                              
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="noti-recent-area clrlist">
                                <div class="recent-box col-sm-6">
                                    <div class="recent__inr">
                                        <div class="recent__hdr__dboard">
                                            <h4>Recent activity on TaskMatch</h4>
                                        </div>
                                        <div class="recent__cont">
                                            <div class="recent__list">
                                                <ul id="activities">


                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="recent-box col-sm-6">
                                    <div class="recent__inr">
                                        <div class="recent__hdr__dboard">
                                            <h4>Notifications</h4>
                                        </div>
                                        <div class="recent__cont">
                                            <div class="recent__list">
                                                <ul id="notifications" >
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $(".setsize").each(function () {
            $(this).height($(this).width());
        });
    });
    $(window).on('resize', function () {
        $(".setsize").each(function () {
            $(this).height($(this).width());
        });
    });
    function getTasksSummary()
    {
        $.ajax(
                {
                    url: "<?php echo url("dashboard/tasks-summary"); ?>",
                    type: 'get',
                    dataType: 'html',
                    success: function (result) {
                        //alert(result);
                        $("#tasks_summary").html(result);
                        $("#taskRunning").addClass('tab-pane active');
                    }
                });
    }

    function getActivities()
    {
        $.ajax(
                {
                    url: "<?php echo url("dashboard/activities"); ?>",
                    type: 'get',
                    dataType: 'html',
                    success: function (result) {
                        //alert(result);
                        $("#activities").html(result);
                        //$("#taskRunning").addClass('tab-pane active');

                    }
                });
    }

    function getNotifications()
    {
        $.ajax(
                {
                    url: "<?php echo url("dashboard/notifications"); ?>",
                    type: 'get',
                    dataType: 'html',
                    success: function (result) {
                        //alert(result);
                        $("#notifications").html(result);
                        //$("#taskRunning").addClass('tab-pane active');

                    }
                });
    }

    $(document).ready(function (e) {
        getTasksSummary();
        getActivities();
        getNotifications();
    });
    var swiper1 = new Swiper('.s1', {
        pagination: '.swiper-pagination',
        slidesPerView: '3',
        centeredSlides: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next1',
        prevButton: '.swiper-button-prev1',
        spaceBetween: 0,
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
        breakpoints: {
            1024: {slidesPerView: 3, spaceBetween: 40},
            768: {slidesPerView: 3, spaceBetween: 30},
            640: {slidesPerView: 1, spaceBetween: 20},
            320: {slidesPerView: 1, spaceBetween: 10}
        }
    });
    var swiper2 = new Swiper('.s2', {
        pagination: '.swiper-pagination',
        slidesPerView: '4',
        centeredSlides: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next2',
        prevButton: '.swiper-button-prev2',
        spaceBetween: 15,
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
        breakpoints: {
            1024: {slidesPerView: 3, spaceBetween: 40},
            768: {slidesPerView: 3, spaceBetween: 30},
            640: {slidesPerView: 1, spaceBetween: 20},
            320: {slidesPerView: 1, spaceBetween: 10}
        }
    });

</script>
@endsection                            