@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="reviews" class="">
                        <div class="reviews-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-star-half"></i> Reviews</h3>
                            </div>
                            <div class="reviews__box">
                                <div class="review__box__desc">
                                    <p>Once you have completed a task on TaskMatch, you will see a summary of your reviews here.</p>
                                </div>
                                <div class="prof__avg__review col-sm-offset-2 col-sm-4">
                                    <h4>Average reviews</h4>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="prof__review__fm">
                                        <p>from 0 review(s)</p>
                                    </div>
                                </div>
                                <div class="prof__avg__review col-sm-5">
                                    <h4>Summary</h4>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge">0</span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge">0</span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge">0</span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge">0</span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge">0</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection                            