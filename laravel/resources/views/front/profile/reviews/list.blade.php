<?php if (count($model) > 0) { ?>
    @foreach($model as $row)
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                @include('front/common/profile_picture')
                <small><a href="{{url('user/')}}/{{$row->user_id}}"> {{$row->firstName}} {{$row->lastName}}</a></small>
                <small>
                    {{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>

            </div>                                            
        </div>
        <div class="row">
            <div class="col-md-8">
                <span>{{$row->comment}}</span>                                                     
            </div>
            <div class="col-md-4">
                <span class="prof__rate clrlist">
                    <ul>
                        @for($i=1;$i<=$row->rating;$i++)
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        @endfor
                        @for($i=5;$i>$row->rating;$i--)
                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                        @endfor
                    </ul>                                            
                </span>   
            </div>
        </div>  
    </div>
    @endforeach
<?php } else { ?>
    <div class="alert now-closed">No reviews added yet</div>
<?php } 