@extends('public_profile')
<?php
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
<?php
$reviews = $user_reviews['reviews'];
$average = $user_reviews['average'];
$ratings = $user_reviews['ratings'];
?>
@section('content')

<style type="text/css">
    .wrap {
        height: 206px;
        overflow: hidden;
        margin: auto;
        cursor: -webkit-grab;

    }
</style>
<section class="public-area">
    <div class="container">
        <div class="public__box col-sm-12">
            <div class="public__inr">
                <div class="pub__cvr">
                    <div class="wrap" id='wrap_div'>
                        @if($user->coverImage == '')
                        <img src="{{ asset('front/images/cover-default.jpg') }}" id="cvrPhoto" alt="cover photo" />
                        @else
                        <img src="{{ asset('/uploads/users/cover/')}}/<?php echo $user->coverImage; ?>" id="cvrPhoto" alt="cover photo" />
                        @endif


                    </div>
                    <div class="text-center form-group collapse out" aria-hidden="0" id="reposition" >
                        <button class="btn btn-success" onclick="saveCoverPosition();" ><i class="fa fa-save"></i>Save</button>
                        <a class="btn btn-danger" href="{{url('user')}}/{{$user_id}}" ><i class="fa fa-undo"></i>Cancel</a>
                    </div>
                    <?php if (isset(Auth::user()->id) && Auth::user()->id == $user_id) { ?>
                        <div class="cvr__upload">
                            <div class="cvr__upload__btn">
                                {!! Form::open(array('files' => true,'class' => 'form','url' => 'changecoverimage', 'method' => 'post', 'id' => 'coverForm')) !!}
                                <div class="text-center form-group" aria-hidden="0" id="select1">
                                    <input type="file" id="imgInp1" name="coverImage"/>
                                    <button ><i class="fa fa-camera"></i>Upload Cover Photo</button>


                                    <button type="button" data-toggle="collapse" data-target="#reposition"   onclick="reposition()" ><i class="fa fa-arrows-v"></i>Reposition Cover Photo</button>
                                </div>


                                <div class="text-center form-group " aria-hidden="0" id="upload1" style="display:none;">

                                    <button class="btn btn-primary btn-file" onclick="document.getElementById('coverForm').submit()">Upload</button>
                                    <button type="button" class="btn btn-default btn-flat" id="cancel1">Cancel</button>

                                </div>
                                {!! Form::close() !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                <?php } ?>
                <div class="pub__prof">
                    <div class="prof__pic__box">
                        @if($user->image == '')
                        <img src="{{ asset('front/images/default.png') }}" id="profilePic" alt="profile picture" />   
                        @else
                        <img src=" {{ asset('/uploads/users/profile')}}/<?php echo $user->image; ?>" id="profilePic"  alt="profile picture" />
                        @endif

                    </div>
                    <?php if (isset(Auth::user()->id) && Auth::user()->id != $user->id) { ?>
                        <div class="">
                            <button data-toggle="modal" class="btn btn-success btn-flat pull-right" data-target="#reqQuote">Request a quote</button><br><br>
                            <div class="dropdown pull-right">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">More options <i class="glyphicon glyphicon-chevron-down"></i></button>
                                <ul class="dropdown-menu">
                                    <li><a class="report" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('report/user/' . $user->id); ?>">Report user as inappropriate</a></li>                                   
                                </ul>
                            </div>  
                            <div class="modal fade" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" style="color: red">Alert</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Are you sure do you want to report this?</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <a id="closemodal" class="btn btn-danger pull-left" href="">Yes</a>
                                            <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                    <?php } ?>
                    <div id="reqQuote" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content" id="reqBody">
                                <div class="modal-header">   
                                    <button type="button" class="close" onclick="pageReload()">&times;</button>
                                    <h3 class="modal-title text-center">Request a quote</h3>
                                </div>
                                <div class="modal-body">
                                    <div id="reqErrors"></div>
                                    <h4 class="text-center">Invite to make an offer on your task</h4>
                                    <form id="requestForm" name="requestForm">
                                        <br><div class="form-group text-center">
                                            <select class="form-control" name="type_id" id="taskPosted">
                                                @if(count($tasks_request) > 0)
                                                <option value="">Choose your posted task</option>  
                                                @foreach($tasks_request as $row)                                            
                                                <option value="{{$row->id}}">{{$row->title}}</option>
                                                @endforeach
                                                @endif                                         
                                            </select>
                                        </div><br>
                                        <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="notify_to" id="notify_to" value="<?php echo $user->id; ?>">
                                        <button disabled="disabled" type="button" class="btn btn-success btn-block text-center" id="test1"><h4>Request a quote</h4></button>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-content" id='reqSuccess' style="display:none;">
                                <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h3 class="modal-title text-center">Success!</h3>

                                </div >
                                <div class='modal-body'>
                                    <p>You successfully requested a quote!</p>
                                    <br>
                                    <button type="button" class="btn btn-success btn-block text-center" onclick="pageReload()"><h4>OK</h4></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                        <div class="prof__pic__upload">
                                                <input type="file" onchange="profilePicture(this);" />
                                                <button><i class="fa fa-camera"></i><span>Upload Profile Picture</span></button>
                                            </div>-->
                </div>
                <div class="clearfix"></div>

                <div class="prof__text">
                    <div class="prof__usr col-sm-4">
                        <div class="prof__usr__title">
                            <h3>{{ $user->firstName}} {{$user->lastName}}</h3>
                        </div>
                        <div class="prof__usr__noti">
                            <h5 class="profileText ng-binding"><i class="fa fa-globe" aria-hidden="true"></i> {{ $user->location }}</h5>
                            <h4><i class="fa fa-info-circle" aria-hidden="true"></i> {{ $user->tagline }}</h4> 
                        </div>
                        <div class="prof__join">
                            <p>Joined: {{date('l, F d, Y',strtotime($user->created_at))}}</p>
                        </div>
                        <div class="prof__rate clrlist">
                            <ul>
                                @for($i=1;$i<=$average;$i++)
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                @endfor
                                @for($i=5;$i>$average;$i--)
                                <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                    <div class="prof__tasks text-center col-sm-4">
                        <div class="prof__tasks__inr col-sm-4">
                            <h3 class="prof__tasks__count">{{count($tasks_posted)}}</h3>
                            <p class="prof__tasks__status">Posted</p>
                        </div>
                        <div class="prof__tasks__inr col-sm-4">
                            <h3 class="prof__tasks__count">{{count($completed)}}</h3>
                            <p class="prof__tasks__status">Completed</p>
                        </div>
                        <div class="prof__tasks__inr col-sm-4">
                            <h3 class="prof__tasks__count">{{@count($reviews)}}</h3>
                            <p class="prof__tasks__status">Reviews</p>
                        </div>
                    </div>
                    <div class="prof__frnd__rate col-sm-4">
                        <div class="prof__mutual">
                            <h5>

                                <?php if (isset(Auth::user()->id) && Auth::user()->id != $user_id) { ?>
                                    <i>
                                        <img src="{{ asset('front/images/mutual-friend-icon.png') }}" alt="mutual friend" />
                                    </i>
                                    {{count($mutualFriends)}} mutual friend(s)
                                <?php } ?>
                            </h5>
                        </div>
                        <div class="prof__pos__rating">
                            <h4>
                                <i class="fa fa-thumbs-o-up"></i>
                                <strong>100%</strong>
                                positive rating
                            </h4>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>

                <div class="prof__abt">
                    <div class="prof__hed__sect p0 col-sm-12">
                        <div class="prof__sect__title col-sm-4">
                            <h4>About {{ $user->firstName}} {{$user->lastName}}</h4>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="prof__sect__edit col-sm-4">
                            <?php if (isset(Auth::user()->id) && Auth::user()->id == $user_id) { ?>
                                <a href="{{ url('profile') }}"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="prof__bio prof__sect__title__inr col-sm-offset-1 col-sm-4">
                        <h5><i>Bio</i></h5>
                        <p>{{ $user->about }}</p>
                    </div>
                    <div class="prof__verf prof__sect__title__inr clrlist col-sm-offset-2 col-sm-4">
                        <h5><i>Verifications</i></h5>
                        <div class="prof__verf__sm">
                            <small>Social media:</small>
                            <ul>
                                <li><a href="#"><i class="fa fa-twitter <?php
                                        if (key_exists('twitter', $accounts)) {
                                            echo "blue";
                                        }
                                        ?>"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram <?php
                                        if (key_exists('instagram', $accounts)) {
                                            echo "blue";
                                        }
                                        ?>"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus <?php
                                        if (key_exists('google', $accounts)) {
                                            echo "red";
                                        }
                                        ?>"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin <?php
                                        if (key_exists('linkedin', $accounts)) {
                                            echo "blue";
                                        }
                                        ?>"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook <?php
                                        if (key_exists('facebook', $accounts)) {
                                            echo "blue";
                                        }
                                        ?>"></i></a></li>
                            </ul>
                        </div>
                        <div class="prof__verf__acct prof__verf__sm">
                            <small>Acccount:</small>
                            <ul>
                                <li><a href="#"><i class="fa fa-phone <?php
                                        if ($user->phone != "") {
                                            echo "blue";
                                        }
                                        ?>"></i></a></li>
                                <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>	
                <div class="clearfix"></div>

                <div class="prof__skills">
                    <div class="prof__hed__sect p0 col-sm-12">
                        <div class="prof__sect__title col-sm-4">
                            <h4>Skills</h4>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="prof__sect__edit col-sm-4">
                            <?php if (isset(Auth::user()->id) && Auth::user()->id == $user_id) { ?>
                                <a href="{{ url('profile') }}"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="prof__skills__main clrlist col-sm-offset-1 col-sm-10">
                        <div class="prof__skl__inr prof__sect__title__inr col-sm-3">
                            <h5><i>Education</i></h5>
                            <ul>
                                @foreach($education as $row)
                                <li>
                                    <a>{{ $row->name }}</a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                        <div class="prof__skl__inr prof__sect__title__inr col-sm-3">
                            <h5><i>Languages</i></h5>
                            <ul>
                                @foreach($language as $row)
                                <li>
                                    <a>{{ $row->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="prof__skl__inr prof__sect__title__inr col-sm-3">
                            <h5><i>Work</i></h5>
                            <ul>
                                @foreach($skill as $row)
                                <li>
                                    <a>{{ $row->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="prof__skl__inr prof__sect__title__inr col-sm-3">
                            <h5><i>Transportation</i></h5>
                            <ul>
                                @foreach(Config::get('params.transportTypes') as $key => $value) 
                                @if(in_array($key,$transport))
                                <li>
                                    <a>{{ $value }}</a>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="prof__review">
                    <div class="prof__hed__sect p0 col-sm-12">
                        <div class="prof__sect__title col-sm-4">
                            <h4>Reviews</h4>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <!--<div class="prof__sect__edit col-sm-4">
                                <a href="dashboard.php#editProfile"><i class="fa fa-pencil"></i></a>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="prof__avg__review col-sm-offset-1 col-sm-4">
                        <h4>Average reviews</h4>
                        <div class="prof__rate clrlist">
                            <ul>
                                @for($i=1;$i<=$average;$i++)
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                @endfor
                                @for($i=5;$i>$average;$i--)
                                <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                @endfor
                            </ul>
                        </div>
                        <div class="prof__review__fm">
                            <p>from {{count($reviews)}} review(s)</p>
                        </div>
                    </div>
                    <div class="prof__avg__review col-sm-offset-2 col-sm-4">
                        <h4>Summary</h4>
                        <div class="prof__rate clrlist">
                            <ul>
                                <li>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    </ul>
                                </li>
                                <li class="prof__rate__graph"></li>
                                <li class=""><span class="badge"><?php echo!empty($ratings[5]) ? count($ratings[5]) : 0; ?></span></li>
                            </ul>
                        </div>
                        <div class="prof__rate clrlist">
                            <ul>
                                <li>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                    </ul>
                                </li>
                                <li class="prof__rate__graph"></li>
                                <li class=""><span class="badge"><?php echo!empty($ratings[4]) ? count($ratings[4]) : 0; ?></span></li>
                            </ul>
                        </div>
                        <div class="prof__rate clrlist">
                            <ul>
                                <li>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                    </ul>
                                </li>
                                <li class="prof__rate__graph"></li>
                                <li class=""><span class="badge"><?php echo!empty($ratings[3]) ? count($ratings[3]) : 0; ?></span></li>
                            </ul>
                        </div>
                        <div class="prof__rate clrlist">
                            <ul>
                                <li>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                    </ul>
                                </li>
                                <li class="prof__rate__graph"></li>
                                <li class=""><span class="badge"><?php echo!empty($ratings[2]) ? count($ratings[2]) : 0; ?></span></li>
                            </ul>
                        </div>
                        <div class="prof__rate clrlist">
                            <ul>
                                <li>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                    </ul>
                                </li>
                                <li class="prof__rate__graph"></li>
                                <li class=""><span class="badge"><?php echo!empty($ratings[1]) ? count($ratings[1]) : 0; ?></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="prof__review">
                    <div class="prof__hed__sect p0 col-sm-12">
                        <div class="prof__sect__title col-sm-4">
                            <form id="filter">
                                <label>Sort by: </label>
                                <label class="btn btn-default"> <input type="radio" name="sort" id="sort" value="created" checked="checked">Created</label>
                                <label class="btn btn-default"><input type="radio" name="sort" id="sort" value="rating">Rating</label>
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                            </form>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <!--<div class="prof__sect__edit col-sm-4">
                                <a href="dashboard.php#editProfile"><i class="fa fa-pencil"></i></a>
                        </div>-->
                        <div class="clearfix"></div>
                        
                    </div>
                    <div class="col-sm-12">
                        <div id="reviews_listing">
                            
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<span id="output" style="display:none;"></span>
<script>
    $(document).ready(function () {
        reviewListing();
    });
    $("form").change(function (e) {
        reviewListing();
        e.preventDefault();
    });
    function reviewListing() {
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('reviews/listing'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                $('#reviews_listing').html(response).show();
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
<script>
    $("#taskPosted").change(function () {
        $('#test1').removeAttr('disabled');
    });
    $('#test1').click(function () {
        var button = $(this);
        button.attr('disabled', 'disabled');
        setTimeout(function () {
            $('#test1').html('<h4>Request a quote</h4>');
        }, 900);
//    $('#test1').html('<img src="{{ asset("front/images/spinner_2.gif") }}" height="100px" width="100px">');
        $('#test1').html('<h4>Waiting..</h4>');
        saveRequest();
    });
    jQuery('.report').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
    function pageReload() {
        location.reload();
    }
    function saveRequest() {
        var formdata = $("#requestForm").serialize();
        $('#errors').html("").hide();
        $.ajax({
            url: "<?php echo url('quote/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-sticky"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#reqErrors').html(errorsHtml).show();
                } else
                {
                    $('#reqBody').html("").hide();
                    $('#reqSuccess').show();
                }
                // window.location = "<?php //echo url('task/create');                                            ?>";
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
<script type="text/javascript">

    function reposition() {
        $('#wrap_div').removeClass('pub__cvr__pic').addClass('wrap');
        wrap();
    }

    function wrap() {

        $('.wrap').imagedrag({
            input: "#output",
            position: "<?php echo $user->coverPosition ?>",
            attribute: "html"
        });
    }

    function saveCoverPosition() {

        var position = $("#output").text();
        $.ajax({
            url: "<?php echo url(""); ?>/changecoverposition",
            type: 'get',
            dataType: 'json',
            data: {user_id: <?php echo $user_id; ?>, position: position},
            success: function (result) {
                $('#reposition').removeClass('in').addClass('out');
                //  $('#wrap_div').removeClass('wrap').addClass('pub__cvr__pic');
            }
        });
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#cvrPhoto').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp1").change(function () {
        readURL1(this);
        document.getElementById('imgInp1').addEventListener('change', function () {
            var style = this.value === '' ? 'block' : 'none';
            var upload = this.value === '' ? 'none' : 'block';
            document.getElementById('select1').style.display = style;
            document.getElementById('upload1').style.display = upload;
        });
    });

    $('#cancel1').on('click', function () {
        document.getElementById('upload1').style.display = 'none';
        document.getElementById('select1').style.display = 'block';
        $('#cvrPhoto').attr('src', '{{ asset(' / uploads / users / cover')}}/<?php echo $user->coverImage; ?>');
    }
    );
    $(document).ready(function () {
        wrap();
    });

</script>

@endsection
