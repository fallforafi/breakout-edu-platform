@extends('layout')

@section('content')
<section class="inr-intro-area pt100">
    <div class="container">	
        <div class="row">
            <div class="forget-fom col-md-6 col-md-offset-3">
                <div class="contact-form">
                    <h2 class="title text-center">Reset Password</h2>

                    @if (session('success'))
                    <div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<div class="cont">
							<ul>
								<li>{{ session('success') }}</li>
							</ul>
							
							<div class="alert__icon"><span></span></div>
						</div>
                    </div>
                    @endif

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<div class="cont">
                        <ul>
							<li>    <strong>Whoops!</strong> There were some problems with your input.</li>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
						<div class="alert__icon"><span></span></div>
						</div>
                    </div>
                    @endif

                    <form class="text-center" role="form" method="POST" action="{{ url('/reset') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}">
                        </div>

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                        </div>
						
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection