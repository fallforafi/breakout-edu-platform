@extends('layout')
<?php
$title = 'Platform Access Code';
$description = 'Breakout EDU is the immersive learning games platform &#124; It\'s time for 
something different';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<?php
$required = "required";
?>
<section class="accesscode-fom-area">
    <div class="container">

        {!! Form::open(array( 'class' => 'form','url' => 'checkcode', 'method' => 'post')) !!}


        @include('front.common.errors')
        <div class="accesscode-fom form-group p10">


            <div class="input-group w70">

                <span class="input-group-addon clrhm">
                    <h2>Access Code</h2>
                </span>

                {!! Form::text('code',null, ['class'=>'form-control','id'=>'code','placeholder'=>'Enter access code here....', $required]) !!}

                <span class="input-group-addon pt0 pl10 pb0 pr0">
                    <input type="submit" class="btn btn-primary" value="Enter" />
                </span>

            </div>
            {!! Form::close() !!}

        </div>
</section>


<section class="accesscode-cont-area broken-image-default00">
    <div class="container">

        <div class="access__cont">
            <h4 class="blue">Don't have Platform Access Yet?</h4>

            <h1>The Breakout EDU Platform</h1>

            <h4>
                100s of Subject Pack games<br>
                Build and share your own Digital Games<br>
                View student progress
            </h4>
        </div>

        <a href="http://store.breakoutedu.com/" class="btn btn-primary access-btn">Obtain Platform Access Code</a>


        <div class="access__img">
            <img src="{{asset('')}}/front/images/accesscode-img.jpg" alt="" />
        </div>

    </div>
</section>

@endsection
