<div class="subj__search col-sm-12">
    <form class="form" role="form" id="filter" action="{{ url('query') }}">
        <div class="input-group">
            <span class="input-group-addon">
                <button class="clrbtn" type="submit"> 
                    <i class="glyphicon glyphicon-search"></i>       
                </button>
            </span>
            <input type="text" autocomplete="off" class="form-control" id="q" name="q" placeholder="Search games" value="{{$q}}">
        </div>
    </form>

    <div class="col-sm-12 p0" id="search_listing" style="display:none"></div>

</div>
<script>
    $("#filter").change(function (e) {
        searchListing();
        e.preventDefault();
    });

    $("#q").keyup(function (e) {
        searchListing();
    });

    function searchListing() {
        $('#search_listing').hide();
        var formdata = $("#filter").serialize();
        var q = $('#q').val();
        $.ajax({
            url: "<?php echo url('search'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                if (q !== '') {
                    $('#search_listing').html(response);
                    if (response !== '') {
                        $('#search_listing').show();
                    }

                } else {
                    $('#search_listing').hide();
                }

            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>