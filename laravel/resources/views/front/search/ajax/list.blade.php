@if(count($model)>0)
<div class="search_listinner">

    <table class="table search_table">
        <tbody>   
            @foreach($model as $row)
            <tr>
                <td>
                    <a class="icon" href="{{url('game/')}}/{{$row->key}}"><img src="<?php echo path();?>uploads/games/thumbnail/{{$row->image}}"></a>
                    <span><a href="{{url('game/')}}/{{$row->key}}" class="txt">{{$row->title}} ({{$row->name}}) @if($row->tags!="") tags: {{$row->tags}}  @endif</a></span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
@else
<div class="search_listinner">
    <span>No result for your search...</span>
</div>
@endif

