@extends('layout')

<?php
$title = 'Breakout EDU';
$description = 'Breakout EDU is the immersive learning games platform &#124; It\'s time for 
something different';
$keywords = '';
?>

@include('front/common/meta')
@section('content')
<section class="subj-area clrlist">
    <div class="container">
        <div class="hed underline">
            <h2>Search Results</h2>
        </div>
        @include('front/search/form')
        <section class="prod-area clrlist">
            @if($q != '' && count($model) > 0)
            <div class="container">
                @foreach($model as $row)
                <div class="prod-box col-sm-4 img-cntr">
                    <div class="prod__inr">
                        <div class="prod__img">
                            <img src="<?php echo path(); ?>uploads/games/thumbnail/{{$row->image}}" alt="{{$row->title}}" />
                        </div>
                        <div class="prod__title">
                            <a href="{{url('game')}}/{{$row->key}}"><h4>{{$row->title}}</h4></a>
                        </div>
                    </div>
                </div>
                @endforeach          
            </div>
            <div class="container">
                <?php // echo $model->appends(['q' => $q])->render(); ?>           
            </div>
            @else 
            <center><h3>No result for your search...</h3></center>
            @endif
        </section>
    </div>
</section>
@endsection
