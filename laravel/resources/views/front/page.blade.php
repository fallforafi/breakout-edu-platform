@extends('layout')
@section('content')
<?php
$title = $model->title;
$description = $model->teaser;
$keywords = $model->keywords;
?>
@include('front/common/meta')
<?php echo $model->body;?>
@endsection
