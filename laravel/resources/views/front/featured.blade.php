@extends('layout')
<?php
$title = 'Breakout EDU';
$description = 'Breakout EDU is the immersive learning games platform &#124; It\'s time for 
something different';
$keywords = '';
$bgs = array('red', 'yellow', 'green', 'pink', 'blue', 'gray', 'orange', 'teal', 'maroon', 'brown', 'purple');
?>
@include('front/common/meta')
@section('content')

<section class="games-area clrlist preloader--off">
    <div class="container">
        <div class="hed underline">
            <h2>DIGITAL Games</h2>
        </div>
    </div>
    <div class="container0">
        <div class="feat-games ">
            <div class="container">
                <div class="feat__hed col-sm-12">
                    <div class="hed underline">
                        <h3>Featured DIGITAL  Games</h3>
                    </div>
                </div>
                <?php $i = 0; ?>

                @foreach($featuredMiniGames as $minigame)

                <?php
                $c = rand(0, 10);
                ?>

                <div class="feat-box col-sm-6  <?php echo "val_" . $i; ?>">
                    <div class="feat__inr">
                        <div class="feat__img bg-<?php echo $bgs[$c]; ?>">
                            <?php if(trim($minigame->image)!=""){ ?>
                            <img src="{{asset('')}}uploads/games/thumbnail/{{$minigame->image}}" alt="{{$minigame->title}}" />
                            <?php } ?>
                        </div>
                       
                        <div class="feat__title">
                            <h4>{{$minigame->title}}</h4>
                        </div>
                        <div class="feat__bar">
                            <ul>
                                <li class="feat__game__share rotateY360--hover"><a href="#" data-id="{{$minigame->id}}" data-toggle="modal" class="mini-game" data-target="#shareGame"><i class="fa fa-share-alt"></i>SHARE GAME</a></li>

                                <li class="feat__game__start rotateY360--hover"><a href="{{url('game/digital')}}/{{$minigame->key}}" ><i class="fa fa-play"></i>START GAME</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="actions-area text-center col-sm-12">
                    <a href="{{ url('/') }}" class="btn btn-primary">
                        <i class="fa fa-angle-left"></i> BACK
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CANCEL A PO MODAL -->
@include('front/common/share_popup')
@endsection
