<h3>Selected Combination</h3>
<div class="tabs--number__select tabs--number__select--two key-control-area">
    <?php
    $limit = 5;

    if ($layout == 'text') {
        $limit = 10;
    }
    $readonly = 'readonly="readonly"';
    if ($layout == 'text' || $layout == 'number') {
        //$readonly='';
    }
    for ($i = 1; $i <= $limit; $i++) {
        if ($layout == 'text' || $layout == 'number') {
            if ($i == 1) {
                $readonly = '';
            } else {
                $readonly = 'readonly="readonly"';
            }
        }
        ?>
        <div class="number__select-box cols-5">
            <div class="number__select-inr number__select-inr--comb <?php echo isset($answers[$i]) ? 'has-value' : ''; ?>" id="comb-<?php echo $i; ?>">
                <h2><input <?php echo $readonly; ?> style="text-transform:uppercase;" maxlength="1" class="form-control" type='text' name='answers[]' id='{{$layout}}_answers_<?php echo $i; ?>' value='<?php echo isset($answers[$i]) ? $answers[$i] : ''; ?>' /></h2>
            </div>
        </div>
    <?php } ?>
    <div class="clearfix"></div>
</div>

<script>

    jQuery(".key-control-area .form-control").keyup(function (index) {
        if ($(this).val() != "") {

            $(this).val($(this).val().toUpperCase())

            jQuery(this).attr("readonly", true);
            jQuery(this)
                    .closest(".number__select-box")
                    .next()
                    .find("input")
                    .attr("readonly", false)
                    .focus();

            jQuery(this)
                    .closest(".number__select-inr")
                    .addClass("has-value");
        }

    });

    jQuery(".key-control-area .form-control").keydown(function (e) {
        if (e.keyCode == 8) {
            if ($(this).val() != "") {
                jQuery(this).val("");
            } else {

                jQuery(this).attr("readonly", true).val("");

                jQuery(this)
                        .closest(".number__select-box")
                        .prev()
                        .find("input")
                        .attr("readonly", false)
                        .focus().val("");
                //jQuery(this).attr("readonly", true);

                jQuery(this)
                        .closest(".number__select-inr")
                        .removeClass("has-value");
            }
        }
    });
</script>