@if(count($gameLocks)>0)
<ul>
    <?php
    $i = 1;
    // echo $model->gameType;
    ?>
    @foreach($gameLocks as $lock)
    <?php
    if ($i == 1) {
        $lock_id = $lock->id;
        $title = $lock->lockSetup;
        $layout = $lock->lockSetup;
        $src = asset('/front/locks/filled/') . '/' . $lock->lockType . '.png';
    } else {
        $src = asset('/front/locks/blanked/') . '/' . $lock->lockType . '.png';
    }
    ?>
    <li><img alt="{{$lock->id}}" src="<?php echo $src ?>" id='lock_image_{{$i}}' > <i class="fa fa-arrow-right blue"></i> </li>
        <?php
        $i++;
        ?>
    @endforeach
</ul>
@endif