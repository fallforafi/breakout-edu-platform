<script src="https://apis.google.com/js/platform.js" async defer></script>
<div class="signup__connect signup__connect--twitter p0 col-sm-12">
    <div id="library_errors"></div>
    <div id="library_success"></div>
</div>

<div class="clearfix"></div>
<div class="shareicon count-child">

    <?php
    if (isset(Auth::user()->id)) {
        ?>
        <!--
            <div class="signup__connect signup__connect--twitter0">
                <a href="#_" data-id='<?php echo $model->id; ?>' class="add-to-library bg-green" ><i class="fa fa-plus"></i>Add to Library</a>
            </div>
        -->
        <?php if (Auth::user()->role_id != 2 && $model->isFeatured == 1) {
            ?>
            <div class="signup__connect signup__connect--twitter0">
                <a href="#_" data-id='<?php echo $model->id; ?>' class="copy-game bg-green" ><i class="fa fa-copy"></i>Copy Game</a>
            </div>
        <?php } ?>
    <?php } ?>


    <div class="signup__connect signup__connect--twitter">
        <a href="https://twitter.com/share?url=<?php echo url('/game/digital/') . '/' . $model->key; ?>" data-text="check out this game" target="_blank" ><i class="fa fa-twitter"></i>Twitter</a><script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
    <div class="signup__connect signup__connect--fb">
        <a id="shareBtn" onclick="shareBtn();"><i class="fa fa-facebook"></i>Facebook</a>
<!--        <div data-href="<?php echo url('/game/digital/') . '/' . $model->key; ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo url('/game/digital/') . '/' . $model->key; ?>"><i class="fa fa-facebook"></i>Facebook</a></div>-->
    </div>
    <div class="signup__connect signup__connect--google">
        <a href="https://plus.google.com/share?url=<?php echo url('/game/digital/') . '/' . $model->key; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><i class="fa fa-google-plus0"><img src="{{asset('')}}/front/images/google-logo.png"></i> Google</a>
    </div>
	<div class="signup__connect  bg-green singup__classroom" >
		<a><i class="fa fa-users"></i> Classroom</a>
		<div class="g-sharetoclassroom" data-size="32" data-url="<?php echo url('/game/digital/') . '/' . $model->key; ?>"></div>
	</div>

</div>

<script>
    function shareBtn() {
        FB.ui({
            method: 'share',
            display: 'popup',
            picture: '<?php echo asset('') ?>/front/images/social-share.png',
            mobile_iframe: true,
            hashtag: "#<?php echo str_replace(' ', '', $model->title); ?>",
            quote: "<?php echo $model->description; ?>",
            href: "<?php echo url('/game/digital') . '/' . $model->key; ?>",
        }, function (response) {});
    }
</script>
<script>
    $(".add-to-library").click(function (event) {
        var id = $(this).attr("data-id");
        $('#library_errors').html('').hide();
        $('#library_success').html('').hide();

        $.ajax({
            url: "<?php echo url(""); ?>/add-to-library",
            type: 'get',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                ajax: 1,
                id: id,
            },
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#library_errors').html(errorsHtml).show();
                } else
                {
                    $('#library_success').html("<div class='alert alert-success'>" + response.message + "</div>").show();
                }
            }
        });
    });
</script>
<script>
    $(".copy-game").click(function (event) {
        var id = $(this).attr("data-id");
        $('#library_errors').html('').hide();
        $('#library_success').html('').hide();

        $.ajax({
            url: "<?php echo url(""); ?>/game/copy/" + id,
            type: 'post',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                ajax: 1,
                id: id,
                _token: '<?php echo csrf_token() ?>',
            },
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#library_errors').html(errorsHtml).show();
                } else
                {
                    $('#library_success').html("<div class='alert alert-success'>" + response.message + "</div>").show();
                }
            }
        });
    });
</script>
<script>
    jQuery(document).ready(function () {

        //myGroup = '[class*="-group"]';
        jQuery(".count-child").each(function () {
            countChildGroup = jQuery(this);
            var countChildLength = jQuery(countChildGroup).children("div").length;
            var totalCountChild = jQuery(countChildGroup).addClass(
                    "count-child-total-" + countChildLength
                    );

//  countChildGroup.children("h2").children("small").html(countChildLength);
        });

    });
</script>
