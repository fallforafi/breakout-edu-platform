@if(count($gameLocks)>0)
<span class="game-type">
    <?php
    $i = 1;
    ?>
</span>
<ul>
    @foreach($gameLocks as $lock)
    <?php
    if ($i == 1) {
        $lock_id = $lock->id;
        $title = $lock->lockSetup;
        $layout = $lock->lockSetup;
        //$currentLockId = $lock->id;
        $src = asset('/front/locks/blanked/') . '/' . $lock->lockType . '.png';
        //$src = asset('/front/locks/filled/') . '/' . $lock->lockType . '.png';
    } else {
        $src = asset('/front/locks/blanked/') . '/' . $lock->lockType . '.png';
    }
    ?>
    <li><a href="javascript:void(0);" id="{{$i}}" data-number="{{$i}}" data-layout="{{$lock->lockType}}" data-title="{{$lock->lockType}}" data-lock-id="{{$lock->id}}" data-id="{{$model->id}}" class="{{$model->gameType}}" ><img src="<?php echo $src; ?>" id='lock_image_{{$i}}'></a></li>
    <?php
    $i++;
    ?>
    @endforeach
</ul>
<div class="individual__intro">
    <div class="end-screen-box text-center mt30 mb30 bg-lock bg-cvr">
        <div class=" clrhm">
            <div class="hd white"><h3>Please select your lock from above.</h3></div>
            <!--<div class="btns text-center mt20">
                <a onclick="validate(<?php echo $model->id ?>, 1);" class="btn btn-primary">FINISH</a>
            </div>-->
            <div class="mix-icons pul-rgt">
				<img src="{{asset('')}}/front/images/logo.png" alt="" />
			</div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@endif
