<div class="game__prev__img__main ofv">
    <?php if ($model->lockSetup == 'image') { ?>
        <div class="game__prev__img  zoomized">
            <img src="<?php echo path(); ?>uploads/gameslocks/{{$model->image}}" alt="{{$model->title}}" />

            <button><i class="fa fa-search-plus"></i></button>

        </div>
        <a href="<?php echo path(); ?>uploads/gameslocks/{{$model->image}}" target="_blank"><i class="fa fa-share-square-o"></i> Open in new window</a>
    <?php } elseif ($model->lockSetup == 'text') { ?>
        <?php echo htmlspecialchars_decode(html_entity_decode($model->text)); ?>
    <?php } elseif ($model->lockSetup == 'video') { ?>
        <?php echo html_entity_decode($model->videoUrl); ?>
    <?php } ?>

</div>
<?php
if($model->story!=""){
?>
<div class="game__prev__img__main ofv">
    <?php echo $model->story; ?>
</div>
<?php
}
?>
<script>


    /**zoomized**/
    jQuery(document).ready(function () {
        var zoomizedIcon = jQuery(".zoomized button")
        jQuery(".zoomized").click(function () {
            jQuery(this).toggleClass("is-active");
        });
    });
    /**./zoomized**/


</script>

<?php
$layout = $model->lockType;
?>
@include('front/mini_games/lock_layouts/'.$layout)

<div class="next-prev-step col-sm-12 p0">

    <?php
    if ($game->gameType == 'connected') {
        if ($paginate->currentPage() > 1) {
            ?>
            <a href="javascript:void(0);" data-number="{{$page-1}}" data-layout="{{$model->lockType}}" data-title="{{$model->lockType}}" data-lock-id="{{$model->id}}" data-id="{{$game->id}}" class="prev-step  btn btn-default {{$game->gameType}}">
                <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>PREVIOUS STEP</a>
            <?php
        }
        if ($paginate->currentPage() < $paginate->total()) {
            ?>
            <a href="javascript:void(0);" data-number="{{$page+1}}" data-layout="{{$model->lockType}}" data-title="{{$model->lockType}}" data-lock-id="{{$model->id}}" data-id="{{$game->id}}" class="next-step btn btn-default pul-rgt  {{$game->gameType}}"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>NEXT</a>
            <?php
        } elseif ($paginate->currentPage() == $paginate->total()) {
            ?>
            <a href="javascript:void(0);" data-lock-id="{{$model->id}}" data-layout="{{$model->lockType}}"  data-type="{{$game->gameType}}"  data-id="{{$game->id}}" class="next-step  btn btn-default ml20 pul-rgt finish_{{$game->gameType}}"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>FINISH</a>
            <?php
        }
    } else {
        ?>
        <a href="javascript:void(0);" data-number="{{$page}}" data-layout="{{$model->lockType}}" data-title="{{$model->lockType}}" data-lock-id="{{$model->id}}" data-id="{{$game->id}}" class="prev-step  btn btn-default {{$game->gameType}}-back"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>Go Back</a>

        <a href="javascript:void(0);" data-number="{{$page}}" data-layout="{{$model->lockType}}" data-title="{{$model->lockType}}" data-lock-id="{{$model->id}}" data-id="{{$game->id}}" class="next-step btn btn-default pul-rgt {{$game->gameType}}-submit"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>SUBMIT</a>

    <?php } ?>
</div>
<input type="hidden"  name="_token" id="_token" value="{{ csrf_token() }}">
<script>
<?php
if ($game->gameType == 'connected') {
    ?>
        $(document).ready(function () {
            $("#lock_image_" +<?php echo $page; ?>).attr('src', '<?php echo asset('/front/locks/filled/') . '/' . $model->lockType . '.png'; ?>');


        });
    <?php
}
?>
    $('.individual-back').click(function () {
        $("#locks_individual").show();
        $("#lock_layout").html('');
    });

    $('.individual-submit').click(function () {
        var title = $(this).attr('data-title');
        var layout = $(this).attr('data-layout');
        var game_id = $(this).attr('data-id');
        var number = $(this).attr('data-number');
        c_lock_id = $(this).attr('data-lock-id');

        saveAnswers();

        $.ajax({
            url: "<?php echo url(""); ?>/check-answer",
            type: 'get',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                layout: layout,
                game_id: game_id,
                lock_id: c_lock_id,
                answers: $.param(a)
            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {

                if (response.error === 1) {
                    $("#lock_image_" + number).attr('src', '<?php echo asset('/front/locks/blanked/'); ?>/' + layout + '.png');
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable alert-sticky"><div class="cont"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors').html(errorsHtml).show();
                } else {
                    $("#lock_image_" + number).attr('src', '<?php echo asset('/front/locks/opened/'); ?>/' + layout + '.png');
                    validate(game_id, 0);
                    $("#locks_individual").show();
                    $("#lock_layout").html('');
                }
            }
        });
    });

    $('.connected').click(function () {

        var title = $(this).attr('data-title');
        var layout = $(this).attr('data-layout');
        var game_id = $(this).attr('data-id');
        var number = $(this).attr('data-number');
        c_lock_id = $(this).attr('data-lock-id');
        c_layout = layout;

        checkAnswer(game_id, layout, title, number);
        $('.connected').on('click');
    });

    $('.finish_connected').click(function () {

        var title = $(this).attr('data-title');
        var layout = $(this).attr('data-layout');
        var game_id = $(this).attr('data-id');
        var number = $(this).attr('data-number');
        c_lock_id = $(this).attr('data-lock-id');
        c_layout = layout;
        checkAnswer(game_id, layout, title, 'end');
    });
</script>




<div class="alert alert-danger alert-sticky" id="alertRefresh">
    <button type="button" class="close">×</button>
    <div class="cont">
        <ul>
            <li>Are you sure you would like to leave this page? <br/>
                All progress will be lost.</li>
        </ul>
        <div class="alert__icon"><span></span></div>
    </div>
</div>

<script>
var mY = 0;
$('body').mousemove(function(e) {
    if (e.pageY < mY) {
        console.log('From Bottom');
		$("body").addClass("from-bottom").removeClass("from-top");
    } else {
        console.log('From Top');
		$("body").addClass("from-top").removeClass("from-bottom");
    }
    mY = e.pageY;
});

jQuery('#alertRefresh').addClass("hidden");

	
/*
    jQuery('body').mouseleave(function () {
		if($('body').hasClass("from-bottom")){
			jQuery('#alertRefresh').removeClass("hidden");
		}
    });
*/

    jQuery('#alertRefresh .close').on("click", function () {
        jQuery('#alertRefresh').addClass("hidden");
    });

    jQuery(document).keydown(function (e) {
        if (e.keyCode == 82 && e.ctrlKey) {
            e.returnValue = false;
			return false;
        }
        if (e.keyCode == 116) {
            e.returnValue = false;
			return false;
        }
    });
</script>

