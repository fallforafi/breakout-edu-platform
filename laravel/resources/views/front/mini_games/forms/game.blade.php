{!! Form::model($model, ['files' => true,'class' => 'form','url' => ['game/'.$action], 'method' => 'post']) !!}
<div class="game-info col-sm-12">

    @include('front/common/errors')
    <div class="form-group p0 col-sm-12">
        {!! Form::label('Title of Game') !!}
        {!! Form::text('title', null , array('id' => 'title','class' => 'form-control','placeholder' => 'Title of Game',$required) ) !!}
    </div>
    <div class="form-group p0 col-sm-12">
        {!! Form::label('Description of Game') !!}
        {!! Form::text('description', null , array('id' => 'description','class' => 'form-control','placeholder' => 'Write your Game description',$required) ) !!}
    </div>
    <div class="row mb20 no-transition ">
        <div class="game-type-lft selectme col-sm-6 checker-area <?php if ($model->gameType == 'individual') { ?> in-view is-active <?php } ?>">
            <div class="type__ind__locks chooseGameTypeClass" id="indi-type-locks">
                <label for="gameType-i"><ul>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                    </ul>
                    <h3>Individual Locks</h3>
                    <h4>Puzzles can be solved in any order</h4></label>
                {!! Form::radio('gameType', 'individual', null, array('id'=>'gameType-i')) !!}
            </div>
        </div>
        <div class="game-type-rgt selectme col-sm-6 checker-area <?php if ($model->gameType == 'connected') { ?> in-view is-active <?php } ?>">
            <div class="type__ind__locks type__conn__locks chooseGameTypeClass" id="conn-type-locks">
                <label for="gameType-c">
                    <ul>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                        <li><i class="fa fa-lock"></i></li>
                    </ul>
                    <h3>Connected Locks</h3>
                    <h4>Sequential - One puzzle leads to another</h4>
                </label>
                {!! Form::radio('gameType', 'connected', null, array('id'=>'gameType-c')) !!}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {!! Form::hidden('type',$type , null, array('id'=>'type')) !!}
    <div id="locksetupstory">
        <h3 >END OF GAME REVEAL</h3>
        <div class="lock-setup-story">
            <div >
                <p>Select a piece of content to show the player when they complete the game.</p>
                <div class="row">
                    <ul class="nav nav-pills">
                        <li class="lock-setup-box text-center col-sm-3 <?php if ($model->endScreenType == 'breakout') { ?> active <?php } ?>">
                            <a class="endScreen" data-id='breakout'  data-toggle="pill" href="#endScreenBreakout">
                                <div class="lock-setup__inr">
                                    <div class="lock-setup__icon">
                                        <i class="fa"><img src="{{asset('')}}/front/images/logoicon.png"></i>
                                    </div>
                                    <div class="lock-setup__title">
                                        <h2>Breakout EDU<br />CONGRATS MESSAGE</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="lock-setup-box text-center col-sm-3 <?php if ($model->endScreenType == 'image') { ?> active <?php } ?>">
                            <a class="endScreen" data-id='image'  data-toggle="pill" href="#endScreenImageTab">
                                <div class="lock-setup__inr">
                                    <div class="lock-setup__icon">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <div class="lock-setup__title">
                                        <h2>Image</h2>

                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="lock-setup-box text-center col-sm-3 <?php if ($model->endScreenType == 'text') { ?> active <?php } ?>">
                            <a class="endScreen" data-id='text'  data-toggle="pill" href="#endScreenTab">
                                <div class="lock-setup__inr">
                                    <div class="lock-setup__icon">
                                        <i class="fa fa-font"></i>
                                    </div>
                                    <div class="lock-setup__title">
                                        <h2>Text</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="lock-setup-box text-center col-sm-3 <?php if ($model->endScreenType == 'video') { ?> active <?php } ?>">
                            <a class="endScreen" data-id='video' data-toggle="pill" href="#endScreenVideoTab">
                                <div class="lock-setup__inr">
                                    <div class="lock-setup__icon">
                                        <i class="fa fa-video-camera"></i>
                                    </div>
                                    <div class="lock-setup__title">
                                        <h2>Video Embed Code</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <script type="text/javascript">

                        $(document).ready(function () {
                            $('.endScreen').click(function () {

                                var endScreen = $(this).attr('data-id');
                                $('#endScreenType').val(endScreen);

                            });
                        });
                    </script>
                    <div class="lock-setup__tabs col-sm-12">
                        <div class="tab-content">

                            <div id="endScreenBreakout" class="lock-setup <?php if ($model->endScreenType == 'breakout') { ?> active in <?php } ?>"></div>

                            <div id="endScreenImageTab" class="lock-setup__tabs--img tab-pane fade <?php if ($model->endScreenType == 'image') { ?> active in <?php } ?>">

                                <div class="lock-setup__img--inr text-center is-active">
                                    <h3>Drag and Drop Image File Here</h3>
                                    <div class="lock-setup__img--box">
                                        <?php if ($model->endScreenType == 'image' && file_exists(public_path('uploads/mini_games/endscreens/' . $model->endScreen))) { ?>
                                            <img src="<?php echo path(); ?>uploads/mini_games/endscreens/thumbnail/<?php echo $model->endScreen ?>" alt="lock image">
                                        <?php } else { ?>
                                            <img src="{{asset('')}}/front/images/upload-icon.png" alt="lock image">
                                        <?php } ?>
                                    </div>
                                    <div class="lock-setup__img--file">
                                        <button>SELECT A FILE FROM YOUR COMPUTER</button>
                                        <!--{!! Form::file('image', null,array('id'=>'lockImageInput','onchange'=>'endScreenImage(this);')) !!}-->
                                        {!! Form::file('image', null,array('id'=>'imgInp')) !!}
                                    </div>
                                </div>

                                <div class="lock-setup__img--has-image">
                                    <div class="lock-setup__img__remove col-sm-2">
                                        <h3><span class="remove-image">×</span>Remove</h3>
                                    </div>
                                    <div class="lock-setup__img__preview col-sm-6 col-sm-offset-1">
                                        <img src="#" id="blah" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div id="endScreenTab" class="tab-pane <?php if ($model->endScreenType == 'text') { ?> active <?php } ?>">
                                <div class="lock-setup__text--box text-left">

                                    <div class="form-group">
                                        <label for="endScreenTextNeeded">Enter a message to display when the game is completed.</label>
                                        <!-- Include stylesheet -->
                                        <!-- Create the editor container -->
                                        <textarea  id="text" style="width:100%;display:none;" name="text"></textarea>
                                        <div id="textdiv" style="height:220px;display:block;">
                                            <?php if ($model->endScreenType == 'text') { echo html_entity_decode($model->endScreen); } ?></div>

                                        <script>
                                            var toolbarOptions = {
                                                handlers: {
                                                    // handlers object will be merged with default handlers object
                                                    'link': function (value) {
                                                        if (value) {
                                                            var href = prompt('Enter link (include the http://):');
                                                            this.quill.format('link', href);
                                                        } else {
                                                            this.quill.format('link', false);
                                                        }
                                                    }
                                                }
                                            };
                                            var quill = new Quill('#textdiv', {
                                                theme: 'snow', // Specify theme in configuration
                                                height: 400,
                                                modules: {
                                                    toolbar: toolbarOptions
                                                }
                                            });

                                        </script>

                                        <script>
<?php
if ($model->endScreenType == 'text') {
    ?>
                                                $('#text').val('<?php echo html_entity_decode($model->endScreen); ?>');
<?php } ?>
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div id="endScreenVideoTab" class="tab-pane fade 
                                 <?php if ($model->endScreenType == 'video') { ?> active in <?php } ?>">
                                <div class="lock-setup__text--box text-left">
                                    <div class="form-group">
                                        <label for="endScreenTextNeeded">Enter Embed Video Here</label>
                                        {!! Form::textarea('video', null , array('id' => 'video','class' => 'form-control','placeholder' => 'Enter the embed video code here') ) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="form-group game__add__timer hidden-checkbox p0 col-sm-12">
        <label for="addTimer">ADD TIMER</label>
        {!! Form::checkbox('hasTimer', 1, null, ['id' => 'toggle']) !!}

        <div class="checkylbl">
            <label for="toggle"></label>
        </div>
        <div class="game__timer__box">
            {!! Form::text('time', null , array('id' => 'time','class' => 'form-control','placeholder' => 'MM:SS') ) !!}
        </div>
    </div>
    
    <?php if(isset($model->id)){ ?>
        <button type="submit" name="save" id="save_exit" class="btn next-step" value="exit" ><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>SAVE AND EXIT</button>
    <?php } ?>
    <button type="submit" name="save" id="save_next" value="next" class="next-step pul-rgt"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>NEXT STEP</button>

</div>
{!! Form::hidden('endScreenType', null , array('id' => 'endScreenType') ) !!}
{!! Form::close() !!}

<script type="text/javascript">

    $(".pul-rgt").click(function () {
        var text = quill.container.firstChild.innerHTML;
        $('#text').val(text);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".lock-setup__img--file input").change(function () {
        readURL(this);
        $(".lock-setup__img--has-image").addClass("show");
    });

    $(".remove-image").click(function () {
        $(".lock-setup__img--has-image").removeClass("show");
    });
</script>