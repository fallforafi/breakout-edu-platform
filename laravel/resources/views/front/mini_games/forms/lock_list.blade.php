<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#selectLockType" class="collapsed">SELECT LOCK TYPE</a>
        </h4>
    </div>
    <div id="selectLockType" class="select-lock-type collapse in" style="height: 0px;">
        <div class="panel-body">
            <div class="row">
                <div class="row two-options" id="lockOptions">


                    @if(count($gameLocks)>0)
                    <div class="game__prev__locks col-sm-12 mb10" >
                        <ul>
                            @foreach($gameLocks as $lock)
                            <li><a href="#" data-layout="{{$lock->lockType}}" data-title="{{$lock->lockType}}" data-id="{{$lock->id}}" class="editlock" ><img src="{{asset('')}}/front/locks/filled/<?php echo $lock->lockType ?>.png"></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    @endif

                    <ul class="nav nav-pills" id="newLock">
                        <li class="game-select-lock cols-5">
                            <a data-layout="text" data-title="Text Lock" class='select-lock' data-toggle="pill" href="#selectLockText">
                                <div class="select__lock__inr">
                                    <div class="select__lock__img">
                                        <img src="{{asset('')}}/front/images/select-lock1.png" alt="select lock">
                                    </div>
                                    <div class="select__lock__title">
                                        <h2>Text Lock</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="game-select-lock cols-5">
                            <a data-layout="number" data-title="Number Lock" class='select-lock' data-toggle="pill" href="#selectLockNumber">
                                <div class="select__lock__inr">
                                    <div class="select__lock__img">
                                        <img src="{{asset('')}}/front/images/select-lock2.png" alt="select lock">
                                    </div>
                                    <div class="select__lock__title">
                                        <h2>Number Lock</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="game-select-lock cols-5">
                            <a data-layout="directional" data-title="Directional Lock" class='select-lock' data-toggle="pill" href="#selectLockDirection">
                                <div class="select__lock__inr">
                                    <div class="select__lock__img">
                                        <img src="{{asset('')}}/front/images/select-lock3.png" alt="select lock">
                                    </div>
                                    <div class="select__lock__title">
                                        <h2>Directional Lock</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li  class="game-select-lock cols-5">
                            <a data-layout="shape" data-title="Shape Lock" data-toggle="pill" class='select-lock' href="#selectLockShape">
                                <div class="select__lock__inr">
                                    <div class="select__lock__img">
                                        <img src="{{asset('')}}/front/images/select-lock4.png" alt="select lock">
                                    </div>
                                    <div class="select__lock__title">
                                        <h2>Shape Lock</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="game-select-lock cols-5">
                            <a data-layout="color" data-title="Color Lock" data-toggle="pill" class='select-lock' href="#selectLockColor">
                                <div class="select__lock__inr">
                                    <div class="select__lock__img">
                                        <img src="{{asset('')}}/front/images/select-lock5.png" alt="select lock">
                                    </div>
                                    <div class="select__lock__title">
                                        <h2>Color Lock</h2>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="select-lock__tabs col-sm-12 p0">
                    <div class="tab-content" id="lock_layout">
                    </div>
                    <div class="tab-content" id="lock_layout_finish" <?php if (count($gameLocks) > 0) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> >
                        <div class="next-prev-step">
                            <div id="message" style="display: none;"class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                            </div>

                            <a class="btn btn-default next-step" href="{{url('edit-digital-game')}}/{{$game_id}}" class="prev-step"><i class="fa fa-chevron-circle-left"></i>PREVIOUS STEP</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-12">
                        <div id="errors" style="display: none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
