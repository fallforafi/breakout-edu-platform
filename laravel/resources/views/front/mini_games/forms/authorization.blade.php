{!! Form::model(null, ['class' => 'form','url' => ['game/digital/checkcode'], 'method' => 'post']) !!}
<div class="col-sm-12" id='errors' >

</div>
<div class="form-group">
    {!! Form::label('Name') !!}

    {!! Form::text('name', null , array('id' => 'name','class' => 'form-control') ) !!}
    {!! Form::hidden('game_id', $model->id , array('id' => 'game_id','class' => 'form-control') ) !!}
</div>
<div class="form-group">
    {!! Form::label('Play Code') !!}
    {!! Form::text('code', null , array('id' => 'code','class' => 'form-control','style' => 'text-transform:uppercase') ) !!}
</div>
{!! Form::hidden('embed', $embed , array('id' => 'embed') ) !!}
{!! Form::hidden('slug', $slug , array('id' => 'slug') ) !!}
<div class="form-group">
    <button type="submit" class="next-step btn-white"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>START GAME</button>
</div>
{!! Form::close() !!}
<script>

    $(document).on("submit", "form", function (event)
    {
        event.preventDefault();
        var url = $(this).attr("action");
        var slug = $('#slug').val();
        $.ajax({
            url: url,
            type: $(this).attr("method"),
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable alert-sticky"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="cont"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors').html(errorsHtml).show();
                } else
                {
                    if(response.embed==1){
                        window.location="<?php echo url('game/embed')?>/"+slug;
                    }else{
                        window.top.location="<?php echo url('game/digital')?>/"+slug;
                    }
                    
                }
            },
            error: function (xhr, status, response) {
            }
        });

    });
</script>