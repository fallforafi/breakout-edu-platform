<?php
$required = "required";
?>
<div id="locksetupstory">
    <h3 class="tab-content"> LOCK SETUP / STORY</h3>
    <div class="lock-setup-story">
        <p>Provide any text you want your players to see along with the content below.</p>
        <div class="row0 ">
            <div class="tab-content">
                <textarea class="form-control bdr-pro" id="story" name="story" style="width:100%;" rows="4">{{$model->story}}</textarea>
            </div>
        </div>
    </div>

    <h3 >LOCK CLUE</h3>
    <div class="lock-setup-story">
        <div >
            <p>Select a content-type below to create a clue for this lock.</p>
            <div class="row">
                <ul class="nav nav-pills">
                    <li class="lock-setup-box text-center col-sm-4 <?php if ($model->lockSetup == 'image') { ?> active <?php } ?>">
                        <a class="lockSetup" data-id='image'  data-toggle="pill" href="#lockSetupImageTab">
                            <div class="lock-setup__inr">
                                <div class="lock-setup__icon">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="lock-setup__title">
                                    <h2>Image</h2>

                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="lock-setup-box text-center col-sm-4 <?php if ($model->lockSetup == 'text') { ?> active <?php } ?>">
                        <a class="lockSetup" data-id='text'  data-toggle="pill" href="#lockSetupTab">
                            <div class="lock-setup__inr">
                                <div class="lock-setup__icon">
                                    <i class="fa fa-font"></i>
                                </div>
                                <div class="lock-setup__title">
                                    <h2>Text</h2>

                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="lock-setup-box text-center col-sm-4 <?php if ($model->lockSetup == 'video') { ?> active <?php } ?>">
                        <a class="lockSetup" data-id='video' data-toggle="pill" href="#lockSetupVideoTab">
                            <div class="lock-setup__inr">
                                <div class="lock-setup__icon">
                                    <i class="fa fa-video-camera"></i>
                                </div>
                                <div class="lock-setup__title">
                                    <h2>Video Embed Code</h2>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
                <script type="text/javascript">

                    $(document).ready(function () {
                        $('.lockSetup').click(function () {

                            var lockSetup = $(this).attr('data-id');
                            $('#lockSetup').val(lockSetup);

                        });
                    });
                </script>


                <div class="lock-setup__tabs col-sm-12">
                    <div class="tab-content">
                        <div id="lockSetupImageTab" class="lock-setup__tabs--img tab-pane fade <?php if ($model->lockSetup == 'image') { ?> active in <?php } ?>">

                            <div class="lock-setup__img--inr text-center is-active">
                                <h3>Drag and Drop Image File Here</h3>
                                <div class="lock-setup__img--box">
                                    <?php if ($model->lockSetup == 'image' && file_exists(public_path('uploads/gameslocks/' . $model->image))) { ?>
                                        <img src="<?php echo path(); ?>uploads/gameslocks/thumbnail/<?php echo $model->image ?>" alt="lock image">
                                    <?php } else { ?>
                                        <img src="{{asset('')}}/front/images/upload-icon.png" alt="lock image">
                                    <?php } ?>
                                </div>
                                <div class="lock-setup__img--file">
                                    <button>SELECT A FILE FROM YOUR COMPUTER</button>
                                    <!--{!! Form::file('image', null,array('id'=>'lockImageInput','onchange'=>'lockSetupImage(this);')) !!}-->
                                    {!! Form::file('image', null,array('id'=>'imgInp')) !!}
                                </div>
                            </div>

                            <div class="lock-setup__img--has-image">
                                <div class="lock-setup__img__remove col-sm-2">
                                    <h3><span class="remove-image">×</span>Remove</h3>
                                </div>
                                <div class="lock-setup__img__preview col-sm-6 col-sm-offset-1">
                                    <img src="#" id="blah" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="lockSetupTab" class="tab-pane fade <?php if ($model->lockSetup == 'text') { ?> active in <?php } ?>">
                            <div class="lock-setup__text--box text-left">

                                <div class="form-group">
                                    <label for="lockSetupTextNeeded">Enter the text that will be needed to open the lock.</label>



                                    <!-- Include stylesheet -->

                                    <!-- Create the editor container -->
                                    <textarea  id="text" style="width:100%;display:none;" name="text"></textarea>
                                    <div id="textdiv" style="height:220px;display:block;"><?php echo html_entity_decode($model->text); ?></div>

                                    <script>
                                        var toolbarOptions = {
                                            handlers: {
                                                // handlers object will be merged with default handlers object
                                                'link': function (value) {
                                                    if (value) {
                                                        var href = prompt('Enter link (include the http://):');
                                                        this.quill.format('link', href);
                                                    } else {
                                                        this.quill.format('link', false);
                                                    }
                                                }
                                            }
                                        };

                                        var quill = new Quill('#textdiv', {
                                            theme: 'snow', // Specify theme in configuration
                                            height: 400,
                                            modules: {
                                                toolbar: toolbarOptions
                                            }
                                        });

                                    </script>

                                    <script>
                                        $('#text').val('');
                                    </script>
                                    <!--
                                                                        {!! Form::text('text', null , array('id' => 'text','class' => 'form-control','placeholder' => 'Enter the answer text here') ) !!}
                                    -->

                                </div>
                            </div>
                        </div>
                        <div id="lockSetupVideoTab" class="tab-pane fade 
                             <?php if ($model->lockSetup == 'video') { ?> active in <?php } ?>
                             ">
                            <div class="lock-setup__text--box text-left">
                                <div class="form-group">
                                    <label for="lockSetupTextNeeded">Enter Embed Video Here</label>
                                    {!! Form::textarea('videoUrl', null , array('id' => 'videoUrl','class' => 'form-control','placeholder' => 'Enter the embed video code here') ) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="next-prev-step">
                <a class="btn next-step" href="{{url('choose-lock-types')}}/{{$game_id}}" class="prev-step"><i class="fa fa-chevron-circle-left"></i>CANCEL</a>

                <?php if (isset($model->id)) { ?>
                    <a class="btn btn-danger confirm-delete" data-id="<?php echo $model->id ?>"  ><i class="fa fa-trash"></i>Delete Lock</a>
                <?php } ?>
                <div class="pul-rgt">
                    <button onclick="setClicked('save_lock_add')" type="submit" name="save_lock_add" id="save_lock_add" class="btn next-step" ><i class="fa fa-chevron-circle-right"  ></i>SAVE AND ADD ANOTHER LOCK</button>
                    <button onclick="setClicked('save_lock_exit')" type="submit" name="save_lock_exit" class="btn next-step" id="save_lock_exit" ><i class="fa fa-chevron-circle-right"  ></i>SAVE LOCK AND EXIT</button>
                </div>

                <!--div class="pul-rgt"><button onclick="setClicked('save_lock_add')" type="submit" name="save_lock_add" id="save_lock_add" class="btn next-step" ><i class="fa fa-chevron-circle-right"  ></i>SAVE AND ADD ANOTHER LOCK</button><button onclick="setClicked('save_lock_exit')" type="submit" name="save_lock_exit" class="btn next-step" id="save_lock_exit" ><i class="fa fa-chevron-circle-right"  ></i>SAVE LOCK AND EXIT</button></div-->
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>You are about to delete a lock.</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn btn-danger">Yes</a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-info">No</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var id = "";
    $('.confirm-delete').on('click', function (e) {
        id = $(this).data('id');
        $('#delete-modal').data('id', id).modal('show');
    });

    $('#btnYes').click(function () {
        window.top.location = "<?php echo url("delete-lock") ?>/" + id;
        $('#myModal').modal('hide');
    });

</script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".lock-setup__img--file input").change(function () {
        readURL(this);
        $(".lock-setup__img--has-image").addClass("show");
    });

    $(".remove-image").click(function () {
        $(".lock-setup__img--has-image").removeClass("show");
    });
</script>
