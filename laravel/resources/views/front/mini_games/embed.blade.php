@extends('embed_layout')
@section('content')

<style>
    #preloader{ display:none !important;}
</style>
<section class="game-preview-area clrlist">
    <div class="container">
        <div class="hed underline">
            <h2>{{$model->title}}</h2>
            <div class="desc lead text-center">{{$model->description}}</div>
        </div>
        <div class="game-preview-box col-sm-12 play-area">
            <div class="game__prev__inr">
			  <div class="col-sm-12 stopwatch-area" id="stopwatch" >
                <?php
                if ($model->hasTimer == 1) {
                    ?>
                    
                        <div id="load-bar-frame">
                            <div id="load-bar"></div>
                        </div>
						
						 <div class="stopwatch l0">
                            <i class="fa fa-clock-o"></i><span class="stopwatch__timer" id="time"><?php echo $model->time; ?></span>
                        </div>
						
                        <div class="stopwatch r0">
                            <i class="fa fa-clock-o"></i><span class="stopwatch__timer" id="time"><?php echo $model->time; ?></span>
                        </div>
                    
                <?php } ?>
			   </div>
              
				<div class="game__prev__locks" id="locks_{{$model->gameType}}" >
                    @include('front/mini_games/includes/'.$model->gameType)
                </div>

                <div id='lock_layout' ></div>
                <div class="clearfix"></div>
                <div id='errors' ></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div id="loader"><div class="preloader-area">
            <div class="preloader-area__status">
                <i class="fa fa-spin"><img src="{{asset('front/images/preloading.png')}}" alt="" /></i>
            </div>
        </div></div>
</section>
@include('front/mini_games/js')
@endsection
