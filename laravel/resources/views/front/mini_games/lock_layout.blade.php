{!! Form::model($model, ['id' => 'form','class' => 'form','url' => ['game/'.$action], 'method' => 'post','files' => true]) !!}
{!! Form::hidden('lockType', $layout , array('id' => 'lockType') ) !!}
{!! Form::hidden('lockSetup', null , array('id' => 'lockSetup') ) !!}
{!! Form::hidden('id', null , array('id' => 'id') ) !!}
{!! Form::hidden('game_id', $game_id , array('id' => 'game_id') ) !!}
@include("front/mini_games/lock_layouts/".$layout)
@include('front/mini_games/forms/story')
{!! Form::close()!!}
<script>

    var clicked = "";

    $(document).on("submit", "form", function (event)
    {
        event.preventDefault();
        var url = $(this).attr("action");
        var text = quill.container.firstChild.innerHTML;
        $('#text').val(text);

        $.ajax({
            url: url,
            type: $(this).attr("method"),
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable alert-sticky"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="cont"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<ul><li>' + value + '</li></ul>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors').html(errorsHtml).show();
                } else
                {
                    if (clicked === 'save_lock_add') {
                        window.top.location = "<?php echo url('choose-lock-types') . '/' . $game_id ?>";
                        return false;
                    } else if (clicked === 'save_lock_exit') {
                        window.top.location = "<?php echo url('digital-game-finish') . '/' . $game_id ?>";
                        return false;
                    }

                    $('#add_lock').show();
                    $('#lock_layout').html('').hide();
                    $('#lock_layout').html('').hide();
                    $('#lock_layout_finish').show();
                    $('#message').html(response.message).show();
                }
            },
            error: function (xhr, status, response) {
            }
        });

    });

    function setClicked(val) {
        clicked = val;
    }

</script>