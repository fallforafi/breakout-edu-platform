<script type="text/javascript">
    $(document).ready(function () {
        var current = <?php echo empty($answers) ? 1 : count($answers); ?>;
        $('.alphabets').click(function () {
            var alphabet = $(this).attr('data-alphabet');

            for (var i = 1; i <= 10; i++) {
                var answer = $("#text_answers_" + i).val();
                if (answer == '') {
                    $("#text_answers_" + i).val(alphabet);
                    current = i;
                    $("#comb-" + current).addClass("has-value");
                    return false;
                }
            }
        });

        $('.text_clear').click(function () {
            $("#text_answers_" + current).val("");
            $("#comb-" + current).removeClass("has-value");
            current--;
        });
    });
</script>
<div id="selectLockText" class="select-lock__tabs select-lock__tabs--text key-control-area">
    <div class="lock__tabs--number-box">
        <div class="select-lock__tabs--number__select">
            <h3>Select the letters that will open the lock</h3>
            <div class="tabs--number__select">
                <?php
                foreach (range('A', 'Z') as $char) {
                    ?>
                    <div class="number__select-box cols-5">
                        <div class="number__select-inr">
                            <h2><a href="#_" data-alphabet="<?php echo $char; ?>" class="alphabets"  ><?php echo $char; ?></a></h2>
                        </div>
                    </div>
                <?php } ?>

                <div class="clearfix"></div>

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="select-lock__tabs--number__select select-lock__tabs--comb__select">

            @include('front/mini_games/combination')

            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>
        <div class="edit-save-btns text-center">
            <button type="button" class="edit-btn text_clear">
                Clear
            </button>
        </div>
    </div>
    <div class="clearfix"></div>	
</div>
<script type="text/javascript">

    jQuery(document).ready(function () {
        setTimeout(function () {

            var lockParent = "#selectLockText";
            var lockButton = "#selectLockText h2 a";

            jQuery(lockButton).on("click", function (e) {
                jQuery(lockParent).addClass("is-active");
            });

            jQuery(lockButton).on("blur", function (f) {
                jQuery(lockParent).removeClass("is-active");
            });

        }, 2000);



        var cursorFocus = function (elem) {
            var x = window.scrollX, y = window.scrollY;
            elem.focus();
            window.scrollTo(x, y);
        }

        cursorFocus(document.getElementById('text_answers_1'));

    });

</script>