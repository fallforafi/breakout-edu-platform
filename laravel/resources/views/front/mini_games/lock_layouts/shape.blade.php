<script type="text/javascript">

    $(document).ready(function () {

        var current = <?php echo empty($answers) ? 1 : count($answers); ?>;


<?php
if (!empty($answers)) {
    ?>

            for (var i = 1; i <= current; i++) {
                var answer = $("#shape_answers_" + i).val();
                $("#shape_answers_" + i).parents("h2").append('<i class="fa"><img src="{{asset('')}}/front/images/shape-'+answer+'.png" alt="shape"></i>');
            }

<?php } ?>




        $('.shapes').click(function () {
            var arrow = $(this).attr('data-shape');

            var arrowIcon = $(this).children("h2").html();
            //alert(arrowIcon);

            for (var i = 1; i <= 5; i++) {
                var answer = $("#shape_answers_" + i).val();
                if (answer == '') {
                    $("#shape_answers_" + i).val(arrow);
                    current = i;
                    
                    $("#shape_answers_" + i).parents("h2").append(arrowIcon);
                    $("#comb-" + current).addClass("has-value");
                    return false;
                }
            }
        });

        $('.shape_clear').click(function () {
            $("#shape_answers_" + current).val("");
            $("#shape_answers_" + current).parents("h2").children("i").remove();
            $("#comb-" + current).removeClass("has-value");
            current--;
        });


    });
</script>

<div id="selectLockShape" class="select-lock__tabs select-lock__tabs--shape select-lock--iconic">
    <div class="lock__tabs--number-box">
        <div class="select-lock__tabs--number__select">
            <h3>Select the shapes that will open the lock.</h3>
            <div class="tabs--number__select">
                <div class="number__select-box cols-5">
                    <div class="number__select-inr">
                        <a href="#" class="shapes" data-shape="circle"><h2><i class="fa"><img src="{{asset('')}}/front/images/shape-circle.png" alt="circle shape"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box cols-5">
                    <div class="number__select-inr">
                        <a href="#" class="shapes" data-shape="square"><h2><i class="fa"><img src="{{asset('')}}/front/images/shape-square.png" alt="square shape"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box cols-5">
                    <div class="number__select-inr">
                        <a href="#" class="shapes" data-shape="triangle"><h2><i class="fa"><img src="{{asset('')}}/front/images/shape-triangle.png" alt="triangle shape"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box cols-5">
                    <div class="number__select-inr">
                        <a href="#" class="shapes" data-shape="star"><h2><i class="fa"><img src="{{asset('')}}/front/images/shape-star.png" alt="star shape"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box cols-5">
                    <div class="number__select-inr">
                        <a href="#" class="shapes" data-shape="diamond"><h2><i class="fa"><img src="{{asset('')}}/front/images/shape-diamond.png" alt="diamond shape"></i></h2></a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
            <div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
                @include('front/mini_games/combination')
            </div>
            <div class="clearfix"></div>
            <div class="edit-save-btns text-center">
                <button type="button" class="edit-btn shape_clear">
                    Clear
                </button>
            </div>
        
    </div>
    <div class="clearfix"></div>
</div>



<script type="text/javascript">

    jQuery(document).ready(function () {
		setTimeout(function() {
							
			var lockParent = "#selectLockShape";
			var lockButton = "#selectLockShape a";
		
			jQuery( lockButton ).on("click", function (e){
				jQuery(lockParent).addClass("is-active");
			});

			jQuery( lockButton ).on("blur", function (f){
				jQuery(lockParent).removeClass("is-active");
			});

		}, 2000);
		
	});

</script>