<script type="text/javascript">

    $(document).ready(function () {

        var current = <?php echo empty($answers) ? 1 : count($answers); ?>;
        $('.numbers').click(function () {
            number = $(this).attr('data-numbers');

            for (var i = 1; i <= 5; i++) {
                var answer = $("#number_answers_" + i).val();
                if (answer == '') {
                    $("#number_answers_" + i).val(number);
                    current = i;
                    $("#comb-" + current).addClass("has-value");
                    return false;
                }
            }
        });

        $('.clear').click(function () {
            $("#number_answers_" + current).val("");
            $("#comb-" + current).removeClass("has-value");
            current--;
        });
    });
</script>

<div id="selectLockNumber" class="select-lock__tabs select-lock__tabs--number">
    <div class="lock__tabs--number-box">
        <div class="select-lock__tabs--number__select">
            <h3>Select the numbers that will open the lock</h3>
            <div class="tabs--number__select">
                <?php
                for ($i = 0; $i <= 9; $i++) {
                    ?>
                    <div class="number__select-box cols-5">
                        <div class="number__select-inr">
                            <h2><a href="#_" data-numbers="<?php echo $i; ?>" class="numbers"  ><?php echo $i; ?></a></h2>
                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
            @include('front/mini_games/combination')
        </div>
        <div class="clearfix"></div>
        <div class="edit-save-btns text-center">
            <button type="button" class="edit-btn clear">
                Clear
            </button>
        </div>
    </div>
    <div class="clearfix"></div>
</div>



<script type="text/javascript">

    jQuery(document).ready(function () {
		setTimeout(function() {
							
			var lockParent = "#selectLockNumber";
			var lockButton = "#selectLockNumber h2 a";
		
			jQuery( lockButton ).on("click", function (e){
				jQuery(lockParent).addClass("is-active");
			});

			jQuery( lockButton ).on("blur", function (f){
				jQuery(lockParent).removeClass("is-active");
			});

		}, 2000);
		
		
		var cursorFocus = function(elem) {
  var x = window.scrollX, y = window.scrollY;
  elem.focus();
  window.scrollTo(x, y);
}

cursorFocus(document.getElementById('number_answers_1'));

	});

</script>