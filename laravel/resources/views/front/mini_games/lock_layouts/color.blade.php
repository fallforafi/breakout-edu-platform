<script type="text/javascript">

    $(document).ready(function () {
        var current = <?php echo empty($answers) ? 1 : count($answers); ?>;
<?php
if (!empty($answers)) {
    ?>
            for (var i = 1; i <= current; i++) {
                var answer = $("#color_answers_" + i).val();
                $("#color_answers_" + i).parents("h2").append('<i class="fa bg-' + answer + '">&nbsp;</i>');
            }

<?php } ?>
        $('.colors').click(function () {
            arrow = $(this).attr('data-color');

            var arrowIcon = $(this).children("div").html();
            for (var i = 1; i <= 5; i++) {
                var answer = $("#color_answers_" + i).val();
                if (answer === '') {
                    $("#color_answers_" + i).val(arrow);
                    current = i;

                    $("#color_answers_" + i).parents("h2").append(arrowIcon);
                    $("#comb-" + current).addClass("has-value");
                    return false;
                }
            }
        });

        $('.color_clear').click(function () {
            $("#color_answers_" + current).val("");
            $("#color_answers_" + current).parents("h2").children("i").remove();
            $("#comb-" + current).removeClass("has-value");
            current--;
        });


    });
</script>
<div id="selectLockColor" class="select-lock__tabs select-lock__tabs--color select-lock--iconic">
    <div class="lock__tabs--number-box">
        <div class="select-lock__tabs--number__select">
            <h3>Select the colors in the sequence that you wish to open the lock</h3>
            <div class="tabs--number__select">
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="red">
                        <div id="select-color-red" class="select-color-box">
                            <i class="fa bg-red">&nbsp;</i>
                        </div>
                    </a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="green">
                        <div id="select-color-green" class="select-color-box">
                            <i class="fa bg-green">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="orange">
                        <div id="select-color-orange" class="select-color-box">
                            <i class="fa bg-orange">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="blue">
                        <div id="select-color-blue" class="select-color-box">
                            <i class="fa bg-blue">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="brown">
                        <div id="select-color-brown" class="select-color-box">
                            <i class="fa bg-brown">&nbsp;</i>
                        </div></a>
                </div>
                <div class="clearfix"></div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="yellow"><div id="select-color-yellow" class="select-color-box">
                            <i class="fa bg-yellow">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="purple"><div id="select-color-purple" class="select-color-box">
                            <i class="fa bg-purple">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="grey"><div id="select-color-grey" class="select-color-box">
                            <i class="fa bg-grey">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="white"><div id="select-color-white" class="select-color-box">
                            <i class="fa bg-white">&nbsp;</i>
                        </div></a>
                </div>
                <div class="number__select-box cols-5">
                    <a href="#" class="colors" data-color="black"><div id="select-color-black" class="select-color-box">
                            <i class="fa bg-black">&nbsp;</i>
                        </div></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        <div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
            @include('front/mini_games/combination')
        </div>
        <div class="clearfix"></div>
        <div class="edit-save-btns text-center">
            <button type="button" class="edit-btn color_clear">
                Clear
            </button>
        </div>

    </div>
    <div class="clearfix"></div>



</div>



<script type="text/javascript">

    jQuery(document).ready(function () {
        setTimeout(function () {

            var lockParent = "#selectLockColor";
            var lockButton = "#selectLockColor a";

            jQuery(lockButton).on("click", function (e) {
                jQuery(lockParent).addClass("is-active");
            });

            jQuery(lockButton).on("blur", function (f) {
                jQuery(lockParent).removeClass("is-active");
            });

        }, 2000);

    });

</script>
