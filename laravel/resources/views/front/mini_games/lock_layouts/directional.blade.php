<?php
// print_r($answers)
?>
<script type="text/javascript">

    $(document).ready(function () {

        var current = <?php echo empty($answers) ? 1 : count($answers); ?>;

<?php
if (!empty($answers)) {
    ?>

            for (var i = 1; i <= current; i++) {

                var answer = $("#directional_answers_" + i).val();

                $("#directional_answers_" + i).parents("h2").append('<i class="fa fa-arrow-' + answer + '"></i>');

            }

<?php } ?>

        $('.arrows').click(function () {
            var arrow = $(this).attr('data-direction');

            var arrowIcon = $(this).children("h2").html();
            //alert(arrowIcon);

            for (var i = 1; i <= 5; i++) {
                var answer = $("#directional_answers_" + i).val();
                if (answer == '') {
                    $("#directional_answers_" + i).val(arrow);
                    current = i;
                    $("#directional_answers_" + i).parents("h2").append(arrowIcon);
                    $("#comb-" + current).addClass("has-value");
                    return false;
                }
            }



        });

        $('.directional_clear').click(function () {
            $("#directional_answers_" + current).val("");
            $("#directional_answers_" + current).parents("h2").children("i").remove();
            $("#comb-" + current).removeClass("has-value");
            current--;
        });
    });
</script>

<div id="selectLockDirection" class="select-lock__tabs select-lock__tabs--direct select-lock--iconic">
    <div class="lock__tabs--number-box">
        <div class="select-lock__tabs--number__select">
            <h3>Select the directions that will open the lock</h3>
            <div class="tabs--number__select tabs--number__select--direct">
                <div class="number__select-box col-sm-3">
                    <div class="number__select-inr">
                        <a href="#" class="arrows" data-direction="up"><h2><i class="fa fa-arrow-up"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box col-sm-3">
                    <div class="number__select-inr">
                        <a href="#" class="arrows" data-direction="down"><h2><i class="fa fa-arrow-down"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box col-sm-3">
                    <div class="number__select-inr">
                        <a href="#" class="arrows" data-direction="left"><h2><i class="fa fa-arrow-left"></i></h2></a>
                    </div>
                </div>
                <div class="number__select-box col-sm-3">
                    <div class="number__select-inr">
                        <a href="#" class="arrows" data-direction="right"> <h2><i class="fa fa-arrow-right"></i></h2></a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
            <div class="select-lock__tabs--number__select select-lock__tabs--comb__select ">
                @include('front/mini_games/combination')
            </div>
            <div class="clearfix"></div>
            <div class="edit-save-btns text-center ">
                <button type="button" class="edit-btn directional_clear">
                    Clear
                </button>
            </div>
        
        <div class="clearfix"></div>
    </div>
</div>



<script type="text/javascript">

    jQuery(document).ready(function () {
		setTimeout(function() {
							
			var lockParent = "#selectLockDirection";
			var lockButton = "#selectLockDirection a";
		
			jQuery( lockButton ).on("click", function (e){
				jQuery(lockParent).addClass("is-active");
			});

			jQuery( lockButton ).on("blur", function (f){
				jQuery(lockParent).removeClass("is-active");
			});

		}, 2000);
		
	});

</script>