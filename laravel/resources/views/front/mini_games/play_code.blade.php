@extends('layout')
<?php
$title = $model->title;
$description = $model->description;
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')

@include('front/common/breadcrumb')
<section class="endgame-area">
    <div class="container">
        <div id="endGameScreen" class="mt20 mb50">
            <div id="endGameScreen" class="mt20 mb50">
                <div class="end-screen-box  ">
                    <div class="end-screen__inr">
                        <div class="end-screen-social">
                            <h2>Digital Games</h2>
                            <div class="end-screen__logo">
                                <img src="{{asset('')}}/front/images/end-screen-logo.png" alt="logo">
                            </div>
                            <h3>{{$model->title}}</h3>
                            @include('front/mini_games/forms/authorization')
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<script>
    $("#code").keyup(function (event) {
        var str = $('#code').val();
        if (str.length == 3 || str.length == 7) {
            $('#code').val(str + '-');
        }
    });
</script>
@endsection