@extends('layout')
<?php
$title = 'Create Digital Game';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')

@include('front/common/breadcrumb')

<section class="subj-area crt-game-area clrlist / in-view">
    <div class="container">
        <div class="hed underline">
            <h2>Create a New Digital Game</h2>
        </div>
        @include('front/mini_games/forms/game')
    </div>
</section>


@endsection
