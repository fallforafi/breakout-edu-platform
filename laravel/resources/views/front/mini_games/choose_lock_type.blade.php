@extends('layout')
<?php
$title = 'Create Lock Type';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="crt-game-steps clrlist">
    <div class="container">
        <div class="accordion-area accordion-plus col-sm-12">
            <div class="panel-group" id="accordion">
                @include('front/mini_games/forms/lock_list')
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $('.editlock').click(function () {

            var title = $(this).attr('data-title');
            var layout = $(this).attr('data-layout');
            var id = $(this).attr('data-id');

            $('#lockType').val(layout);
            $('#lockOptions').hide();
            $('#newLock').hide();

            $.ajax({
                url: "<?php echo url(""); ?>/edit-lock-layout",
                type: 'get',
                dataType: 'html',
                cache: false,
                async: true,
                data: {
                    title: title,
                    layout: layout,
                    id: id
                },
                beforeSend: function () {
                    $('#loader').show();
                },
                complete: function () {
                    $('#loader').hide();
                },
                success: function (response) {
                    $('#lock_layout').html(response);
                }
            });
        });

        $('.select-lock').click(function () {

            var title = $(this).attr('data-title');
            var layout = $(this).attr('data-layout');

            $('#lockType').val(layout);
            $('#lockOptions').hide();
            $('#newLock').hide();

            $.ajax({
                url: "<?php echo url(""); ?>/get-lock-layout",
                type: 'get',
                dataType: 'html',
                cache: false,
                async: true,
                data: {
                    title: title,
                    layout: layout,
                    game_id: <?php echo $game_id; ?>
                },
                success: function (response) {
                    $('#lock_layout').html(response);
                }
            });
        });
    });

    function lockSetupImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#lockImageUploaded')
                        .attr('src', e.target.result)
                        .width('430px')
                        .height('320px');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("#lockImageInput").change(function () {
            var fileName = $(this).val();
            $(".lock-setup__img--inr").removeClass('is-active');
            $(".lock-setup__img--has-image").addClass('is-active');
        });
    });


    $(document).ready(function () {
        $('.remove-image').click(function () {
            $(".lock-setup__img--has-image").removeClass('is-active');
            $(".lock-setup__img--inr").addClass('is-active');
        });
    });


    $(document).ready(function () {
        $(".selector .selectme").on("click", function () {
            $(this).siblings(".selectme").removeClass("is-active");
            $(this).addClass("is-active");
        });
    });
</script>

@endsection