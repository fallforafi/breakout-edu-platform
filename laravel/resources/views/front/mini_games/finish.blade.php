@extends('layout')
<?php
$title = $model->title;
$description = $model->description;
$keywords = "";
$required = "required";
?>

<style>
    .test{
        cursor: text !important;
    }
</style>

@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="endgame-area">
    <div class="container">
        <div id="endGameScreen" class="mt20 mb50">
            <div class="end-screen-box text-center">
                <div class="end-screen__inr">
                    <h2><?php echo $model->title ?></h2>
                    <div class="end-screen__logo">
                        <img src="{{asset('front/images/end-screen-logo.png')}}" alt="end screen logo">
                    </div>
                    <div class="end-screen-social">

                        <div class="form-group">
                            {!! Form::label('Game URL') !!}
                            <div class="input-group  clr">
                                {!! Form::text('url', url('/game/digital/').'/'.$model->key , array('id' => 'url','readonly' => 'readonly','class' => 'form-control test') ) !!}
                                <span class="input-group-addon addon-2">
                                   <a target="_blank" data-toggle="modal" href="{{url('/game/digital/').'/'.$model->key}}" class="btn  btn-small00 fw300 btn-success white"> <i class="fa fa-play"></i> Play </a>
								   <input value='Copy' type="button" name='copyurl' onclick="copy('#url')" id='copyurl' class="btn btn-default" />
                                   
                                </span>
								
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Play Code') !!}
                            <div class="input-group clr">
                                {!! Form::text('playCode', $model->playCode , array('id' => 'playCode','readonly' => 'readonly','class' => 'form-control test') ) !!}
                                <span class="input-group-addon">
                                    <input value='Copy' type="button" name='copyurl' onclick="copy('#playCode')" id='copyurl' class="btn btn-default"  />
                                </span>
                            </div>
                        </div>
						
                        <div class="form-group">
                            {!! Form::label('Game Embed Code') !!}
                            <?php
                            $url = url('/game/embed/') . '/' . $model->key;
                            $iframe = '<iframe src="' . $url . '" width="100%" height="500"></iframe>';
                            ?>
                            <div class="input-group  clr">
                                {!! Form::text('iframe', $iframe , array('id' => 'iframe','readonly' => 'readonly','class' => 'form-control test') ) !!}
                                <span class="input-group-addon">
                                    <input value='Copy' type="button" name='copyurl' onclick="copy('#iframe')" id='copyurl' class="btn btn-default" />
                                </span>
                            </div>
                        </div>

                        <div class="social-login text-center col-sm-12 pul-cntr p0 mt30">
                            @include('front/mini_games/includes/share')
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>

    </div>
</section>
<script>
    function copy(element)
    {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>
@endsection
