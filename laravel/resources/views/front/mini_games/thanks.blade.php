<?php
//d();
$referer=explode('/',$_SERVER['HTTP_REFERER']);
if(in_array("embed", $referer)){
    $embed=1;
}else{
    $embed=0;
}
?>
<div id="endGameScreen" class="mt20 mb50">
    <div class="end-screen-box text-center">
        <?php if ($model->endScreenType == 'image') { ?>

            <div class="end-screen__inr">
                <div class="end-screen__logo">
                    <img src="<?php echo path(); ?>uploads/mini_games/endscreens/{{$model->endScreen}}" />
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } elseif ($model->endScreenType == 'text') { ?>

            <div class="end-screen__inr white">
                <h3 class="lead mb20">
                    <?php //echo html_entity_decode($model->endScreen); ?>
                    <?php echo htmlspecialchars_decode(html_entity_decode($model->endScreen)); ?>
                </h3>
                <div class="clearfix"></div>
            </div>

        <?php } elseif ($model->endScreenType == 'video') { ?>

            <div class="end-screen__inr">
                <p class="lead mb20">
                    <?php echo html_entity_decode($model->endScreen); ?>
                </p>
                <div class="clearfix"></div>
            </div>

        <?php } else { ?>
            <div class="end-screen__inr">
                <?php if ($model->hasTimer == 1) { ?>
                    <h2>Congratulations!</h2> <h2>You Broke Out in <?php echo $timeTaken; ?>.</h2>
                <?php } else { ?>
                    <h2>Congratulations!</h2> <h2>You Broke Out!</h2>
                <?php } ?>
                <div class="end-screen__logo">
                    <img src="{{asset('front/images/end-screen-logo.png')}}" alt="logo">
                </div>
                <!-- <h1 class="display-3 white">Thank You!</h1>
    <p class="lead white"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>-->
                <hr>
                <p class="lead mb20">
                    <a rel="nofollow" rel="noreferrer"class="btn btn-primary btn-lg" href="{{url('/')}}" role="button">Continue to homepage</a>
                    
                </p>
                <div class="social-login text-center col-sm-8 pul-cntr">
                    @include('front/mini_games/includes/share')
                </div>

                <div class="clearfix"></div>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <?php if($embed==1){ ?>
                    <a rel="nofollow" rel="noreferrer"class="btn btn-lg0 mt20 bg-orange white" href="{{url('game/embed/')}}/<?php echo $model->key?>" role="button">Replay</a>
        <?php } ?>
    </div>
</div>