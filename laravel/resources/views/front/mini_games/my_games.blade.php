@extends('layout')
<?php
$title = 'My Digital Games';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="game-preview-area clrlist">
    <div class="container">

        <div class="hed underline">
            <h2>MY DIGITAL GAMES</h2>
        </div>

        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>

        <div class="game-preview-box col-sm-9">

            <div class="game__prev__inr game__list table-responsive">

                <div class="hedlist pul-lft clrhm0">
                    <h3>List of games</h3>
                </div>
                <div class="row">
                    <div id="library_errors"></div>
                    <div id="library_success"></div>
                </div>
                <a href="{{url('create-digital-game')}}" class="btn btn-default pul-rgt"> <i class="fa fa-plus"></i> Create New Game</a>

                @include('front/common/errors')
                @if(count($model)>0)
                <table class="table table-game-list table-valign mb0 btn-slide">
                    <thead>
                        <tr>
                            <th>Game Name</th>
                            <th><span class="pl15">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $game)
                        <tr class="">
                            <td>
                                <div class="btn-group btn-slide__group">
                                    <a data-toggle="modal" href="{{url('game/digital/view/')}}/{{$game->id}}" class="btn  btn-small fw300 btn-success white"> <i class="fa fa-play"></i> Play </a>
                                </div>

                                <!--a href="{{url('edit-digital-game')}}/{{$game->id}}">{{$game->title}}</a-->
                                <span class="desc">{{$game->title}}</span>
                            </td>

                            <td class="w60 text-right ">

                                <a href="javascript:void(0);"  data-id="{{$game->id}}" class="mini-game0 btn btn-default btn-small fw300 copy"> <i class="fa fa-list-alt"></i> <span>Copy</span> </a>
                                
                                <a data-toggle="collapse" data-target="#gameCompleteTable{{$game->id}}" href="javascript:void(0);"  data-id="{{$game->id}}" class="mini-game0 btn btn-default btn-small fw300 result"> <i class="fa fa-list-alt"></i> <span>Results</span> </a>

                                <a data-id="{{$game->id}}" class="mini-game0 btn btn-default btn-small fw300" href="{{url('game/digital/view/')}}/{{$game->id}}"> <i class="fa fa-share-alt"></i> <span>Share</span> </a>

                                <a  href="{{url('edit-digital-game')}}/{{$game->id}}" class="btn btn-default btn-small fw300"> <i class="fa fa-pencil"></i> <span>Edit</span> </a>


                                <a data-toggle="modal" data-embed="{{url('/game/embed/')}}/{{$game->key}}"  class="btn btn-default btn-small fw300 embed"> <i class="fa fa-code"></i> <span>Embed</span> </a>

                                <a data-id="{{$game->id}}" href="javascript:void(0);" class="confirm-delete btn btn-danger btn-small fw300"> <i class="fa fa-trash"></i> <span>Delete</span> </a>
                            </td>
                        </tr>
                        <tr class="tr__collapse">
                            <td colspan="2">
                                <div id="gameCompleteTable{{$game->id}}" class="collapse">
                                </div>
                            </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
                <div class="col-sm-12">
                <?php echo $model->appends(Input::query())->render(); ?>
                    </div>
                @else
                <div class="col-sm-12">
                    <div class="alert alert-danger mt30">You haven't created any games yet.</div>
                </div>
                @endif
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
@include('front/common/share_popup')

<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>You are about to delete.</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn btn-danger">Yes</a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-info">No</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="embedModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>Emebed Code</h4>
            </div>
            <div class="modal-body">
                <textarea id="iframe" readonly="readonly" class="form-control" name="iframe" type="text">

                </textarea>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="copyModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
			<a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
            
            <div class="modal-body text-center" id='copies_message'>
               
            </div>
        </div>
    </div>
</div>



<script>
    $(".copy").click(function (event) {
        var id = $(this).attr("data-id");
        $.ajax({
            url: "<?php echo url(""); ?>/game/copy/" + id,
            type: 'post',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                ajax: 1,
                id: id,
                _token: '<?php echo csrf_token() ?>',
            },
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    // $('#library_errors').html(errorsHtml).show();
                    $('#copyModal').modal('show');
                    $('#copies_message').html(errorsHtml);
                } else
                {
                    var message = "<h4>Game has been copies</h4> ";
                    var gamelink= "<?php echo url("game/digital/view/"); ?>"+"/"+response.new_game_id;
                    $('#copyModal').modal('show');
                    $('#copies_message').html(message+" <a class='btn btn-primary' href='"+gamelink+"'>Click Here</a>");
                }
            }
        });
    });
</script>

<script>
/*
    $('.copy').click(function () {
        var src = $(this).data('embed');
        var f = '<iframe src="' + src + '" id="embed_iframe" width="100%" height="500"></iframe>';
        $('#embedModal').modal('show');
        $('#iframe').val(f);
    });
*/
    $('.embed').click(function () {
        var src = $(this).data('embed');
        var f = '<iframe src="' + src + '" id="embed_iframe" width="100%" height="500"></iframe>';
        $('#embedModal').modal('show');
        $('#iframe').val(f);
    });

    $('.result').on('click', function (e) {
        var game_id = $(this).data('id');
        $.ajax({
            url: "<?php echo url(""); ?>/my-digital-games/results/" + game_id,
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#gameCompleteTable' + game_id).html(response);
            }
        });

    });

    jQuery(".btn-slide .desc").on('click', function () {
        jQuery(this).parent("td").toggleClass("btn-slide__group--show");
    });
    var id = "";
    $('#myModal').on('show', function () {
        id = $(this).data('id'),
                removeBtn = $(this).find('.danger');
    })

    $('.confirm-delete').on('click', function (e) {
        id = $(this).data('id');
        $('#delete-modal').data('id', id).modal('show');
    });

    $('#btnYes').click(function () {
        window.top.location = "<?php echo url("game/digital/delete") ?>/" + id;
        $('#myModal').modal('hide');
    });
</script>
@endsection
