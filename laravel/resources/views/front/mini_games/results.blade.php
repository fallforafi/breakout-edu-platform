@if(count($results)>0)
<div class="col-sm-12 pull-right">               
    <a href="{{url('game/results-download/')}}/{{$model->id}}"><button class="mt20 btn btn-success pull-right btn-download"><i class="fa fa-download"></i> Download CSV</button></a>
</div>
<table class="table table-game-list table-valign">
    <thead>
        <tr >
            <th  class="w30">Name</th>
            <th>Completed</th>
            <th>Time to Complete</th>
            <th  class="w30">Date</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach ($results as $row)
        <tr>
            <td><?php echo $row->name; ?></td>
            <td><?php echo $row->completed; ?> % </td>
            <td><?php echo $row->time; ?></td>
            <td><?php echo showDate($row->created_at); ?></td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
</table>
<div class="col-sm-12">
    <?php echo $results->appends(Input::query())->render(); ?>
</div>
@else
<div class="col-sm-12">
    <div class="alert alert-danger mt30">No one has played this game yet.</div>
</div>
@endif

<script>

    $('li a').click(function (e) {

        $.ajax({
            url: this.href,
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#gameCompleteTable' + <?php echo $model->id; ?>).html(response);
            }
        });
        return false;
    });
</script>
