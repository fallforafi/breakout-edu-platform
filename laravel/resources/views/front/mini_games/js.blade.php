<script type="text/javascript">
    var a = {};
    var time = 0;
    var stopWatch;
    var c_lock_id =<?php echo $model->id ?>;
    var c_layout = '<?php echo $model->lockType ?>';
    var hasTimer = <?php echo $model->hasTimer ?>;

    $(document).ready(function () {

<?php if ($model->gameType == 'connected') { ?>
            getLock(<?php echo $model->id; ?>, '<?php echo $layout; ?>', '<?php echo addslashes($title); ?>', 1);

<?php } ?>

        $('#loader').hide();
    });

    function saveAnswers()
    {
        for (i = 1; i <= 10; i++)
        {
            var value = $('#' + c_layout + "" + "_answers_" + i).val();
            if (i == 1) {
                if (value == "") {
                    break;
                } else {
                    a[c_lock_id] = {};
                }
            }
            a[c_lock_id][i] = value;


        }
        // alert(JSON.stringify(a));
    }

    function getLock(game_id, layout, title, number) {

        $.ajax({
            url: "<?php echo url(""); ?>/play-lock-layout",
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            data: {
                title: title,
                layout: layout,
                game_id: game_id,
                page: number,
                answers: $.param(a)

            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#lock_layout').html(response);
                $('.individual').on('click');
            }
        });
    }

    function validate(game_id, errorMessage) {

        $.ajax({
            url: "<?php echo url("game/digital/end"); ?>/" + game_id,
            type: 'get',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                game_id: game_id,
                answers: $.param(a)
            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                // alert(JSON.stringify(response));

                if (response.error === 1) {

                    if (errorMessage == 1) {
                        var errorsHtml = '<div class="alert alert-danger alert-dismissable alert-sticky"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="cont"><ul>';
                        $.each(response.errors, function (key, value) {
                            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                        });

                        errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                        $('#errors').html(errorsHtml).show();

                    }
                } else
                {
                    if (response.answers.wrong == 0) {
                        thanks(game_id);
                    }
                }
            }
        });

    }

    function checkAnswer(game_id, layout, title, number) {
        saveAnswers();
        $.ajax({
            url: "<?php echo url(""); ?>/check-answer",
            type: 'get',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                layout: c_layout,
                game_id: game_id,
                lock_id: c_lock_id,
                answers: $.param(a)

            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable alert-sticky"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="cont"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors').html(errorsHtml).show();
                } else
                {
                    if (number == 'end') {
                        thanks(game_id);

                    } else {
                        getLock(game_id, layout, title, number);
                    }
                }
            }
        });

    }

    function thanks(game_id)
    {
        clearInterval(stopWatch);
        $.ajax({
            url: "<?php echo url(""); ?>/thank-you",
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            data: {
                game_id: game_id,
                time: time,
                answers: $.param(a)
            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('.game__prev__inr').html(response);
            }
        });
    }

    function finish(type, game_id)
    {
        var token = $('#_token').val();

        $.ajax({
            url: "<?php echo url(""); ?>/game/digital/end",
            type: 'get',
            dataType: 'json',
            cache: false,
            async: true,
            data: {
                type: type,
                game_id: game_id,
                time: time,
                answers: a,
                _token: token
            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-sticky"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="cont"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
                    $('#errors').html(errorsHtml).show();
                } else
                {
                    thanks(game_id);
                    //  $('#lock_layout').html('').hide();
                    //  $('#lock_layout_finish').show();
                    //  $('#message').html(response.message).show();
                }
            }
        });

    }
</script>
<script>


    $('.individual').click(function () {

        var title = $(this).attr('data-title');
        var layout = $(this).attr('data-layout');
        var game_id = $(this).attr('data-id');
        var number = $(this).attr('data-number');
        c_layout = layout;
        getLock(game_id, layout, title, number);
        $("#locks_individual").hide();
    });

    $('.finish_individual').click(function () {

        var type = $(this).attr('data-type');
        var game_id = $(this).attr('data-id');
        var layout = $(this).attr('data-layout');
        var lock_id = $(this).attr('data-lock-id');
        saveAnswers();
        finish(type, game_id);
    });

    jQuery(".zoomized").click(function () {
        jQuery(this).toggleClass("is-active");
    });
</script>
<?php

if ($model->hasTimer == 1) {
    list($min, $sec) = explode(':', $model->time);
    $secToMin = $sec / 60;
    $target = $min + $secToMin;
    ?>
    <script type="text/javascript">

        $(document).ready(function () {
            var fiveMinutes = 60 * <?php echo $target ?>,
                    display = $('#time');
            startTimer(fiveMinutes, display);
        });

        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            stopWatch = setInterval(function () {
                minutes = parseInt(timer / 60, 10)
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                time = minutes + ":" + seconds;
                display.text(time);

                if (--timer < 0) {
                    timer = duration;
                    timesUp('<?php echo $model->gameType ?>', '<?php echo $model->id ?>');
                }
            }, 1000);
        }


        function timesUp(type, game_id)
        {

            $('.game-preview-box').html('<div class="end-screen-box game-timeout-area text-center col-sm-12"><div class="end-screen__inr"><img src="{{asset('')}}/front/images/timeout-img.png" /><h3>Sorry, but time has run out!</h3><div class="end-screen__logo"><img src="{{asset('')}}/front/images/end-screen-logo.png" alt="fail"></div></div><div class="clearfix"></div></div>');
                    clearInterval(stopWatch);
            time = "00:00";
            
            $.ajax({
                url: "<?php echo url(""); ?>/thank-you",
                type: 'get',
                dataType: 'html',
                cache: false,
                async: true,
                data: {
                    game_id: game_id,
                    time: time,
                    answers: $.param(a)
                },
                beforeSend: function () {
                    $('#loader').show();
                },
                complete: function () {
                    $('#loader').hide();
                },
                success: function (response) {
                    // $('.game__prev__inr').html(response);
                }
            });



        }
    </script>
    <script>

        var hms = jQuery("#time").html();
        var split = hms.split(':');

        colorNormal = "#dd0000";
        colorHigh = "#dd0000";

        /* minutes are worth 60 seconds. Hours are worth 60 minutes. */
        var seconds = (+split[0]) * 60 + (+split[1]);

        var timeInSeconds = jQuery("#timeInSeconds").html(seconds);
		
		var myPer = 1 / seconds * 100;
       
        var per = 0;
        setInterval(function () {
            per = per + myPer; /* % as time value */
            if (per <= 100) {
                //jQuery('.stopwatch').css({left: +per+"%"});
                jQuery('#load-bar').css({background: "linear-gradient(to right, " + colorNormal + " " + per + "%,#FFF " + per + "%)"});

                if (per > 75) {
                    colorNormal = colorHigh;
                    jQuery("#load-bar-frame").addClass("near");
                }
            }
			
			console.log(per);
			
			if(per>=10){
				//alert(0);
				$("body").addClass("stopwatch--10");
			}
			if(per>90){
				$("body").addClass("stopwatch--near");
			}
			
			
			
        }, 1000);

		
    </script>
    <?php
}
?>