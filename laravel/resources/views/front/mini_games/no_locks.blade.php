@extends('layout')
<?php
$title = 'No Locks';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="game-preview-area clrlist">
    <div class="container">
        <div class="content">
            <div class="title">Sorry this game has no locks</div>
        </div>
    </div>
</section>
