@extends('layout')
<?php
$title = 'My Games';
$description = "";
$keywords = "";
$required = "required";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="game-preview-area clrlist">
    <div class="container">
        <div class="hed underline">
            <h2>My Library</h2>
        </div>
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="game-preview-box col-sm-9">
            <div class="game__prev__inr">
                @if(count($model)>0)
                <table class="table table-valign mb0">
                    <thead>
                        <tr>
                            <th>Game Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $game)
                        <tr>
                            <td><a href="{{url('game/mini')}}/{{$game->key}}">{{$game->title}}</a></td>
                            <td class="w40 text-left">
                                <a  href="{{url('library/delete')}}/{{$game->id}}" class="btn btn-danger btn-small fw300"> <i class="fa fa-trash"></i> Delete </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                <div class="col-sm-12">
                    <div class="alert alert-danger">You haven’t added any games to your library yet.</div>
                </div>
                @endif
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
@endsection
