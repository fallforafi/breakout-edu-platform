@extends('layout')
<?php
$title = $category[0]->name;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
<style>
    a.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>
@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('categories')}}">SUBJECT PACK</a></li>
        <?php
        foreach ($parents as $parent) {
            ?>
            <li><a href="{{url('category')}}/<?php echo $parent['key']; ?>"><?php echo strtoupper($parent['name']); ?></a></li> 
        <?php } ?>
        <li class="active"><?php echo strtoupper($category[0]->name); ?></li>        
    </ol>
</div>

<section class="prod-bnr-area chem-bnr" style="background-image: url('<?php echo path(); ?>uploads/categories/cover_images/<?php echo $category[0]->coverImage;?>')">
    <div class="container">

        <div class="prod__bnr__cont valigner text-center col-sm-12">
            <div class="valign">
                <div class="hed underline">
                    <h2><?php echo $category[0]->name; ?></h2>
                </div>
                <div class="subj__games__num">
                    <h4 id="sum" >{{count($games)}}</h4>
                    <span>Games</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="prod-area clrlist">
    <?php
    $q = '';
    ?>
    <div class="container">
        <div class="valign">
            @include('front/search/form')
        </div>
    </div>
</section>
@if(count($childCategoriesGames)>0)

<section class="prod-area pt0 clrlist">
    <div class="container">

        @foreach($childCategoriesGames as $row)

        @foreach($row as $value)
        <?php //d($row, 1); ?>
        <div class="prod-box col-sm-4">
            <div class="prod__inr">
                <div class="prod__img">
                    <img src="<?php echo path(); ?>uploads/categories/thumbnail/{{$value->image}}" alt="{{$value->name}}" />
                </div>
                <div class="prod__title"><a href="{{ url('category/'. $value->key) }}" class="active"><h4 > <?php echo $value->name; ?>(<span id='c_{{$value->id}}'></span>)</h4></a>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach


    </div>
</section>

@else
<section class="prod-area pt0 clrlist">
    <div class="container">
        @foreach($games as $game)
        <div class="prod-box col-sm-4
        <?php if (isset(Auth::user()->id) && Auth::user()->role->role != 'paid' && $game->forPaid == 1) { ?>
                 game-paid
             <?php } ?>
             ">
            <div class="prod__inr">
                <div class="prod__img">
                    <img src="<?php echo path(); ?>uploads/games/thumbnail/{{$game->image}}" alt="{{$game->title}}" />
                </div>
                <div class="prod__title">
                    <?php if (isset(Auth::user()->id) && Auth::user()->role->role != 'paid' && $game->forPaid == 1) { ?>
                        <a href="{{url('game')}}/{{$game->key}}"><h4 style="color:#d3d3d3">{{$game->title}}</h4></a>
                    <?php } else { ?>
                        <a href="{{url('game')}}/{{$game->key}}"><h4>{{$game->title}}</h4></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
@endif


<script>
    var sum=0;
    $(document).ready(function () {
        getGames();
        
    });
    
    function getGames()
    {
        var categories =<?php echo json_encode($cids) ?>;
        $.each(categories, function (key, value) {

            $.ajax({
                url: "<?php echo url('get-games'); ?>?id=" + value,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $("#c_"+value).html(response.games);
                     sum=sum+response.games;
                     $("#sum").html(sum).show();
                },
                error: function (xhr, status, response) {
                }
            });
        });
        
    }
    
</script>
@endsection
