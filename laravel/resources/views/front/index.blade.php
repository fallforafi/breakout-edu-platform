@extends('layout')
<?php
$title = 'Home';
$description = 'Breakout EDU is the immersive learning games platform &#124; It\'s time for 
something different';
$keywords = '';
$bgs = array('red', 'yellow', 'green', 'pink', 'blue', 'gray', 'orange', 'teal', 'maroon', 'brown', 'purple');
?>
@include('front/common/meta')
@section('content')

<section class="subj-area clrlist">
    <div class="container">

        <div class="hed underline">
            <h2>Subject Packs</h2>
        </div>
        <div class="subj__search col-sm-12 ">
            <!--div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                <input id="search" type="text" class="form-control" name="search" placeholder="Search games">
            </div-->
            <form class="form" role="form" id="filter" action="{{ url('query') }}">
                <div class="input-group">
                    <span class="input-group-addon">
                        <button type="submit" class="clrbtn"> 
                            <i class="glyphicon glyphicon-search"></i>       
                        </button>
                    </span>
                    <input type="text" autocomplete="off" class="form-control" id="q" name="q" placeholder="Search games">
                </div>
            </form>
            <div class="col-sm-12" id="search_listing" style="display:none"></div>
        </div>

        @if(count($categories) > 0)
        <div class="subj-main col-sm-12 no-transition broken-image-default">
            <!-- Swiper -->
            <div class="swiper-container s1">
                <div class="swiper-wrapper">
                    @foreach($categories as $category)
                    <div class="swiper-slide ">
                        <div class="subj-box img-cntr ">
                            <div class="subj__inr ">
                                <div class="subj__img ">
                                    <img src="<?php echo path(); ?>uploads/categories/thumbnail/{{$category->image}}" alt="{{$category->name}}" />
                                </div>
                                <div class="subj__title">
                                    <a href="{{url('category')}}/{{$category->key}}"><h4>{{$category->name}} Games</h4></a>
                                </div>
                                <div class="subj__games__num __icon">
                                    <h4 id="c_{{$category->id}}" >{{$category->total}}</h4>
                                    <span>Games</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination1"></div>
            </div>
        </div>

        <div class="more-area mt30 col-sm-12 text-center">
            <a href="{{url('categories')}}" class="btn btn-lg btn-primary">MORE</a>
        </div>
        @else 
        <div class="more-area mt30 col-sm-12 text-center">
            <a href="" class="btn btn-lg "><h3>No Data Found . . . .</h3></a>
        </div>
        @endif

    </div>
</section>

<section class="games-area clrlist">
    <div class="container">
        <div class="hed underline">
            <h2>DIGITAL Games</h2>
        </div>
    </div>
    <div class="container0">
        <div class="feat-games">
            <div class="container">
                <div class="feat__hed col-sm-12">
                    <div class="hed underline">
                        <h3>Featured DIGITAL  Games</h3>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-sm-12 p0">
                    <div class="swiper-container s2 ">
                        <div class="swiper-wrapper mb20">


                            <?php $i = 0; ?>

                            @foreach($featuredMiniGames as $minigame)

                            <?php
                            $c = rand(0, 10);
                            ?>

                            <div class="feat-box col-sm-12 swiper-slide <?php echo "val_" . $i; ?>">
                                <div class="feat__inr">
                                    <div class="feat__img bg-<?php echo $bgs[$c]; ?>">
                                        <?php if (trim($minigame->image) != "") { ?>
                                            <img src="<?php echo path(); ?>uploads/games/thumbnail/{{$minigame->image}}" alt="{{$minigame->title}}" />
                                        <?php } ?>
                                    </div>
                                    <div class="feat__title">
                                        <h4>{{$minigame->title}}</h4>
                                    </div>
                                    <div class="feat__bar">
                                        <ul>
                                            <li class="feat__game__share "><a href="#" data-id="{{$minigame->id}}" data-toggle="modal" class="mini-game" data-target="#shareGame"><i class="fa fa-share-alt"></i>SHARE GAME</a></li>

                                            <li class="feat__game__start "><a href="{{url('game/digital')}}/{{$minigame->key}}" ><i class="fa fa-play"></i>START GAME</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination swiper-pagination2"></div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="more-area mt10 col-sm-12 text-center">
                    <a href="{{url('featured')}}" class="btn btn-lg btn-primary">MORE</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="create-area clrlist">
    <div class="container">
        <div class="create-game col-sm-12 rotate360--hover">
            <div class="create__game__box">
                <a class="__icon" href="{{url('create-digital-game')}}"><span>&#43;</span></a>
                <h2>Create New Breakout EDU DIGITAL game </h2>
            </div>
        </div>
    </div>
</section>

<section class="games-area clrlist">
    <div class="container0">
        <div class="feat-games">
            <div class="container">
                <?php
                //d(Auth::user()->role->role,1);
                if (isset(Auth::user()->id)) {
                    $i = 0;
                    ?>
                    <div class="feat__hed col-sm-12">
                        <div class="hed underline">
                            <h3>MY DIGITAL GAMES</h3>
                        </div>
                    </div>

                    @foreach($miniGames as $minigame)
                    <?php
                    $c = rand(0, 10);
                    ?>
                    <div class="feat-box col-sm-6 <?php echo "val_" . $i; ?>">
                        <div class="feat__inr">
                            <div class="feat__img bg-<?php echo $bgs[$c]; ?> ">
                                <!--img src="<?php echo path(); ?>/front/images/minigames1.jpg" alt="{{$minigame->title}}" /-->
                            </div>
                            <div class="feat__title">
                                <h4>{{$minigame->title}}</h4>
                            </div>
                            <div class="feat__bar">
                                <ul>
                                    <li class="feat__game__share "><a href="#" data-id="{{$minigame->id}}" data-toggle="modal" class="mini-game" data-target="#shareGame"><i class="fa fa-share-alt"></i>SHARE GAME</a></li>

                                    <li class="feat__game__start "><a href="{{url('game/digital')}}/{{$minigame->key}}" ><i class="fa fa-play"></i>START GAME</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                    @endforeach
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<div id="test"></div>
@include('front/common/share_popup')
<script>
    $(document).ready(function () {
        searchListing();
        getGames();
    });
    $("#filter").change(function (e) {
        searchListing();
        e.preventDefault();
    });
//    $("#filter").submit(function (e) {
//        searchListing();
//        e.preventDefault();
//    });
    $("#q").keyup(function (e) {
        searchListing();
    });

    function getGames()
    {
        var categories =<?php echo json_encode($cids) ?>;
        $.each(categories, function (key, value) {

            $.ajax({
                url: "<?php echo url('get-games'); ?>?id=" + value,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $("#c_"+value).html(response.games);
                },
                error: function (xhr, status, response) {
                }
            });
        });
    }

    function searchListing() {
        $('#search_listing').hide();
        var formdata = $("#filter").serialize();
        var q = $('#q').val();
        $.ajax({
            url: "<?php echo url('search'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                if (q !== '') {
                    $('#search_listing').html(response);
                    if (response !== '') {
                        $('#search_listing').show();
                    }

                } else {
                    $('#search_listing').hide();
                }

            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
@endsection
