@extends('layout')
<?php
$title = $game->title;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="img-tut-area clrlist / in-view">
    <div class="container">
        <div class="img-tut__lft col-sm-7">
            <div class="img-tut__img">
                <img src="{{asset('')}}/uploads/games/{{$game->image}}" alt="{{$game->title}}" />
            </div>
        </div>	
        <div class="img-tut__rgt col-sm-5">
            <div class="img-tut__points">
                <h2>{{$game->title}}</h2>
                <h5>{{$game->note}}</h5>
            </div>
        </div>		
    </div>
</section>
<section class="vid-tut-dtl-area crt-game-steps clrlist">
    <div class="container">
        <div class="access-resrc-btn">
            <a href="<?php echo $game->launchTool; ?>"><span>LAUNCH FACILITATION TOOL</span></a>
        </div>
    </div>
</section>
@endsection