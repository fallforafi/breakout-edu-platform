@extends('layout')
<?php
$title = $game->title;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
@include('front/common/breadcrumb')
<section class="img-tut-area clrlist / in-view">
    <div class="container">
        <div class="img-tut__lft col-sm-7">
            <div class="img-tut__img">
                <img src="<?php echo path(); ?>uploads/games/{{$game->image}}" alt="{{$game->title}}" />
            </div>
        </div>	
        <div class="img-tut__rgt col-sm-5">
            <div class="img-tut__points">
                <h2>{{$game->title}}</h2>
                <ul>
                    <li>{{$game->infoLine1}}</li>
                    <li>{{$game->infoLine2}}</li>
                    @if($game->infoLine3)
                    <li>{{$game->infoLine3}}</li>
                    @endif
                    @if($game->infoLine4)
                    <li>{{$game->infoLine4}}</li>
                    @endif
                </ul>
            </div>
        </div>		
    </div>
</section>
<section class="img-tut-dtl-area crt-game-steps clrlist">
    <div class="container">
        <div class="accordion-area accordion-plus col-sm-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default active">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#imgTutStory">
                                STORY</a>
                        </h4>
                    </div>
                    <div id="imgTutStory" class="selector panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="img-story__inr">
                                <?php echo $game->story ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#imgTutLockComb">
                                LOCK COMBINATIONS</a>
                        </h4>
                    </div>
                    <div id="imgTutLockComb" class="selector panel-collapse collapse">
                        <div class="panel-body">
                            <div class="img-lock__inr">
                                <?php echo $game->lockCombinations ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#imgTutSetup">
                                SETUP INSTRUCTIONS</a>
                        </h4>
                    </div>
                    <div id="imgTutSetup" class="selector panel-collapse collapse">
                        <div class="panel-body">
                            <div class="img-setup__inr col-sm-12">
                                <?php echo $game->instructions ?>

                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
</section>

<section class="vid-tut-bnr-area clrlist">
    <div class="container">
        <div class="vid-bnr-area col-sm-12">
            <div class="vid-bnr__inr">
                <?php echo $game->videoEmbed; ?>
            </div>
        </div>
    </div>
</section>

<section class="vid-tut-dtl-area crt-game-steps clrlist">
    <div class="container">
		
		<div class="access-resrc-btn col-sm-6">
			<a target="_blank" href="<?php echo $game->linkToFiles ?>"><span>Access Game Resources</span></a>
        </div>
					
        <div class="access-resrc-btn col-sm-6">
            <a href="<?php echo $game->launchTool; ?>"><span>LAUNCH FACILITATION TOOL</span>
            </a>
        </div>

        <div class="accordion-area accordion-plus col-sm-12">
            <div class="panel-group" id="accordion">
                <!-- 
                @if(count($tools)>0)
                <div class="panel panel-default active">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#tools" class="">
                                Facilitation Tools</a>
                        </h4>
                    </div>
                    <div id="tools" class="selector panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="vid-ques__inr">

                                @foreach($tools as $tool)

                                <div class="prod-box col-sm-4">
                                    <div class="prod__inr">
                                        <div class="prod__img">
                                            <img src="<?php echo path(); ?>uploads/tools/{{$tool->image}}" alt="{{$tool->title}}" />
                                        </div>
                                        <div class="prod__title">
                                            <h4><a href="{{$tool->url}}">{{$tool->title}}</a></h4>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                <p><a href="#">Learn more about Breakout EDUreflection Cards</a></p> 
            </div>
        </div>
    </div>
</div>

@endif
                -->
                @if($game->questions)
                <div class="panel panel-default active">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#reflectionQuestions" class="">
                                REFLECTION QUESTIONS</a>
                        </h4>
                    </div>
                    <div id="reflectionQuestions" class="selector panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="vid-ques__inr">
                                <?php
                                echo $game->questions;
                                ?>
                                <!--
                                <p><a href="#">Learn more about Breakout EDUreflection Cards</a></p> -->
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($game->modifications)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#gameModify" class="collapsed">
                                GAME MODIFICATIONS</a>
                        </h4>
                    </div>
                    <div id="gameModify" class="selector panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <div class="img-setup__inr vid-modi__inr">
                                <?php
                                echo $game->modifications;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($game->requirements)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#additionalRequirements" class="collapsed">ADDITIONAL REQUIREMENTS</a>
                        </h4>
                    </div>
                    <div id="additionalRequirements" class="selector panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <div class="img-lock__inr vid-req__inr">
                                <?php
                                echo $game->requirements;
                                ?>

                                <!-- <div class="vid-req__inr__box text-center">
                                    <div class="vid-req__inr__img">
                                        <img src="images/video-req-pic.jpg" alt="video requirement kit">
                                    </div>
                                    <div class="vid-req__inr__title">
                                        <h3>GET a breakout EDU kit</h3>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection