@extends('layout')
<?php
$title = 'Non Paid User';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <h1>
                    Oops!</h1>
                <h2> <i class="fa fa-exclamation-triangle" style="color:red"></i> Unauthorized to access! You're not a paid user.</h2>
            </div>
        </div>
    </div>
</div>
@endsection
