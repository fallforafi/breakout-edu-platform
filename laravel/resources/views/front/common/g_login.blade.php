<script src="https://apis.google.com/js/api:client.js"></script>
<script>
    var googleUser = {};
    var startApp = function () {
        gapi.load('auth2', function () {
            // Retrieve the singleton for the GoogleAuth library and set up the client.
            auth2 = gapi.auth2.init({
                client_id: '<?php echo env('GOOGLE_CLIENT_ID') ?>',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
            });
            attachSignin(document.getElementById('google_login'));
            attachSignin(document.getElementById('google_signup'));
        });
    };

    function attachSignin(element) {
        auth2.attachClickHandler(element, {},
                function (googleUser) {
                    email = googleUser.getBasicProfile().getEmail();
                    firstName = googleUser.getBasicProfile().getGivenName();
                    lastName = googleUser.getBasicProfile().getFamilyName();
                    checkMember(email, firstName, lastName, 'google');
                    //verify('google', "https://plus.google.com/" + googleUser.getBasicProfile().getId(), googleUser.getBasicProfile().getId());
                }, function (error) {
            // alert(JSON.stringify(error, undefined, 2));
        });
    }

    
</script>