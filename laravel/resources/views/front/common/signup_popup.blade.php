<!-- Sign up Modal -->
<div id="signupModel" class="modal fade signup-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create an Account</h4>
            </div>
            <div class="modal-body" id='sign-body'>
                <div class="preloader-area">
                    <div class="preloader-area__status">
                        <i class="fa fa-spin"><img src="{{asset('front/images/preloading.png')}}" alt="" /></i>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--Sign up Modal End-->