@if(count($breadcrumbs)>0)
<section class="breadcrumb-area fade-corner fade-corner--right ">
 <div class="container">
    <ol class="breadcrumb">
        
        <?php
        $i=1;
        foreach($breadcrumbs as $breadcrumb){?>
        <li <?php if(count($breadcrumbs)==$i){ ?> class="active" <?php }?>><a href="<?php echo $breadcrumb['url']?>"><?php echo strtoupper($breadcrumb['name'])?></a></li>
        <?php
        $i++;
        }
        ?>
          
    </ol>
</div>
</section>
@endif