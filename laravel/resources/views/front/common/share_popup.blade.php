<div class="modal fade" id="share-dialog" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span> Share</h4>
            </div>
            <div class="modal-body" id="share">
                <div class="preloader-area">
                    <div class="preloader-area__status">
                        <i class="fa fa-spin"><img src="{{asset('front/images/preloading.png')}}" alt="" /></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('.mini-game').click(function (event) {
        $("#share-dialog").modal("show");
        var id = $(this).attr("data-id");
        $.ajax({
            url: "<?php echo url(""); ?>/share/" + id,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            success: function (response) {
                $('#share').html(response);
            }
        });
    });

</script>
@include('front/common/fb_login')
