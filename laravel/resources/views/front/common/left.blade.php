<ul class="nav nav-pills nav-stacked ">
    <li><a href="{{ url('profile') }}"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>
    <li><a href="{{ url('changepassword')}}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
    <li><a href="{{ url('my-digital-games')}}"><i class="fa fa-gamepad"></i> <span>My Digital Games</span></a></li>
</ul>
