<header>
    <section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white hdr-white">
        <div class="container">
            <nav class="navbar navbar-default" role="navigation" id="slide-nav">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a>
                    </div>

                    <!-- href="{{url('signup')}}"Collect the nav links, forms, and other content for toggling -->
                    <div id="slidemenu">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-main pul-rgt">
                                <li><a href="{{ url('https://breakoutedu.com/') }}">Home</a></li>
								<li><a href="{{ url('/') }}">Platform</a></li>
                                <li class=""><a target="_blank" href="https://store.breakoutedu.com/">Store</a></li>
                                <?php if (!isset(Auth::user()->id)) { ?>
                                    <li><a id='signup-button' data-toggle="modal" data-target="#signupModel" >Sign Up</a></li>
                                    <li><a id='login-button' data-toggle="modal" data-target="#signIn">Sign In</a></li>
                                <?php } else { ?>

                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> My Account</a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ url('profile')}}" >Edit Profile</a>
                                            </li>
                                            <li><a href="{{ url('changepassword')}}" class="pagelinkcolor">Change Password</a></li>
                                            <li>
                                                <a href="{{ url('create-digital-game')}}" target="_self">Create New Games</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('my-digital-games')}}" target="_self">My Digital Games</a>
                                            </li>
                                            <li class="divider"></li>


                                            <li>
                                                <a href="{{ url('auth/logout')}}" target="_self">Signout</a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>

                                <!--
                                <li style="display:none;"><a href="#!6">My Account</a></li>
                                <li><a data-toggle="modal" data-target="#login">Login</a></li>
                                <li><a data-toggle="modal" data-target="#signup">Sign Up</a></li>
                                -->
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </section>
</header>
