<script>
    function checkMember(email, firstName, lastName, type) {
        $.ajax({
            url: "<?php echo url(""); ?>/fblogin",
            type: 'get',
            dataType: 'json',
            data: {
                email: email,
                firstName: firstName,
                lastName: lastName,
                joinFrom: type,
                _token: $('#_token').val()
            },
            success: function (result) {
                if (result.login == false)
                {
                    window.top.reload();
                    return false;
                } else
                {
                    window.top.location = "<?php echo url(''); ?>/" + result.redirect;
                    return false;
                }
            }
        });
    }


    $("#signup-button").click(function () {
        $('#login-body').html('');
        $.ajax({
            url: "<?php echo url(""); ?>/signupform",
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            data: {
                ajax: 1
            },
            success: function (response) {
                $('#sign-body').html(response);
            }
        });

    });

    $("#login-button").click(function () {
        $('#signup-body').html('');
        $.ajax({
            url: "<?php echo url(""); ?>/loginform",
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            data: {
                ajax: 1
            },
            success: function (response) {
                $('#login-body').html(response);
            }
        });

    });



</script>