<footer>

    <section class="bottom-area p20 clrlist white listdvr">
    
		<div class="container">
	
			<div class="pul-lft">
			© 2017 Breakout Inc.
			</div>
	
			<div class="pul-rgt">
			<ul>
				<li><a href="{{url('page/privacy')}}">Privacy policy</a></li>
				<li><a href="{{url('terms')}}">Terms of Use</a></li>
			</ul>
			</div>
            <!--a href="#" class="bottom__rgt pul-rgt" target="_blank">
                <span class="bottom__txt"> </span>
            </a-->

        </div>
		
    </section>

</footer>