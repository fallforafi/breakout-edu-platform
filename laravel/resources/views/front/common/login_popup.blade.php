<!-- Sign In Modal -->
<div id="signIn" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body" id='login-body' >
                <div class="preloader-area">
                    <div class="preloader-area__status">
                        <i class="fa fa-spin"><img src="{{asset('front/images/preloading.png')}}" alt="" /></i>
                    </div>
                </div>

            </div>
        </div>
    </div>   
</div>
