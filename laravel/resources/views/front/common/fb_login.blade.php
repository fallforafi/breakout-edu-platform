
<script>
    function fb_login() {
        FB.login(function (response) {

            if (response.authResponse) {
                //  console.log('Welcome!  Fetching your information.... ');
                //console.log(response); // dump complete info
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

                FB.api('/me?fields=id,email,first_name,last_name,link', function (response) {
                    user_email = response.email;
                    FB.api('/me/picture?type=large&redirect=false', function (r) {

                        checkMember(response.email, response.first_name, response.last_name, 'facebook');
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email,user_friends,public_profile'
        });
    }


</script>
<div id="fb-root"></div>
