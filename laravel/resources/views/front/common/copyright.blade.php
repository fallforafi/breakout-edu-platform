<section class="bottom-area bottom-logo--slide p20 white">
    <div class="container">

        <div class="copy-box text-center col-sm-12">
            <p>{{Config('params.site_name')}} 2016 &copy;</p>
            <div class="btm__img">
                <img src="{{asset('')}}/front/images/favicon.png" alt="{{Config('params.site_name')}}">
            </div>
        </div>
    </div>
</section>
