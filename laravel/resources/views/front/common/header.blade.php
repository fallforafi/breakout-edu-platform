<header>
    <section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white">
        <div class="container">
            @include('front/common/navigation')
        </div>
    </section>
</header>
