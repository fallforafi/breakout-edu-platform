@extends('layout')
<?php
$title = 'Breakout EDU';
$description = 'Breakout EDU is the immersive learning games platform &#124; It\'s time for 
something different';
$keywords = '';
$bgs = array('red', 'yellow', 'green', 'pink', 'blue', 'gray', 'orange', 'teal', 'maroon', 'brown', 'purple');
// echo count($categories);
?>
@include('front/common/meta')
@section('content')

<section class="subj-area clrlist">
    <div class="container">

        <div class="hed underline">
            <h2>Subject Packs</h2>
        </div>
        <div class="subj-main col-sm-12 no-transition broken-image-default">
            @foreach($categories as $category)
            <div class="subj-box col-sm-4 img-cntr ">
                <div class="subj__inr rotateY360--hover">
                    <div class="subj__img ">
                        <img src="<?php echo path();?>uploads/categories/{{$category->image}}" alt="{{$category->name}}" />
                    </div>
                    <div class="subj__title">
                        <a href="{{url('category')}}/{{$category->key}}"><h4>{{$category->name}} Games</h4></a>
                    </div>
                    <div class="subj__games__num __icon">
                        <h4  id='c_{{$category->id}}' ></h4>
                        <span>Games</span>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="actions-area text-center col-sm-12">
                <a href="{{ url('/') }}" class="btn btn-primary">
                    <i class="fa fa-angle-left"></i> BACK
                </a>
            </div>

        </div>
    </div>
</div>
</section>
<script>
    $(document).ready(function () {
        getGames();
    });

    function getGames()
    {
        var categories =<?php echo json_encode($cids) ?>;
        $.each(categories, function (key, value) {

            $.ajax({
                url: "<?php echo url('get-games'); ?>?id=" + value,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $("#c_"+value).html(response.games);
                },
                error: function (xhr, status, response) {
                }
            });
        });
    }
</script>
@endsection