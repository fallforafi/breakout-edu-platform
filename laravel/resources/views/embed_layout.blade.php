<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="description" content="<?php echo Config('params.site_name'); ?> | @yield('description')">
        <meta name="keywords" content="@yield('keywords')" />
        <meta name="apple-mobile-web-app-capable" content="@yield('apple-mobile-web-app-capable')">
        <meta name="apple-mobile-web-app-status-bar-style" content="@yield('apple-mobile-web-app-status-bar-style')">
        <meta property="fb:app_id" content="<?php echo env('FACEBOOK_API_ID'); ?>">
        <meta property="og:site_name" content="<?php echo Config('params.site_name'); ?>">
        <meta property="og:title" content="<?php echo Config('params.site_name'); ?> | @yield('title')">
        <meta property="og:description" content="<?php echo Config('params.site_name'); ?> | @yield('description')">
        <meta property="og:url" content="{{url('/')}}">
        <meta property="og:image" content="{{asset('')}}/front/images/social-share.png">
        <meta property="og:type" content="website">
        <meta name="twitter:title" content="<?php echo Config('params.site_name'); ?> | @yield('title')">       <meta name="twitter:description" content="<?php echo Config('params.site_name'); ?> | @yield('title')">
        <meta name="twitter:url" content="{{url('/')}}">
        <meta name="twitter:image" content="{{asset('')}}/front/images/social-share.png">
        <meta name="google-signin-client_id" content="<?php env('GOOGLE_CLIENT_ID') ?>">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{asset('')}}/front/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <link rel="icon" type="image/png" href="{{asset('front/images/favicon.png')}}">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/stylized.css')}}">
        <link rel="stylesheet" href="{{asset('front/style.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/colorized.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/animate.css')}}">
        <!--link rel="stylesheet" href="{{asset('front/css/slidenav.css')}}" -->
        <link rel="stylesheet" href="{{asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/swiper.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/extralized/bootstrap-datetimepicker.css')}}"/>
        <link href="{{ asset('front/css/selectize.bootstrap3.css') }}" rel="stylesheet">
        <script>
            var startTime = (new Date()).getTime();
        </script>
        <script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
        <script>
                    $(window).load(function () {
                var endTime = (new Date()).getTime();
                var millisecondsLoading = endTime - startTime;
                // Put millisecondsLoading in a hidden form field
                // or Ajax it back to the server or whatever.
                console.log("Fully Loaded: " + millisecondsLoading);
            });
        </script>
        <!--script src="{{asset('front/extralized/daterangepicker.js')}}"></script-->
        <script src="{{asset('front/extralized/bootstrap-datepicker.js')}}"></script>
        <!--datetimepicker-->
        <script type="text/javascript" src="{{asset('front/extralized/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('front/extralized/bootstrap-datetimepicker.js')}}"></script>

        <!--script src="{{asset('front/extralized/scrollreveal.min.js')}}"></script-->
        <script>
                    //	window.scrollReveal = new scrollReveal();
//	window.scrollReveal = new scrollReveal();
        </script>
        <!--script src="{{asset('front/extralized/modernizr.min.js')}}"></script-->
        <script src="{{asset('front/js/css_browser_selector.js')}}" type="text/javascript"></script>

        <!--[if lte IE 8]>
          <script src="{{asset('front/js/jquery1.9.1.min.js')}}"></script>
        <![endif]-->



    </head>
    <body class="transition nav-plusminus slide-navbar slide-navbar--right modal-vcntr--all hdr--sticky-mobile--off embed-area" >


        <div id="preloader" class="preloader">
            <div id="preloader__status">
                <i class="fa fa-spin"><img src="{{asset('front/images/preloading.gif')}}" alt="" /></i>
            </div>
        </div>


        <main id="page-content">
		
		<div class="logo-embed text-center">
		<a href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a></div>
            @yield('content')
			
			
        </main>
        <div id="loading"></div>
        @include('front/common/js')
        <a href="" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
        <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('front/js/viewportchecker.js')}}"></script>
        <script src="{{asset('front/js/kodeized.js')}}"></script>
        <script src="{{asset('front/js/customized.js')}}"></script>
        <script src="{{asset('front/js/swiper.jquery.min.js')}}"></script>
        <script src="https://cdn.quilljs.com/1.3.0/quill.js"></script>



        <script>
            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                slidesPerView: 3,
                slidesPerColumn: 2,
                paginationClickable: true,
                spaceBetween: 30,
                breakpoints: {
                    1024: {slidesPerView: 3, spaceBetween: 30},
                    768: {slidesPerView: 2, spaceBetween: 20},
                    480: {slidesPerView: 1, spaceBetween: 10},
                    320: {slidesPerView: 1, spaceBetween: 10}
                }

            });
        </script>
    </body>
</html>
