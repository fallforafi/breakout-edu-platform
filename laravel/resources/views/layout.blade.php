<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
    <head>
        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="description" content="<?php echo Config('params.site_name'); ?> | @yield('description')">
        <meta name="keywords" content="@yield('keywords')" />
        <meta name="apple-mobile-web-app-capable" content="@yield('apple-mobile-web-app-capable')">
        <meta name="apple-mobile-web-app-status-bar-style" content="@yield('apple-mobile-web-app-status-bar-style')">
        <meta property="fb:app_id" content="<?php echo env('FACEBOOK_API_ID'); ?>">
        <meta property="og:site_name" content="<?php echo Config('params.site_name'); ?>">
        <meta property="og:title" content="<?php echo Config('params.site_name'); ?> | @yield('title')">
        <meta property="og:description" content="<?php echo Config('params.site_name'); ?> | @yield('description')">
        <meta property="og:url" content="{{url('/')}}">
        <meta property="og:image" content="{{asset('')}}/front/images/social-share.png">
        <meta property="og:type" content="website">
        <meta name="twitter:title" content="<?php echo Config('params.site_name'); ?> | @yield('title')">       <meta name="twitter:description" content="<?php echo Config('params.site_name'); ?> | @yield('title')">
        <meta name="twitter:url" content="{{url('/')}}">
        <meta name="twitter:image" content="{{asset('')}}/front/images/social-share.png">
        <meta name="google-signin-client_id" content="<?php env('GOOGLE_CLIENT_ID') ?>">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{asset('')}}/front/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <link rel="icon" type="image/png" href="{{asset('front/images/favicon.png')}}">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/stylized.css')}}">
        <link rel="stylesheet" href="{{asset('front/style.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/colorized.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/swiper.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/extralized/chartist.min.css')}}">
        <link href="{{ asset('front/css/selectize.bootstrap3.css') }}" rel="stylesheet">
        <link href="https://cdn.quilljs.com/1.3.2/quill.snow.css" rel="stylesheet">


        <!-- Include the Quill library -->

        <script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
        <script>
            var startTime = (new Date()).getTime();
        </script>
        <script>
            $(window).load(function () {
            var endTime = (new Date()).getTime();
            var millisecondsLoading = endTime - startTime;
            // Put millisecondsLoading in a hidden form field
            // or Ajax it back to the server or whatever.
            console.log("Fully Loaded: " + millisecondsLoading);
            });
        </script>
        <!--script src="{{asset('front/extralized/daterangepicker.js')}}"></script-->
        <!--datetimepicker-->
        <script type="text/javascript" src="{{asset('front/extralized/moment.min.js')}}"></script>
        <!--script src="{{asset('front/extralized/scrollreveal.min.js')}}"></script-->
        <script>
            //	window.scrollReveal = new scrollReveal();
//	window.scrollReveal = new scrollReveal();
        </script>
        <!--script src="{{asset('front/extralized/modernizr.min.js')}}"></script-->
        <script src="{{asset('front/js/css_browser_selector.js')}}" type="text/javascript"></script>
        <!--[if lte IE 8]>
          <script src="{{asset('front/js/jquery1.9.1.min.js')}}"></script>
        <![endif]-->
        <script src="https://cdn.quilljs.com/1.3.2/quill.js"></script>
        <script>
            window.fbAsyncInit = function () {
            FB.init({
            appId: '<?php echo env("FACEBOOK_API_ID"); ?>',
                    status: true,
                    cookie: true,
                    xfbml: true
            });
            };
            (function (doc) {
            var js;
            var id = 'facebook-jssdk';
            var ref = doc.getElementsByTagName('script')[0];
            if (doc.getElementById(id)) {
            return;
            }
            js = doc.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
            }(document));
        </script>

        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108799875-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108799875-1');
</script>
    </head>
    <body class="transition nav-plusminus slide-navbar slide-navbar--right modal-vcntr--all hdr--sticky-mobile--off">

	
        <div id="preloader" class="preloader">
            <div id="preloader__status">
                <i class="fa fa-spin"><img src="{{asset('front/images/preloading.png')}}" alt="" /></i>
            </div>
        </div>
		
		


        @include('front/common/navigation')
        <main id="page-content">
            @yield('content')
            @include('front/common/footer')

            @if(!isset(Auth::user()->id))
            @include('front/common/login_popup')
            @include('front/common/signup_popup')
            @include('front/common/fb_login')

            @endif
        </main>
		
		
		
		
        <div id="loading"></div>
        @include('front/common/js')


        <a href="" class="scrollToTop"><i class="fa fa-angle-up"></i></a>

        <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('front/js/viewportchecker.js')}}"></script>
        <script src="{{asset('front/js/kodeized.js')}}"></script>
        <script src="{{asset('front/js/customized.js')}}"></script>
        <script src="{{asset('front/js/swiper.jquery.min.js')}}"></script>
        <script>
            var swiper1 = new Swiper('.s1', {
            pagination: '.swiper-pagination1',
                    slidesPerView: 3,
                    slidesPerColumn: 2,
                    slidesPerColumnFill: 'row', slidesPerGroup: 1,
                    paginationClickable: true,
                    preventClicks: false,
                    spaceBetween: 30,
                    breakpoints: {
                    1024: {slidesPerView: 3, spaceBetween: 30},
                            768: {slidesPerView: 2, spaceBetween: 20},
                            480: {slidesPerView: 1, spaceBetween: 10},
                            320: {slidesPerView: 1, spaceBetween: 10}
                    }

            });
            var swiper2 = new Swiper('.s2', {
            pagination: '.swiper-pagination2',
                    slidesPerView: 2,
                    slidesPerColumn: 3,
                    slidesPerColumnFill: 'row', slidesPerGroup: 1,
                    paginationClickable: true,
                    preventClicks: false,
                    spaceBetween: 0,
                    breakpoints: {
                    1024: {slidesPerView: 2, spaceBetween: 30},
                            768: {slidesPerView: 2, spaceBetween: 20},
                            480: {slidesPerView: 1, spaceBetween: 10},
                            320: {slidesPerView: 1, spaceBetween: 10}
                    }

            });
			
			
        </script>
		
		
		
		
		
        @include('front/common/g_login')
        
<script src="https://my.hellobar.com/f252fe8fda3f39d86fce49f1eacb739f1f86c7ba.js" type="text/javascript" charset="utf-8" async="async"></script>
    </body>
</html>
