<?php
$size = Config::get('params.best_image_size');
$required = "required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Title') !!}<span class='red'>*</span>
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('categories') !!}<span class='red'>*</span> (Press Ctrl to select multiple categories)
    {!! Form::select('categories[]', $categories,$gameCategories,['class' => 'form-control','multiple' => 'multiple',$required]) !!}
</div>

<div class="form-group">
    {!! Form::label('Link to Files') !!}
    {!! Form::text('linkToFiles', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group">
    {!! Form::label('Url Key') !!}<span class='red'>*</span>
    {!! Form::text('key', $key , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::checkbox('forPaid',1,false,['id'=>'sale']); !!}
    {!! Form::label('Only for paid users') !!}
</div>

<div class="form-group">
    {!! Form::label('Facilitation Launch Tool Link') !!}
    {!! Form::text('launchTool', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group">
    {!! Form::label('Tags') !!}
    {!! Form::text('tags', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null,array('class'=>'form-control')) !!}
    Best Image Size(<?php echo $size; ?>)
</div>
<br clear='all'/>
<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Video Embeded</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">{!! Form::textarea('videoEmbed', null, ['size' => '105x3','class' => 'form-control']) !!} 
    </div>
</div>


<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Informations<span class='red'>*</span></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('Info Line 1') !!}
            {!! Form::text('infoLine1', null , array('class' => 'form-control') ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Info Line 2') !!}
            {!! Form::text('infoLine2', null , array('class' => 'form-control') ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Info Line 3') !!}
            {!! Form::text('infoLine3', null , array('class' => 'form-control') ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Info Line 4') !!}
            {!! Form::text('infoLine4', null , array('class' => 'form-control') ) !!}
        </div>
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Story</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('story', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Lock Combinations</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('lockCombinations', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Instructions</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('instructions', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Questions</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('questions', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Modifications</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('modifications', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Requirements</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('requirements', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Note</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('note', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <button type="submit" name="isPublished" id="isPublished" value="1" class="btn btn-primary btn-block btn-flat">Save & Publish</button>
    </div>
    <div class="col-sm-4">
        <button type="submit" name="isPublished" id="isPublished" value="0" class="btn btn-primary btn-block btn-flat">Save Only</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/games')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>
