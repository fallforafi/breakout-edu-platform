<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="box-header with-border">
    <h3 class="box-title">( Total Games : {{ count($model) }} )</h3>
    <div class="box-tools">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> 
    </div>
</div>
<div class="box-body">
    <?php if (count($model) > 0) { ?>

        <ul class="products-list product-list-in-box">
            <table class="table" id="order_table">
                <thead>
                    <tr >
                        <th>#</th>
                        <th>Title</th>
                        <th>Game Category</th>
                        <th>Publish</th>
                        <th>For Paid</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($model as $row)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php if($row->image!=''){ ?>
                            <img src="<?php echo path(); ?>uploads/games/thumbnail/<?php echo $row->image?>" width="50" />
                            <br/>
                            <?php } ?>
                            <?php echo $row->title; ?>
                        </td>                        
                        <td><?php echo ucfirst($row->category_name); ?></td>
                        <td>
                            @if($row->isPublished == 0)
                            <a href="{{url('admin/game/publish/'.$row->id)}}" class="btn btn-success">Publish</a>
                            @else
                            <a href="{{url('admin/game/unpublish/'.$row->id)}}" class="btn btn-danger">Unpublish</a>
                            @endif
                        </td>
                        <td>
                            @if($row->forPaid == 0)
                            <a href="{{url('admin/game/for-paid/enable/'.$row->id)}}" class="btn btn-success">Enable</a>
                            @else
                            <a href="{{url('admin/game/for-paid/disable/'.$row->id)}}" class="btn btn-danger">Disable</a>
                            @endif
                        </td>
                        <td><?php echo date('d/m/Y', strtotime($row->created_at)); ?></td>
                        <td><?php echo date('d/m/Y', strtotime($row->updated_at)); ?></td>
                        <td>
                            <a href="{{ url('admin/games/edit/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="games/delete/<?php echo $row->id ?>"><i class="fa fa-trash"></i> </button>
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                    @include('admin/commons/delete_modal')
                </tbody>

            </table>
            <?php echo $model->appends(Input::query())->render(); ?>
        </ul>


    <?php } else {
        ?>
        <div class="">
            No Data found. . .
        </div>
    </div>
<?php }
?>
<script>
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>


