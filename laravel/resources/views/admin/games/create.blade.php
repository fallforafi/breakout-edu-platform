@extends('admin/admin_template')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Add New Game</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(array( 'class' => 'form','url' => 'admin/games/insert','files' => true)) !!}
                @include('admin.games.form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection