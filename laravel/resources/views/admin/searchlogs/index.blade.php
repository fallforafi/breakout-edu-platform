@extends('admin/admin_template')
@section('content')

<!-- Main row -->
<div class="row">

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Search Terms ( Total : {{ count($model) }} ) </h3>
            </div>
            <div class="box-body">
                <?php if (count($model) > 0) { ?>

                    <ul class="products-list product-list-in-box">
                        <table class="table" id="order_table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Term</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($model as $row)
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $row->term; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row->created_at)); ?></td>
                                    <td>
                                        <a class="btn btn-danger delete" href="{{ url('admin/searchlogs/delete/') }}/<?php echo $row->id ?>" class="btn btn-primary"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>

                        </table>
                    </ul>


                <?php } else {
                    ?>
                    <div class="">
                        No Data found. . .
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
</div>
</div>
<!-- /.row -->	

@endsection
