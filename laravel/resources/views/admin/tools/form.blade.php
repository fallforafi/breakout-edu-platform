<?php
$required = "required";
?>
@include('admin/commons/errors')

<div class="form-group">
    {!! Form::label('Name') !!}
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}

</div>

<div class="form-group">
    {!! Form::label('url') !!}
    {!! Form::text('url', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null, 
    array('class'=>'form-control')) !!}
</div>

<div class="form-group">
    <div class="col-sm-4">
        <button type="submit" value="tools" class="btn btn-primary btn-block btn-flat">Save</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/tools')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>