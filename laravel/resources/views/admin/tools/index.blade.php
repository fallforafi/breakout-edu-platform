@extends('admin/admin_template')
@section('content')

<!-- Main row -->
<div class="row">

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Categories( Total : {{ count($model) }} ) </h3>
            </div>
            <div class="box-body">
                <ul class="products-list product-list-in-box">

                    <?php $i = 1; ?>
                    @foreach ($model as $row)

                    <?php
                    $color = ($i % 2 == 0 ? 'success' : 'info');
                    ?>

                    <li class="item">
                        
                        <div class="product-img">
                            <img src="{{ asset('uploads/tools/thumbnail')}}/<?php echo $row->image; ?>" alt="<?php echo $row->title; ?>" />
                            <br clear="all" />
                            <a class="btn btn-warning" href="tools/edit/<?php echo $row->id ?>">Edit</a>
                            <!--
                            <a  class="btn btn-danger" href="tools/delete/<?php echo $row->id ?>">Delete</a>
                            -->
                        </div>

                        <div class="product-info">
                           <span class="product-description">
                                <?php echo $row->title; ?>
                           </span>
                        </div>
                    </li>
                    <!-- /.item -->
                    <?php $i++; ?>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->	

@endsection
