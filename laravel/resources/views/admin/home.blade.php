@extends('admin/admin_template')

@section('content')
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div> <!-- end .flash-message -->
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {{count($totalUsers)}}
                    </h3>
                    <p>
                        User Registrations
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{ url('admin/users') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{count($totalGames)}}
                    </h3>
                    <p>
                        Total Games
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-plus"></i>
                </div>
                <a href="{{ url('admin/games') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        {{count($totalMiniGames)}}
                    </h3>
                    <p>
                        Total Digital Games
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ url('admin/digital-games') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        {{count($totalCategories)}}
                    </h3>
                    <p>
                        Total Categories
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-list"></i>
                </div>
                <a href="{{ url('admin/categories') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div><!-- /.row -->
    <br><br>
    <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Recently Added Users</h3>
                    <div class="box-tools">
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    @if(count($recentUsers) > 0)
                    <table class="table table-hover">
                        <tbody><tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            @foreach($recentUsers as $users)
                            <tr>
                                <td><a href="<?php echo url('admin/user/' . $users->id); ?>">{{$users->id}}</a></td>
                                <td><a href="<?php echo url('admin/user/' . $users->id); ?>">{{$users->firstName}} {{$users->lastName}}</a></td>
                                <td>{{date('d/m/Y',strtotime($users->created_at))}}</td>
                                <?php
                                if ($users->status == 1) {
                                    $status = 'success';
                                    $text = 'Approved';
                                } else {
                                    $status = 'danger';
                                    $text = 'Disapproved';
                                }
                                ?>
                                <td><span class = "label label-{{$status}}">{{$text}}</span></td>
                            </tr>
                            @endforeach
                        </tbody></table>
                </div><!--/.box-body -->
                @else
                <div class="col-sm-6">
                    <h5>No data found. . . . </h5>
                </div>
                @endif
            </div><!--/.box -->
        </div>
        <div class = "col-xs-4">
            <div class = "box">
                <div class = "box-header">
                    <h3 class = "box-title">Recently Added Games</h3>
                    <div class = "box-tools pull-right">
                    <!--<button type = "button" class = "btn btn-box-tool" data-widget = "collapse"><i class = "fa fa-minus"></i> </button> -->

                    </div>
                </div><!--/.box-header -->
                <div class = "box-body table-responsive no-padding">
                    @if(count($recentGames) > 0)
                    <table class = "table table-hover">
                        <tbody><tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Date</th>
                            </tr>
                            @foreach($recentGames as $games)
                            <tr>
                                <td><a href="<?php echo url('admin/games/edit/' . $games->id); ?>">{{$games->id}}</a></td>
                                <td><a href="<?php echo url('admin/games/edit/' . $games->id); ?>">{{$games->title}}</a></td>
                                <td>{{date('d/m/Y',strtotime($games->created_at))}}</td>
                            </tr>
                            @endforeach
                        </tbody></table>
                </div><!--/.box-body -->
                @else
                <div class="col-sm-6">
                    <h5>No data found. . . . </h5>
                </div>
                @endif
            </div><!--/.box -->
        </div>
        <div class = "col-xs-4">
            <div class = "box">
                <div class = "box-header">
                    <h3 class = "box-title">Recently Added Digital Games</h3>
                    <div class = "box-tools pull-right">
                    <!--<button type = "button" class = "btn btn-box-tool" data-widget = "collapse"><i class = "fa fa-minus"></i> </button> -->

                    </div>
                </div><!--/.box-header -->
                <div class = "box-body table-responsive no-padding">
                    @if(count($recentMiniGames) > 0)
                    <table class = "table table-hover">
                        <tbody><tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Date</th>
                            </tr>
                            @foreach($recentMiniGames as $d_games)
                            <tr>
                                <td><a href="<?php echo url('admin/digital-game/' . $d_games->id); ?>">{{$d_games->id}}</a></td>
                                <td><a href="<?php echo url('admin/digital-game/' . $d_games->id); ?>">{{$d_games->title}}</a></td>
                                <td>{{date('d/m/Y',strtotime($d_games->created_at))}}</td>
                            </tr>
                            @endforeach
                        </tbody></table>
                </div><!--/.box-body -->
                @else
                <div class="col-sm-6">
                    <h5>No data found. . . . </h5>
                </div>
                @endif
            </div><!--/.box -->
        </div>
    </div>
</section>   
@endsection