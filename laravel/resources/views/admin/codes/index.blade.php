@extends('admin/admin_template')
@section('content')

<!-- Main row -->
<div class="row">

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Access Codes( Total : {{ $count }} ) </h3>
            </div>
            <div class="box-body">
                <?php if (count($model) > 0) { ?>

                    <ul class="products-list product-list-in-box">
                        <table class="table" id="order_table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>Redeemed</th>
                                    <th>Redeemed To</th>
                                    <th>Redeemed Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($model as $row)
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $row->code; ?>
                                     <input value='<?php echo $row->code; ?>' type="hidden" name='<?php echo $i; ?>'  id='<?php echo $i; ?>' class="btn btn-default" />   
                                    <input value='Copy' type="button" name='copyurl' onclick="copy('#<?php echo $i; ?>')" id='copyurl' class="btn btn-info" />
                                    </td>
                                    <td><?php echo $row->isUsed==1?'Yes':'No'; ?></td>
                                    <td><?php echo $row->firstName; ?> <?php echo $row->lastName; ?> (<?php echo $row->email; ?>)</td>
                                    <td><?php echo $row->isUsed==1 ? date("d M Y", strtotime($row->created_at)) : ''; ?></td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                                
                            </tbody>

                        </table>
                        <?php echo $model->appends(Input::query())->render(); ?>
                    </ul>


                <?php } else {
                    ?>
                    <div class="">
                        No Data found. . .
                    </div>
                </div>
            <?php }
            ?>

        </div>
    </div>
</div>
<script>
    function copy(element)
    {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>
<!-- /.row -->	

@endsection
