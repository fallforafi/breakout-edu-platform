<?php
$required = "required";
?>
@include('admin/commons/errors')


<div class="form-group">
    {!! Form::label('Code') !!}
    {!! Form::text('code', null , array('class' => 'form-control',$required) ) !!}

</div>

<div class="form-group">
    <div class="col-sm-4">
        <button type="submit" value="products" class="btn btn-primary btn-block btn-flat">Save</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/codes')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>