<?php
$size = Config::get('params.best_image_size') ;
$required = "required";

if(isset($category->id)){
    $string=$category->parent_id;
}else{
    $string=0;
}
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Parent Category') !!}
    {!! Form::select('parent_id', $categories,$string,array('class' => 'form-control',$required)) !!}
</div>
<div class="form-group">
    {!! Form::label('Name') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}

</div>
<!--
<div class="form-group">
    {!! Form::label('body') !!}
    {!! Form::textarea('description', null, ['size' => '105x25','class' => 'form-control ckeditor',$required]) !!} 
</div>
<div class="form-group">
    {!! Form::label('teaser') !!}
    {!! Form::textarea('teaser', null, ['size' => '105x3','class' => 'form-control']) !!} 

</div>
-->
<div class="form-group">
    {!! Form::label('Url Keys') !!}
    {!! Form::text('key', $key , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null,array('class'=>'form-control')) !!}
    Best Image Size(<?php echo $size ;?>)
</div>
<div class="form-group">
    {!! Form::label('Cover Image') !!}
    {!! Form::file('coverImage', null,array('class'=>'form-control')) !!}
    Best Image Size(1024 x 480 )
</div>
<div class="form-group">
    <div class="col-sm-4">
        <button type="submit" value="categories" class="btn btn-primary btn-block btn-flat">Save</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/categories')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>