@extends('admin/admin_template')
@section('content')
<!-- Main row -->
<div class="row">

    <div class="col-md-12">
        @include('admin/commons/errors')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total Categories : {{count($categories)}}  )</h3>
                <div class="box-tools">
                    <a href="{{ url('admin/categories/create') }}" class="btn btn-flat btn-primary">Insert</a>
<!--                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> -->
                </div>
                <br>
            </div>
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php if (count($categories) > 0) { ?>
                        <table class="table" id="order_table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $item)
                                
                                <?php
                                ?>
                                <tr>
                                    <td>
                                        <img src="<?php echo path(); ?>uploads/categories/thumbnail/<?php echo $item->image?>" width="50"/>
                                        
                                        @if($item->children->count() > 0)                            
                                        @foreach($item->children as $subcat)
                                        {{ $subcat->name }} <strong>></strong> <small>{{ $item->name }}</small>
                                        @endforeach                            
                                        @else
                                        {{ $item->name }}
                                        
                                        
                                        @endif
                                    </td>
                                    <td><a href="<?php echo url('admin/categories/edit/' . $item->id); ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="categories/delete/{{$item->id}}"><i class="fa fa-trash"></i> </button>
                                    </td>
                                </tr>
                                @endforeach
                                @include('admin/commons/delete_modal')
                            </tbody>
                        </table>
                    <?php echo  $categories->render();
                    } else {
                        ?>
                        <div class="">
                            No Data found. . .
                        </div>

                    <?php }
                  
                    ?>

                </ul>                 
            </div>    
            <script>
                jQuery('.delete').click(function ()
                {
                    $('#closemodal').attr('href', $(this).data('link'));
                });
            </script>
        </div>
    </div>
</div>
<!-- /.row -->	

@endsection
