@extends('admin/admin_template')

@section('content')
<style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
    #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; }
    #sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<div class="row">
    <div class="col-xs-12">
        @include('admin/commons/errors')
    </div>
</div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">

        <div class="box box-primary" id="listing">
            <div class="box-header with-border">
                <h3 class="box-title">( Total : {{ count($model) }} )</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> 
                </div>
            </div>
            <div class="box-body">
                <?php if (count($model) > 0) { ?>

                    <ul id="sortable">
                        <?php $i = 1; ?>
                        @foreach ($model as $row)
                        <li data-id="<?php echo $row->id; ?>" class="ui-state-default"><span class="fa fa-arrows-v "></span><?php echo $row->name; ?></li>
                        <?php $i++; ?>
                        @endforeach
                    </ul>
                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                <?php } else {
                    ?>
                    <div class="">
                        No Data found. . .
                    </div>
                </div>
            <?php }
            ?>

        </div>

    </div>
</div>
<script>
    $(function () {
        $("#sortable").sortable({
            update: function () {
                // alert("asda asdasd");
                var list = new Array();
                $('#sortable').find('.ui-state-default').each(function () {
                    var id = $(this).attr('data-id');
                    list.push(id);
                });
                // var data = JSON.stringify(list);
                $.ajax({
                    url: '<?php echo url('admin/categories/sorting/update'); ?>', // server url
                    type: 'GET', //POST or GET 
                    data: {list: list}, // data to send in ajax format or querystring format
                    datatype: 'json',
                    success: function (message) {
                        // alert(message);
                    }
                });
            }
        });       // $("#sortable").disableSelection();
    });
</script>
@endsection
