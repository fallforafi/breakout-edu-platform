<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Admin Links Start-->
            <?php if (Auth::user()->role->role == 'admin') { ?>
                <!--
                                <li class="treeview">
                                    <a href="{{ url('/admin-users') }}">
                                        <i class="fa fa-dashboard"></i> <span>Admin Users</span> <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                
                                        <li class="active"><a href="{{ url('admin/admin-users') }}"><i class="fa fa-circle-o"></i> <span>List Admins</span></a></li>
                                        <li><a href="{{ url('admin/admin-users/add') }}"><i class="fa fa-circle-o"></i> Add Admin User</a></li>
                                    </ul>
                                </li> -->
                <li>
                    <a href="{{ url('admin') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ url('admin/users') }}">
                        <i class="fa fa-dashboard"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <li class="active"><a href="{{ url('admin/users') }}"><i class="fa fa-circle-o"></i> <span>List Users</span></a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ url('admin/digital-games') }}">
                        <i class="fa fa-dashboard"></i> <span>Digital Games</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <li class="active"><a href="{{ url('admin/digital-games') }}"><i class="fa fa-circle-o"></i> <span>List Digital Games</span></a></li>

                        <li class="active"><a href="{{ url('admin/digital-game/set/featured') }}"><i class="fa fa-circle-o"></i> <span>Set Featured Games Order</span></a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ url('admin/games') }}">
                        <i class="fa fa-dashboard"></i> <span>Games</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/games') }}"><i class="fa fa-circle-o"></i> List Games</a></li>
                        <li><a href="{{ url('admin/games/create') }}"><i class="fa fa-circle-o"></i> Add Game</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ url('admin/outside-games') }}">
                        <i class="fa fa-dashboard"></i> <span>Outside-Games</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/outside-games') }}"><i class="fa fa-circle-o"></i> List Outside-Games</a></li>
                        <li><a href="{{ url('admin/outside-game/create') }}"><i class="fa fa-circle-o"></i> Add Outside-Game</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ url('admin/codes') }}">
                        <i class="fa fa-dashboard"></i> <span>Codes</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <li class="active"><a href="{{ url('admin/codes') }}"><i class="fa fa-circle-o"></i> <span>List All Codes</span></a></li>
                        <li><a href="{{ url('admin/codes/create') }}"><i class="fa fa-circle-o"></i> Add New Code</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> Search Logs <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/searchlogs') }}"><i class="fa fa-circle-o"></i> List Logs</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i> List Categories</a></li>
                        <li><a href="{{ url('admin/categories/create') }}"><i class="fa fa-circle-o"></i> Add Category</a></li>
                        <li><a href="{{ url('admin/categories/sorting') }}"><i class="fa fa-circle-o"></i> Set Categories Order</a></li>
                    </ul>
                </li>
                <!--                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-dashboard"></i> <span>Games</span> <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu" style="display: none;">
                                        <li><a href="{{ url('admin/games') }}"><i class="fa fa-circle-o"></i> List Games</a></li>
                                        <li><a href="{{ url('admin/games/create') }}"><i class="fa fa-circle-o"></i> Add Game</a></li>
                                    </ul>
                                </li>-->
                <!--
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Facilitation Tools</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/tools') }}"><i class="fa fa-circle-o"></i> List Tools</a></li>
                        <li><a href="{{ url('admin/tools/create') }}"><i class="fa fa-circle-o"></i> Add Tool</a></li>
                    </ul>
                </li>
                -->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Content Blocks</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/content?type=page') }}"><i class="fa fa-circle-o"></i> Pages</a></li>
                       <!-- <li><a href="{{ url('admin/content?type=email') }}"><i class="fa fa-circle-o"></i> Emails</a></li>
                        <li><a href="{{ url('admin/content?type=block') }}"><i class="fa fa-circle-o"></i> Blocks</a></li>-->
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Invitees</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="{{ url('admin/uploads/') }}"><i class="fa fa-circle-o"></i> Uploads</a></li>
                        <li><a href="{{ url('admin/uploads/create') }}"><i class="fa fa-circle-o"></i> Upload CSV File</a></li>
                    </ul>
                </li>


            <?php } ?>
        </ul> 
        <!--   
      <ul class="sidebar-menu">
          <li class="header">Blog</li>
                 <li class="treeview">
                          <a href="#">
                              <i class="fa fa-dashboard"></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
                          </a>
                          <ul class="treeview-menu" style="display: none;">
                              <li><a href="{{ url('admin/blog/categories') }}"><i class="fa fa-circle-o"></i> List Categories</a></li>
                              <li><a href="{{ url('admin/blog/categories/create') }}"><i class="fa fa-circle-o"></i> Add Category</a></li>
                          </ul>
                      </li>
                      <li class="treeview">
                          <a href="#">
                              <i class="fa fa-dashboard"></i> <span>Posts</span> <i class="fa fa-angle-left pull-right"></i>
                          </a>
                          <ul class="treeview-menu" style="display: none;">
                              <li><a href="{{ url('admin/blog/posts') }}"><i class="fa fa-circle-o"></i> List Posts</a></li>
                              <li><a href="{{ url('admin/blog/posts/create') }}"><i class="fa fa-circle-o"></i> Add New Post</a></li>
                          </ul>
                      </li>


      </ul>
        -->
    </section>
</aside>