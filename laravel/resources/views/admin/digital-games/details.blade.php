@extends('admin/admin_template')

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('admin/commons/errors')
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Digital Game</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="form-group">
                    <label>Title:</label>
                    <span>{!! $model->title !!} </span>      
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <span>{!! $model->description !!}</span>      
                </div>
                <div class="form-group">
                    <label>Play Code:</label>
                    <span>{!! $model->playCode !!} </span>      
                </div>   
                <div class="form-group">
                    <label>Play Count:</label>
                    <span>{!! $model->playCount !!} </span>      
                </div>
                <div class="form-group">
                    <label>Time:</label>
                    <span>{!! $model->time !!} </span>      
                </div>
                <div class="form-group">
                    <label>Key:</label>
                    <span>{!! $model->key !!} <a tabindex="_blank" href="{{url('game/digital/'.$model->key)}}" class="btn btn-success">Go To Game</a> </span>      
                </div>
                <div class="form-group">
                    <label>Game Type:</label>
                    <span>{!! ucfirst($model->gameType) !!}</span>      
                </div>
                <div class="form-group">
                    <label>PlayCode Status:</label>
                    @if($model->hasPlayCode == 0)
                    <a href="{{url('admin/digital-game/playcode/enable/'.$model->id)}}" class="btn btn-success">Enable</a>
                    @else
                    <a href="{{url('admin/digital-game/playcode/disable/'.$model->id)}}" class="btn btn-danger">Disable</a>
                    @endif
                </div>
                <div class="form-group">
                    <label>Featured Status:</label>
                    @if($model->isFeatured == 0)
                    <a href="{{url('admin/digital-game/featured/enable/'.$model->id)}}" class="btn btn-success">Enable</a>
                    @else
                    <a href="{{url('admin/digital-game/featured/disable/'.$model->id)}}" class="btn btn-danger">Disable</a>
                    @endif
                </div>
                <div class="form-group">
                    <label>Created At :</label>
                    <span>{!! $model->created_at !!}</span>      
                </div>
                <div class="form-group">
                    <label>Created By:</label>
                    <span>{!! $model->email !!} ({!! $model->firstName !!} {!! $model->firstName !!})</span>      
                </div>
                <a href="{{url('admin/digital-games')}}" class="btn btn-warning pull-right">Back</a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">( Stats : {{ count($results) }} )</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> 
                </div>
            </div>

            <div class="box-body">
                <?php if (count($results) > 0) { ?>

                    <ul class="products-list product-list-in-box">
                        <table class="table" id="order_table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Completed</th>
                                    <th>Time Remaing</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($results as $row)
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->completed; ?> % </td>
                                    <td><?php echo $row->time; ?></td>
                                    <td><?php echo $row->created_at; ?></td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>

                        </table>
                    </ul>


                <?php } else {
                    ?>
                    <div class="">
                        No Data found. . .
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
@endsection
