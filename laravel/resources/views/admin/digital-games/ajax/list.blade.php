<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="box-header with-border">
    <h3 class="box-title">( Total Digital Games : {{ count($model) }} )</h3>
    <div class="box-tools">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> 
    </div>
</div>
<div class="box-body">
    <div class="col-sm-12 pull-right">               
        <a href="{{ URL::to('admin/digital-games/download/csv') }}"><button class="btn btn-success pull-right"><i class="fa fa-download"></i> Download CSV</button></a>
    </div>
    <?php if (count($model) > 0) { ?>

        <ul class="products-list product-list-in-box">
            <table class="table" id="order_table">
                <thead>
                    <tr >
                        <th>#</th>
                        <th>Title</th>
                        <th>Created By</th>
                        <th>Play Code</th>
                        <th>Play Count</th>
                        <th>Key</th>
                        <th>Featured</th>
                        <th>PlayCode Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($model as $row)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><a href="{{ url('admin/digital-game/'.$row->id) }}"><?php echo $row->title; ?></a></td>
                        <td><?php echo $row->email; ?> (<?php echo $row->firstName; ?> <?php echo $row->lastName; ?>)</td>
                        <td><?php echo $row->playCode; ?></td>
                        <td><?php echo $row->playCount; ?></td>
                        <td><?php echo $row->key; ?></td>
                        <td>@if($row->isFeatured == 0) <i class="fa fa-remove"></i> @else <i class="fa fa-check"></i> | <a href="{{url('admin/digital-game/image')}}/{{$row->id}}"> <i class="fa fa-upload"></i> image </a> @endif</td>

                        <td>@if($row->hasPlayCode == 0) <i class="fa fa-remove"></i> @else <i class="fa fa-check"></i> @endif</td>

                        <td>
                            <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="digital-game/delete/<?php echo $row->id ?>"><i class="fa fa-trash"></i> </button>
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                    @include('admin/commons/delete_modal')
                </tbody>

            </table>
            <?php echo $model->appends(Input::query())->render(); ?>
        </ul>


    <?php } else {
        ?>
        <div class="">
            No Data found. . .
        </div>
    </div>
<?php }
?>
<script>
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>


