@extends('admin/admin_template')

@section('content')
<div class="row">
    <div class="col-xs-12">
        @include('admin/commons/errors')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            <form class="form" role="form" id="filter">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('Title') !!}
                                {!! Form::text('title',(isset($title))?$title:'', array('class' => 'form-control', 'id' => 'title') ) !!}
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="filter">Filter by GameType</label>
                            <select class="form-control" name="gameType" id="gameType">
                                <option value="" selected>All </option>
                                <option value="individual" {{ (isset($gameType) && $gameType == 'individual')?'selected':'' }}>Individual</option>
                                <option value="connected" {{ (isset($gameType) && $gameType == 'connected')?'selected':'' }}>Connected</option>
                            </select>
                        </div>                                          
                        <div class='clearfix'></div>
                        <input type="hidden" class="form-control" name="page" id="page" value="<?php echo $page; ?>"/>
                        <div class='clearfix'></div>
<!--                        <div class="form-group col-sm-6">
                            <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-search"></i> Search</button>
                        </div>-->
                        <div class=" form-group col-sm-6 col-sm-offset-3">
                            <a href="{{ url('admin/digital-games') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">

        <div class="box box-primary" id="listing">

        </div>

    </div>
</div>
<script>
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>
<script>
    $(document).ready(function () {
        miniGamesListing();
    });
    $("form").submit(function (e) {
        miniGamesListing();
        e.preventDefault();

    });
    $("form").change(function (e) {
        miniGamesListing();
        e.preventDefault();
    });
    $("#title").keyup(function (e) {
        miniGamesListing();
    });

    function miniGamesListing() {
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('admin/digital-games/listing'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                $('#listing').html(response);
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
@endsection
