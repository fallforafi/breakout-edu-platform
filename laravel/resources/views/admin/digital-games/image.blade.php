@extends('admin/admin_template')

@section('content')
<?php
$size = Config::get('params.best_image_size');
$required = "required";
?>
@include('admin/commons/errors')
{!! Form::model($model, ['class' => 'form','url' => ['admin/digital-game/saveimage', $model->id], 'method' => 'post','files' => true]) !!}


<div class="row">
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Add Image</h3>
            </div>
            <div class="box-body">
                
                <?php if($model->image){?>
                <div class="col-md-12">
                    <img src="<?php echo path(); ?>uploads/games/thumbnail/{{$model->image}}" width="80" />
                   
                </div>
                <?php } ?>
                <div class="form-group">
                    {!! Form::label('image') !!}
                    {!! Form::file('image', null,array('class'=>'form-control')) !!}
                    Best Image Size(<?php echo $size; ?>)
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <button type="submit" value="games" class="btn btn-primary btn-block btn-flat">Save</button>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('admin/digital-games')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection