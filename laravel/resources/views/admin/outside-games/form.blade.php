<?php
$size = Config::get('params.best_image_size');
$required = "required";
//$required = "";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Title') !!}<span class='red'>*</span>
    {!! Form::text('title', null , array('class' => 'form-control','placeholder' => 'Add title',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('categories') !!}<span class='red'>*</span> (Press Ctrl to select multiple categories)
    {!! Form::select('categories[]', $categories,$gameCategories,['class' => 'form-control','multiple' => 'multiple',$required]) !!}
</div>

<div class="form-group">
    {!! Form::label('Tags') !!}<span class='red'>*</span>
    {!! Form::text('tags', null , array('class' => 'form-control','placeholder' => 'Add comma`s separated tags',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('Url Keys') !!}
    {!! Form::text('key', $key , array('class' => 'form-control','placeholder' => 'Add your url key',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('Facilitation Launch Tool Link') !!}
    <input type="url" name="launchTool" class="form-control" required="required" placeholder="https://www.example.com" value="{{isset($model->launchTool)? $model->launchTool:''}}">
</div>

<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null,array('class'=>'form-control')) !!}
    Best Image Size(<?php echo $size; ?>)
</div>
<br clear='all'/>
 {!! Form::label('Description') !!}
<div class="box box-primary">
    <div class="box-header">
<!--        <h3 class="box-title">Description<span class='red'></span></h3>-->
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
        </div>
    </div>
    <div class="box-body">
        {!! Form::textarea('note', null, ['size' => '105x3','class' => 'form-control']) !!} 
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <button type="submit" value="games" class="btn btn-primary btn-block btn-flat">Save</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/outside-games')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>