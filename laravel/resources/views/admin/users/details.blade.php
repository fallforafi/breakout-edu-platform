@extends('admin/admin_template')

@section('content')
<style>
    img {
        height: 260px;
        width: 100%;
    }
</style>

<div class="row">
    <div class="col-md-12">
        @include('admin/commons/errors')
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">User's Information </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>

                </div>
            </div>
            <?php
            $user = $data[0];
            ?>        
            @if($user->status == '1')
            <div class="box-body bg-success">
                @else
                <div class="box-body bg-danger">
                    @endif

                    <div class="row">
                        <div class="col-sm-12">

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>First Name :</td>
                                        <td>{{ $data[0]->firstName }}</td>
                                    </tr>
                                    <tr>
                                        <td>Last Name :</td>
                                        <td>{{ $data[0]->lastName }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email :</td>
                                        <td>{{ $data[0]->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Login Count :</td>
                                        <td>{{ $data[0]->loginCount }}</td>
                                    </tr>

                                    <?php if (count($code) > 0) { ?>
                                        <tr>
                                            <td>Code :</td>
                                            <td>{{ $code->code }}</td>
                                        </tr>
                                        <tr>
                                            <td>Expires :</td>
                                            <td>{{ $code->expires }}</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <br>
                    <div class="col-sm-12">
                        <a href="{{ url('admin/users') }}" class="btn btn-default">Back</a>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">( Total Digital Games : {{ count($model) }} ) </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>

                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <table class="table" id="order_table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Game Type</th>
                                    <th>PlayCode Status</th>
                                    <th>Featured Status</th>
<!--                                    <td></td> -->

                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($model as $row)
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a href="{{ url('admin/digital-game/'.$row->id) }}">{{ $row->title }}</a></td>
                                    <td><a href="{{ url('admin/digital-game/'.$row->id) }}">{{ $row->description }}</a></td>
                                    <td><?php echo ucfirst($row->gameType); ?></td>
                                    <td>@if($row->hasPlayCode == 0) <i class="fa fa-remove"></i> @else <i class="fa fa-check"></i> @endif</td>
                                    <td>@if($row->isFeatured == 0) <i class="fa fa-remove"></i> @else <i class="fa fa-check"></i> @endif</td>
<!--                                    <td>
                                        <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('admin/digital-game/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button></td>-->
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                                @include('admin/commons/delete_modal')

                            </tbody>

                        </table>
                        <input type="hidden" name="page" id="page" value="{{$page}}">
                        <?php echo $model->render(); ?>

                    </ul>
                </div>

            </div>
        </div>
    </div>
    <script>
        jQuery('.delete').click(function ()
        {
            $('#closemodal').attr('href', $(this).data('link'));
        });
    </script>
    @endsection
