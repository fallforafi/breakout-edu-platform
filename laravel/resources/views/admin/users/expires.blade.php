@extends('admin/admin_template')

@section('content')

<?php
$required = 'required';
?>

<div class="row">
    <div class="col-md-12">
        @include('admin/commons/errors')
        <!-- Horizontal Form -->

        <!-- /.box -->
        <!-- general form elements disabled -->
        <div class="box box-warning">

            <div class="box-header with-border">
                <h3 class="box-title">Set User Expiry Date</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($user, ['class' => 'form','url' => 'admin/user/updateexpirydate/'.$user_id, 'method' => 'post']) !!}

                <div class="form-group col-sm-8">

                    <div class="col-sm-12 pl0">
                        {!! Form::label('expires', 'Expiry Date*') !!}
                    </div>

                    <div class="col-sm-3 pl0">
                        {!! Form::label('expires', 'Date*') !!}
                        {!! Form::selectRange('date',1,31,null,['class' => 'form-control',$required]) !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::label('expires', 'month*') !!}
                        {!! Form::selectMonth('month',null, ['class' => 'form-control',$required]) !!}
                    </div>

                    <div class="col-sm-4 pr0">
                        {!! Form::label('expires', 'year*') !!}
                        {!! Form::selectRange('year',date('Y')+10,date('Y')-2,null,['class' => 'form-control',$required])!!}
                    </div>
                    <br clear="all" />
                    <br />
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" value="users" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                        <div class="col-sm-4">
                            <a href="{{ url('admin/users')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
                        </div>
                    </div>

                </div>

                {!! Form::close() !!}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection