<?php
$required = "required";
?>

<div class="form-group">
    {!! Form::label('First Name') !!}
    {!! Form::text('firstName', $user->firstName , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('Last Name') !!}
    {!! Form::text('lastName', $user->lastName , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    {!! Form::label('email') !!}
    {!! Form::text('email', $user->email , array('class' => 'form-control','readonly') ) !!}
</div>
<?php
if($user->id!=1){
?>
<div class="form-group">
    {!! Form::label('Role') !!}<span class='red'>*</span>
    {!! Form::select('role_id', $roles,$user->role_id,['class' => 'form-control',null,'name'=>'role_id']) !!}
</div>
<?php } ?>
<div class="form-group">
    <div class="col-sm-4">
        <input type="hidden" name="id" value="{{ $user_id }}">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
    </div>
    <div class="col-sm-4">
        <a href="{{ url('admin/users')}}" class="btn btn-warning btn-block btn-flat">Cancel</a>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".select").select2();
    });
</script>