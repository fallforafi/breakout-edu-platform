<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>

        <div style="margin-bottom:10px;"><a href="{{ URL::to('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="logo" style="width:200px;"></a>


            <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
                <p style="margin-bottom:0px;">
                    Dear <?php echo $name;?>,
                </p>
                <p style="margin-bottom:0px;">Thank you for your purchase and welcome to Breakout EDU!

                    This email will help you get started on the Breakout EDU Platform.

                    The Breakout EDU Platform is where you will be able to access all of the Subject Pack games
                    as well as create and share Breakout EDU Digital games.

                    To get started, each user will need to activate an account. Please follow the steps below.
                </p>
                <p style="margin-bottom:0px;"> 1) Go to <a href="{{url('/')}}" target="_blank">platform.breakoutedu.com</a> in your browser and click "Sign Up" on the upper right</p>
                <p style="margin-bottom:0px;"> 2) Fill out your user information.</p>

                <p style="margin-bottom:0px;"> 3) Provide one of the Access Codes below (please note that each code works only for one individual account
                </p>

                <ul style="
                    list-style-type: none;
                    padding-left: 0;
                    ">
                        <?php foreach ($codes as $code) { ?>

                        <li style="
                            font-size: 20px;
                            font-weight: 100;
                            font-family: monospace;
                            border: 1px solid #ddd;
                            display: inline-block;
                            padding: 10px 15px;
                            "><?php echo $code; ?></li>

                    <?php } ?>
                </ul>
                <p style="margin-bottom:0px;">If you are a school or district and wish to provide multiple users with access, you can assign them a code above or share this document with them, assigning them which code to register in the system with.</p>


                <p style="margin-bottom:0px;">Once each user creates an account, they will be able to access all of the Breakout EDU Subject Pack games and create and share Breakout EDU Digital games.</p>
                <p style="margin-bottom:0px;">We have created some tutorials that can be found at link.<a href="http://www.breakoutedu.com/digital" target="_blank">BreakoutEDU.com/digital</p> <p style="margin-bottom:0px;"> Should you need any help, please feel free to contact us at <a href="mailti:info@BreakoutEDU.com" target="_blank">info@BreakoutEDU.com </p>


            </div>
    </body>
</html>