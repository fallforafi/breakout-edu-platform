<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
	
	  <div style="margin-bottom:10px;"><a href="{{ URL::to('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="logo" style="width:200px;"></a>
              
			  
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <p style="margin-bottom:0px;">
                Hi <?php echo $user->firstName; ?> <?php echo $user->lastName; ?>,
            </p>
            <p style="margin-bottom:0px;">Time sure flies! We wanted to let you know that your year of access to the Breakout EDU Platform is almost expired. Extending your membership is quick and easy to do.</p>
            <p style="margin-bottom:0px;">As a loyal Breakout EDU customer, we wanted to extend a special offer to you. Continue your Breakout EDU platform membership for just $50 a year. That is a
                savings of more than 15% of our standard annual Platform Access.</p>

            <p style="margin-bottom:0px;">Simply click the button below and confirm your payment.</p>

            <p style="margin-bottom:0px;"><a href="{{url('subscription/renew')}}/<?php echo $user->confirmationCode; ?>" >Click here to renew</a></p>


            <p style="margin-bottom:0px;"> Should you need any help, please feel free to contact us at <a href="mailti:info@BreakoutEDU.com" target="_blank">info@BreakoutEDU.com </p>
        </div>
    </body>
</html>