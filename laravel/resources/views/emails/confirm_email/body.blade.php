<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
          
                      
            <div style="margin-bottom:10px;"><a href="{{ URL::to('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="logo" style="width:200px;"></a>
              
            <h1 style="color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; text-transform:uppercase; font-weight:300;">Email Verification</h1>
            <p style="margin-bottom:0px;">Thanks for creating an account.<br>
                Simply click the big button below or follow the link below to verify your email address</p>
            <h4><a style="color:#15c;font-size:12px;"href="{{ URL::to('register/verify/' . $confirmation_code) }}">
                    {{ URL::to('register/verify/' . $confirmation_code) }}</a>
            </h4>
            <div style="margin-top:10px;">
                <a  href="{{ URL::to('register/verify/' . $confirmation_code) }}" style="text-align:center;color: #fff;padding: 15px 25px;text-decoration:none;border-radius: 5px;margin: 10px 0;display: inline-block;font-size: 18px;background-color: #006699;">Verify Your Account</a>
            </div>
            <br>
			
			<div>This email address is unmonitored, if you would like to get in touch, please contact <a href="mailto:info@breakoutedu.com">info@breakoutedu.com</a></div>
			
            </div>
          
        </div>
    </body>
</html>
