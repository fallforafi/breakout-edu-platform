@extends('signup_login')

@section('content')
<section class="inr-signup-area">
    <div class="container">

        <div class="logo text-center col-sm-12 mb10">
            <a href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a>
        </div>
        <div class="row">
            <div class="resetpassword-area col-sm-12 mt50 mb50">
                <div class="hed text-center"><h2>Reset Password</h2></div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <div class="cont">
                        <ul>
                            <li><strong>Whoops!</strong> There were some problems with your input.</li>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <div class="alert__icon"><span></span></div>
                    </div>
                </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Reset Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
