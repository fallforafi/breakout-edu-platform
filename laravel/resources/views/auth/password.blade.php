@extends('signup_login')
<?php
$title = 'Reset Password';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<section class="inr-signup-area">
    <div class="container">
        <div class="bg-white overload">


            <div class="logo text-center col-sm-12 mb10">
                <a href="{{ url('/') }}"><img src="{{asset('')}}/front/images/logo-org.png" alt="{{Config('params.site-name')}}" /></a>
            </div>
            <div class="resetpassword-area col-sm-12 mt50 mb50">


                <div class="fom-area col-sm-6 pul-cntr shad-dp20  ">

                    @if (session('status'))
                    <div class="alert alert-success alert-sticky">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="cont">
                            <ul>
                                <li>{{ session('status') }}</li>
                            </ul>
                            <div class="alert__icon"><span></span></div>
                        </div>
                    </div>
                    @endif

                    @if (count($errors) > 0)
                    <div class="alert alert-danger alert-sticky">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="cont">
                            <ul>
                                <li><strong>Whoops!</strong> There were some problems with your input.</li>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>

                            <div class="alert__icon"><span></span></div>
                        </div>
                    </div>
                    @endif

                    <div class="hed"><h2>Forgot Password</h2></div>
                    <p>Enter your email below and we will send you instructions on how to reset your password</p>

                    <form class="form" role="form" method="POST" action="{{ url('/password/email') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label>E-Mail Address</label>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Reset my password
                            </button>
                            <a href="{{url('/')}}" class='btn btn-warning'>
                                Cancel
                            </a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
