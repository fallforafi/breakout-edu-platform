<?php

return [
    'site_name' => 'Breakout EDU',
    'commission' => '15',
    'currencies' => [
        'USD' => ['symbol' => '$', 'name' => 'US Dollors'],
        'EUR' => ['symbol' => '‎€', 'name' => 'Euro'],
    ],
    'currency_default' => 'USD',
    'languages' => [
        'en_uk' => 'English (UK)',
        'en_us' => 'English (US)',
    ],
    'best_image_size' => '300 x 300 pixels',
    'language_default' => 'en_uk',
    'contentTypes' => [
        'page' => 'Page',
        'email' => 'Email',
        'block' => 'Block',
    ],
    'order_prefix' => "break",
    'titles' => [
        'Mr.' => 'Mr.',
        'Ms.' => 'Ms.',
        'Mrs.' => 'Mrs.',
        'Dr.' => 'Dr.',
    ]
];
