<?php
$url= PHP_SAPI === 'cli' ? false : url('twitter/callback');
return [

    'consumer_key' => env('TWITTER_CONSUMER_KEY'),
    'consumer_secret' => env('TWITTER_CONSUMER_SECRET'),
    'callback_url' =>$url,
];