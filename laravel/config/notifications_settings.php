<?php

return [
    'task_comment' => array('name' => "Comments received", 'email' => 1),
    'task_offer' => array('name' => "Offers received", 'email' => 1),
    'offer_reject' => array('name' => "Offers rejected", 'email' => 1),
    'task_accept' => array('name' => "Tasks accepted", 'email' => 1),
    'task_cancel' => array('name' => "Tasks cancelled", 'email' => 1),
    'task_quote' => array('name' => "Tasks Quote Invitation", 'email' => 1),
    'money_receive' => array('name' => "Money received", 'email' => 1),
    'payment_made' => array('name' => "Payments made receive", 'email' => 1),
    'message_receive' => array('name' => "Messages received", 'email' => 1),
    'reviews_awaiting' => array('name' => "Reviews awaiting", 'email' => 1),
    'tasks_posted' => array('name' => "Tasks posted", 'email' => 0),
    'task_review' => array('name' => "Reviews left", 'email' => 0),
    'offer_ammend' => array('name' => "Offer Ammended", 'email' => 0),
];
