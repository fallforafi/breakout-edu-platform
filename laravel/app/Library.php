<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Library extends Model {

    protected $table='library';
    
    
    public static function getUserGames($user_id)
    {
        $sql = "SELECT g.id,g.title,g.key FROM `mini_games` as g "
                . "join library l on l.game_id = g.id "
                . "WHERE l.user_id='$user_id'"
                . " group by g.id;";

        return DB::select($sql);
    }
}
