<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categories extends Model {

    protected $table = 'categories';

    public static function getAllCategories() {
        return Categories::leftJoin('urls as ul', 'ul.type_id', '=', 'categories.id')
                        ->select('categories.*', 'ul.key')
                        ->where('ul.type', 'category')
                        ->get();
    }

    public static function getCategoryGames($id = "",$parent_id='') {

        $subSql = "";

        if ($id != "") {
            $subSql .= " and c.id=$id ";
        }
        //echo $parent_id;
        if (is_numeric($parent_id)) {
            $subSql .= " and c.parent_id=$parent_id ";
        }
        
        /*
          if (isset(Auth::user()->id) && Auth::user()->role->role == 'normal') {
          $subSql .= " and g.forPaid=0 ";
          } elseif (isset(Auth::user()->id) && Auth::user()->role->role == 'paid') {
          $subSql .= " and g.forPaid=1 or g.forPaid=0 ";
          } else {
          $subSql .= " and g.forPaid=0 ";
          } */

        $sql = "SELECT c.id,c.parent_id,c.class,c.name,c.image,c.coverImage,count(g.id) as total,u.key FROM `categories` as c "
                . " left join game_categories gc on gc.category_id = c.id "
                . " left join games g on gc.game_id = g.id "
                . " join urls u on u.type_id = c.id "
                . " WHERE u.type='category' $subSql"
                . " group by c.id"
                . " order by c.sortOrder asc;";
        return DB::select($sql);
    }

    public function parent()
    {
        return $this->belongsTo('App\Categories', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Categories', 'id', 'parent_id');
    }

}
