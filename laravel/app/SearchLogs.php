<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchLogs extends Model {

    protected $table = 'search_logs';
    protected $fillable = ['term'];

    public static function addLog($term) {
        
        if(strlen($term)>5){
            if (SearchLogs::where('term', '=', $term)->count() == 0) {
                return SearchLogs::insertGetId(array('term'=>$term,'created_at'=>date("Y-m-d H:i:s")));
            }
        }
    }

}
