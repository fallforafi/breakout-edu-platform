<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class ProfileUpdates extends Model {

    protected $table = 'profile_updates';

//1.	First Name & Last Name = 5%
//2.	Profile picture = 10%
//3.	Email = 5%
//4.	Education = 10%
//5.	Transport = 10%
//6.	Phone Number = 10%
//7.	Location = 10%
//8.	Tag line = 10%
//9.	About me = 10%
//10.	Skills = 10%
//11.   Language = 10%

    public static function updateProfile($data) {
       // d($data,1);
        $updatedProfile = 0;
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        $profile = ProfileUpdates::where('user_id', '=', $user_id)->first();       
        if (count($profile) == 0) {
            $id = ProfileUpdates::insertGetId(['profile' => '0', 'user_id' => $user_id]);
        } else {
            $id = $profile->id;
        }
        if (isset($data['firstName']) && $data['firstName'] != '' && isset($data['lastName']) && $data['lastName'] != '') {
            $updatedProfile += 5;
        }
        if (isset($data['email']) && $data['email'] != '') {
            $updatedProfile += 5;
        }
        if (isset($user->image) && $user->image != '') {
            $updatedProfile += 10;
        }
        if (isset($data['phone']) && $data['phone'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['location']) && $data['location'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['about']) && $data['about'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['education']) && $data['education'] != '') {          
            $updatedProfile += 10;
        }
        if (isset($data['transport']) && $data['transport'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['language']) && $data['language'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['tagline']) && $data['tagline'] != '') {
            $updatedProfile += 10;
        }
        if (isset($data['skill']) && $data['skill'] != '') {
            $updatedProfile += 10;
        }
      //  d($updatedProfile,1);
        ProfileUpdates::where('user_id', $user_id)->update(['profile' => $updatedProfile]);
    }

}
