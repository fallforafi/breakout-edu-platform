<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Role;
use App\User;

class UserCodes extends Model {

    protected $table = 'user_codes';

    public static function checkUser($user_id) {

        $sql = "select id,user_id,created_at,expires from user_codes uc where user_id=$user_id and status=1 and deleted=0 order by id desc limit 1;";

        $row = DB::select($sql);
          if (count($row) == 0) {
          return 'accesscode';
          }
        return 'home';

        // return 'register/success/'.$row[0]->user_id;
    }

    public static function insertUserCode($user, $code) {



        $model = new UserCodes();
        $model->user_id = $user->id;
        $model->code_id = $code->id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->expires = date('Y-m-d H:i:s', strtotime('+1 year'));
        $model->save();
        Codes::where('id', $code->id)->update(array('isUsed' => 1));
        $role = Role::where('role', 'paid')->first();
        return User::where('id', $user->id)->update(array('role_id' => $role->id));
    }

}
