<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Functions\Functions;

class Resubscribe extends Command {

    protected $name = 'resubscribe';
    protected $description = 'Resubscribe User this will run daily';
    protected $signature = 'resubscribe:afteroneday';

    public function handle() {

        $model = DB::select("SELECT u.id,u.firstName,u.lastName,u.email as email,uc.expires,DATEDIFF(uc.expires, NOW()) as daysRemaining,u.confirmationCode as confirmationCode FROM `users` u join user_codes uc on uc.user_id=u.id WHERE DATEDIFF(uc.expires, NOW())=30 || DATEDIFF(uc.expires, NOW())=7;");

        foreach ($model as $user) {

            $subject = view('emails.crons.days' . $user->daysRemaining . '.subject');
            $body = view('emails.crons.days' . $user->daysRemaining . '.body', compact('user'));
            Functions::sendEmail($user->email, $subject, $body);
        }
    }
}
