<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Games extends Model {

    protected $table = 'games';

    public static function getGames($search = array()) {
        $subSql = "";

        if (isset($search['game_id'])) {
            $subSql .= " and g.id=" . $search['game_id'];
        }

//        if (isset(Auth::user()->id) && Auth::user()->role->role == 'normal') {
//            $subSql .= " and g.forPaid=0 ";
//        } elseif (isset(Auth::user()->id) && Auth::user()->role->role == 'paid') {
//            $subSql .= " and g.forPaid=1 or g.forPaid=0 ";
//        } else {
//            $subSql .= " and g.forPaid=0 ";
//        }


        if (isset($search['category_id'])) {
            $subSql .= " and c.id=" . $search['category_id'];
        }

        if (isset($search['isPublished'])) {
            $subSql .= " and g.isPublished=" . $search['isPublished'];
        }

        $sql = "SELECT g.id,g.title,g.image,g.forPaid,u.key FROM `games` as g "
                . "join urls u on u.type_id = g.id "
                . "left join game_categories gc on gc.game_id = g.id "
                . "left join categories c on c.id = gc.category_id "
                . "WHERE u.type='game' $subSql"
                . " group by g.id;";

        return DB::select($sql);
    }

    public static function searchGames($search) {
        $result = self::where("games.deleted", '=', 0);

        if (isset($search['title']) && $search['title'] != "") {
            $title = $search['title'];
            $result = $result->where('title', 'LIKE', "%" . $title . "%");
            $result = $result->orWhere('c.name', 'LIKE', "%" . $title . "%");
        }

        if (isset($search['gameType']) && $search['gameType'] != "") {
            $gameType = $search['gameType'];
            $result = $result->where('gameType', '=', $gameType);
        }

        $result = $result->leftjoin('game_categories as gc', 'gc.game_id', '=', 'games.id');
        $result = $result->leftjoin('categories as c', 'c.id', '=', 'gc.category_id')
                ->select('games.*', DB::raw('GROUP_CONCAT(c.name) as category_name'));

        // d($result,1);
        $result = $result->groupBy('games.id');
        $result = $result->orderBy('id', 'desc');
        $result = $result->paginate(10);
        $result->setPath('games');
        return $result;
    }

}
