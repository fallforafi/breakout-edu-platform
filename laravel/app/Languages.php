<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\UsersLanguages;

class Languages extends Model {

    protected $table = 'languages';

    public static function setLanguages($param) {
        $langs = explode(',', $param);
        $user_id = Auth::user()->id;
        UsersLanguages::where('user_id', '=', $user_id)->delete();
        foreach ($langs as $value) {
            if (Languages::where('title', '=', $value)->exists()) {
                $getRow = Languages::where('title', '=', $value)->first();
                $uLangModel = new UsersLanguages();
                $uLangModel->user_id = $user_id;
                $uLangModel->language_id = $getRow->id;
                $uLangModel->save();
            } else {
                $sLangModel = new Languages();
                $sLangModel->title = $value;
                $sLangModel->save();
                $getRow = Languages::where('title', '=', $value)->first();
                $uLangModel = new UsersLanguages();
                $uLangModel->user_id = $user_id;
                $uLangModel->language_id = $getRow->id;
                $uLangModel->save();
            }
        }
        return true;
    }  
    public static function getLanguages($param) {       
       $model =  Languages::join('users_languages','users_languages.language_id','=','languages.id')
                ->select('languages.title as name')
                ->where('user_id', '=', $param)
                ->get();
      
        return $model;
    }
   
}
