<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\MiniGames;
use App\GameLocks;
use App\GameResults;
use App\GameAnswers;
use Illuminate\Http\Request;
use Auth;
use App\Functions\Functions;

class PlayController extends Controller {

    public function __construct() {
        // $this->middleware('auth');
        session_start();
    }

    public function iframe($slug) {

        $data['slug'] = $slug;

        return view('front.mini_games.iframe', $data);
    }

    public function embed($slug) {

        $model = MiniGames::where('key', $slug)->first();
        $gameLocks = GameLocks::where('game_id', $model->id)->orderBy('id', 'asc')->get();
        $data['model'] = $model;
        $data['gameLocks'] = $gameLocks;
        $data['game_id'] = $model->id;
        $data['embed'] = 1;
        $data['slug'] = $slug;

        if (count($gameLocks) > 0) {

            $data['layout'] = $gameLocks[0]->lockType;
            $data['lock_id'] = $gameLocks[0]->id;
            $data['title'] = $gameLocks[0]->lockType;
        } else {
            return view('front.mini_games.embed.no_locks', $data);
            die;
        }

        if ($model->hasPlayCode == 0) {
            MiniGames::where('id', $model->id)->increment('playCount');
            return view('front.mini_games.embed', $data);
            die;
        }
        // unset($_SESSION['game_id']);
        if (isset($_SESSION['game_id']) && $_SESSION['game_id'] === $model->id) {
            MiniGames::where('id', $model->id)->increment('playCount');
            return view('front.mini_games.embed', $data);
        } else {
            return view('front.mini_games.embed_code', $data);
        }
    }

    public function play($slug) {

        $data = array();
        $model = MiniGames::where('key', $slug)->first();

        if (!isset($model->id)) {
            return redirect('404');
        }

        $gameLocks = GameLocks::where('game_id', $model->id)->orderBy('id', 'asc')->get();
        $data['slug'] = $slug;
        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";
        $breadcrumbs[1]['url'] = url('my-digital-games');
        $breadcrumbs[1]['name'] = 'Digital Game';

        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = $model->title;

        $data['model'] = $model;
        $data['breadcrumbs'] = $breadcrumbs;
        $data['gameLocks'] = $gameLocks;
        $data['game_id'] = $model->id;
        $data['embed'] = 0;

        if (count($gameLocks) > 0) {

            $data['layout'] = $gameLocks[0]->lockType;
            $data['lock_id'] = $gameLocks[0]->id;
            $data['title'] = $gameLocks[0]->lockType;
        } else {
            return view('front.mini_games.no_locks', $data);
            die;
        }

        if ($model->hasPlayCode == 0) {
            MiniGames::where('id', $model->id)->increment('playCount');
            return view('front.mini_games.play', $data);
            die;
        }
        if (isset($_SESSION['game_id']) && $_SESSION['game_id'] === $model->id) {
            MiniGames::where('id', $model->id)->increment('playCount');
            return view('front.mini_games.play', $data);
        } else {
            return view('front.mini_games.play_code', $data);
        }
    }

    public static function getCompleteTime($target, $completedTime) {

        list($min, $sec) = explode(':', $target);
        list($min2, $sec2) = explode(':', $completedTime);
        $t1 = ($min * 60) + $sec;
        $t2 = ($min2 * 60) + $sec2;
        $d = $t1 - $t2;
        return $time = gmdate("i:s", $d);
    }

    public function checkcode(Request $request) {

        $rules = array(
            'name' => 'required',
            'code' => 'required',
            'game_id' => 'required',
        );

        $messages = array(
            'name.required' => 'You need to enter a your name.',
            'code.required' => 'You need to enter a valid play code to start the game.',
        );
        $input = $request->all();
        $validation = 'success';
        $response['error'] = 0;
        $response['login'] = 0;
        $response['embed'] = $input['embed'];

        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            $validation = 'fail';
            //return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if ($validation == 'success') {
            $model = MiniGames::find($input['game_id']);
            
            if ($model->playCode != strtoupper($input['code'])) {
                $validator->errors()->add('error_db', 'You need to enter a valid play code to start the game.');
                $validation = 'fail';
                // return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
            } else {
                $response['name'] = $_SESSION['name'] = $input['name'];
                $response['game_id'] = $_SESSION['game_id'] = $model->id;
                //return redirect()->back();
            }
        }

        if ($validation == 'fail') {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        }

        echo json_encode($response);
    }

    public function view($game_id) {
        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = url('my-digital-games');
        $breadcrumbs[1]['name'] = "Digital Game";

        $model = MiniGames::find($game_id);
        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = $model->title;

        $data['breadcrumbs'] = $breadcrumbs;
        $data['model'] = $model;
        $data['game_id'] = $game_id;

        return view('front.mini_games.finish', $data);
    }

    public function checkanswer(Request $request) {

        $rules = array(
            'game_id' => 'required',
            'lock_id' => 'required',
        );

        $input = $request->all();

        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            parse_str($request->answers, $answers);
            if (empty($answers[$input['lock_id']])) {
                $validator->errors()->add('error_db', 'Nope, that’s not it. Try Again!');
                $errors = $validator->errors();
                $response['error'] = 1;
                $response['errors'] = $errors;
            } else {
                $model = GameLocks::find($input['lock_id']);
                $inputAnswers = $answers[$input['lock_id']];
                $values = [NULL, FALSE, '', 0, 1];
                $inputAnswers = array_filter($inputAnswers, function($value) {
                    return ($value !== null && $value !== false && $value !== '');
                });
                $inputAnswers = implode(',', $inputAnswers);

                $correct = 0;
                $wrong = 0;
                // d($answers);
//                d('gap');
                //  d($model->answers);

                if ($inputAnswers == $model->answers) {
                    $correct = 1;
                } else {
                    $validator->errors()->add('error_db', 'Nope, that’s not it. Try Again!');
                    $errors = $validator->errors();
                    $response['error'] = 1;
                    $response['errors'] = $errors;
                }
            }
        }
        echo json_encode($response);
    }

    public function locklayout(Request $request) {

        $data = array();
        $data['title'] = $request->title;
        $data['layout'] = $request->layout;
        $data['game_id'] = $request->game_id;
        $data['page'] = $request->page;
        $data['answers'] = array();

        $model = GameLocks::where('game_id', $data['game_id'])->orderBy('id', 'asc')->paginate(1);

        parse_str($request->answers, $answers);
        if (!empty($answers[$model[0]->id])) {
            $values = [NULL, FALSE, '', 0, 1];
            $data['answers'] = array_filter($answers[$model[0]->id], function($value) {
                return ($value !== null && $value !== false && $value !== '');
            });
        }
        //d($data['answers'], 1);

        $data['model'] = $model[0];
        $data['paginate'] = $model;
        $data['game'] = MiniGames::find($data['game_id']);
        return view('front.mini_games.includes.paly_display', $data);
    }

    public function thanks(Request $request) {

        $data['game_id'] = $request->game_id;
        $data['answers'] = $request->answers;
        $data['model'] = MiniGames::find($data['game_id']);
        parse_str($request->answers, $answers);
        $data['answers'] = $answers;

        if ($data['model']->hasTimer == 1) {
            $data['timeTaken'] = self::getCompleteTime($data['model']->time, $request->time);
            $data['time'] = self::getCompleteTime($data['model']->time, $request->time);
        }

        self::saveStats($data);

        return view('front.mini_games.thanks', $data);
    }

    public static function saveStats($data) {

        $grModel = new GameResults();
        $grModel->game_id = $data['game_id'];

        if (isset(Auth::user()->id)) {
            $grModel->user_id = Auth::user()->id;
        }

        if (isset($_SESSION['name'])) {
            $grModel->name = $_SESSION['name'];
        }

        if ($data['model']->hasTimer == 1) {
            $grModel->time = $data['time'];
        }

        if (key_exists('undefined', $data['answers'])) {
            unset($answers['undefined']);
        }

        $grModel->created_at = date('Y-m-d H:i:s');
        $grModel->save();

        $locks = \App\GameLocks::where('game_id', $data['game_id'])->get();

        if ($grModel->id) {
            $count = 0;
            foreach ($data['answers'] as $lock_id => $answer) {

                $values = [NULL, FALSE, '', 0, 1];
                $answer = array_filter($answer, function($value) {
                    return ($value !== null && $value !== false && $value !== '');
                });


                $inputAnswers = implode(',', $answer);
                $lockModel = \App\GameLocks::find($lock_id);

                $gaModel = new GameAnswers();
                $gaModel->result_id = $grModel->id;
                $gaModel->lock_id = $lock_id;
                $gaModel->answer = $inputAnswers;
                $gaModel->correctAnswer = $lockModel->answers;
                $grModel->created_at = date('Y-m-d H:i:s');
                $gaModel->save();
                $count++;
            }
        }

        $totalLocks = count($locks);
        $completed = ($count / $totalLocks) * 100;
        $isWon = 0;

        if ($completed == 100) {
            $isWon = 1;
        }

        GameResults::where('id', $grModel->id)->update(array('isWon' => $isWon, 'completed' => $completed));
    }

    public function end($game_id, Request $request) {

        $rules = array(
            'game_id' => 'required',
            'answers' => 'required',
        );

        $messages = array(
            'answers.required' => 'Sorry, but you need to remove all the locks in order to complete the game.',
        );

        $input = $request->all();
        $response['error'] = 0;
        $response['login'] = 0;

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
            echo json_encode($response);
            die;
        }
        parse_str($request->answers, $input['answers']);
        if (key_exists('undefined', $input['answers'])) {
            unset($input['answers']['undefined']);
        }

        $model = GameLocks::where('game_id', $input['game_id'])->orderBy('id', 'desc')->get();

        if (count($input['answers']) != count($model)) {
            $validator->errors()->add('error_db', 'Sorry, but you need to remove all the locks in order to complete the game.');
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {

            $correct = 0;
            $wrong = 0;
            $answers_wrong = array();
            $answers_correct = array();

            foreach ($model as $lock) {
                $inputAnswers = $input['answers'][$lock->id];

                //$inputAnswers = array_filter($inputAnswers);
                $values = [NULL, FALSE, '', 0, 1];
                $inputAnswers = array_filter($inputAnswers, function($value) {
                    return ($value !== null && $value !== false && $value !== '');
                });


                $inputAnswers = implode(',', $inputAnswers);
                if ($inputAnswers == $lock->answers) {
                    $correct++;
                    $answers_correct[] = $lock->id;
                } else {
                    $wrong++;
                    $answers_wrong[] = $lock->id;
                }
            }

            $answers['answers_correct'] = $answers_correct;
            $answers['answers_wrong'] = $answers_wrong;
            $answers['correct'] = $correct;
            $answers['wrong'] = $wrong;
            $response['answers'] = $answers;
        }

        echo json_encode($response);
    }

}
