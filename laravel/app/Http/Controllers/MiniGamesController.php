<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\MiniGames;
use App\GameLocks;
use App\Library;
use App\Role;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Functions\Functions;
use App\GameResults;
use Excel;
use App\Aws\S3;

class MiniGamesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        session_start();
        if (isset(Auth::user()->id)) {
            $user_id = Auth::user()->id;
            $role = Role::find(Auth::user()->role_id);
            if ($role->role == 'user') {
                header("Location:" . url('accesscode'));
                die;
            }
        }
    }

    public static function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public function create() {

        $data = array();
        $model = new MiniGames();

        $data['model'] = $model;

        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = "#_";
        $breadcrumbs[1]['name'] = "Create Digital Game";

        $data['breadcrumbs'] = $breadcrumbs;
        $data['type'] = 'create';
        $data['action'] = 'save';

        return view('front.mini_games.create', $data);
    }

    public function edit($id) {

        $data = array();

        if (!is_numeric($id)) {
            return redirect('404');
            return false;
        }

        $model = MiniGames::find($id);
        $data['model'] = $model;

        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = "#_";
        $breadcrumbs[1]['name'] = "Edit Digital Game";

        $data['breadcrumbs'] = $breadcrumbs;
        $data['type'] = 'edit';
        $data['action'] = 'update/' . $id;
        return view('front.mini_games.edit', $data);
    }

    public function copy($game_id) {

        $user_id = Auth::user()->id;

        $model = MiniGames::find($game_id);
        $response['error'] = 0;
        $response['login'] = 0;
        ///$key = self::slugify($model->title) . '-' . $user_id;

        // $count = MiniGames::where('user_id', $user_id)->where('key', $key)->count();
        $input = array();

        $input['title'] = $model->title;
        $input['description'] = $model->description;
        $input['hasTimer'] = $model->hasTimer;
        $input['time'] = $model->time;
        $input['user_id'] = $user_id;
        $input['copied_from'] = $game_id;
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['key'] = self::slugify($model->title) . '-' . $user_id . '-' . Functions::generateRandomString(10);
        $input['playCode'] = Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3);
        $input['playCode'] = strtoupper($input['playCode']);
        $input['gameType'] = $model->gameType;
        $input['endScreenType'] = $model->endScreenType;
        
        if ($model->endScreenType == 'image') {
            $source = $model->endScreen;
            $arr = explode('.', $source);
            $extension = end($arr);
            $destinationPath = 'uploads/mini_games/endscreens/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $dest = rand(111, 999) . time() . '.' . $extension;
            $imageThumb = $destinationPathThumb .  $dest;
            $image = $destinationPath . $dest;
            S3::copy($destinationPath . $source, $image);
            S3::copy($destinationPathThumb . $source, $imageThumb);
            $input['endScreen'] = $dest;
        } elseif ($model->endScreenType == 'breakout') {
            $input['endScreen'] = '';
        } elseif ($model->endScreenType == 'text') {
            $input['endScreen'] = $model->endScreen;
        } elseif ($model->endScreenType == 'video') {
            $input['endScreen'] = $model->endScreen;
        }
        $id = MiniGames::insertGetId($input);

        $gameLocks = GameLocks::where('game_id', $game_id)->get();

        foreach ($gameLocks as $lock) {
            $input = array();


            if ($lock->lockSetup == 'image') {

                $source = $lock->image;
                $destinationPath = '/uploads/gameslocks/';
                $destinationPathThumb = $destinationPath . 'thumbnail/';
                $arr = explode('.', $source);
                $extension = end($arr);
                $dest = rand(111, 999) . '.' . $extension;
                $image = $destinationPath . $dest;
                $from = $destinationPath . $source;
                S3::copy($from, $image);
                $imageThumb = $destinationPathThumb . $dest;
                S3::copy($destinationPathThumb.$source,$imageThumb);
                $input['image'] = $dest;
                
            } elseif ($lock->lockSetup == 'text') {
                $input['text'] = $lock->text;
            } elseif ($lock->lockSetup == 'video') {
                $input['videoUrl'] = $lock->videoUrl;
            }

            $input['created_at'] = date('Y-m-d H:i:s');
            $input['lockSetup'] = $lock->lockSetup;
            $input['answers'] = $lock->answers;
            $input['lockType'] = $lock->lockType;
            $input['game_id'] = $id;
            $response['new_game_id'] = $id;
            GameLocks::insertGetId($input);
        }

        $response['game_id'] = $game_id;
        $response['message'] = 'This game has been copied.';
        $response['success'] = true;
        return json_encode($response);
        die;
    }

    public function save(Request $request) {

        $rules = array(
            'title' => 'required|max:100',
            'description' => 'required|max:1000',
            'gameType' => 'required',
            'endScreenType' => 'required',
        );

        $messages = array();
        $input = $request->all();
        $user_id = Auth::user()->id;

        if (isset($input['hasTimer'])) {
            $input['hasTimer'] = 1;
            $input['time'] = trim($request->time);
            $rules['time'] = 'required|date_format:m:i';
            $messages = array(
                'time.required' => 'You have to enter a valid time.',
                'time.date_format' => 'The time does not match the format minutes:seconds.',
            );
        } else {
            $input['hasTimer'] = 0;
        }

        if ($input['endScreenType'] == 'image') {
            if (empty(Input::file('image'))) {
                $rules['endScreen'] = 'required|mimes:jpeg,bmp,png,gif';
                $messages['endScreen.required'] = 'Please upload an image';
            }
        }

        $input['user_id'] = $user_id;
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $count = MiniGames::where('user_id', $user_id)->where('title', trim($input['title']))->count();

        if ($count > 0) {
            $validator->errors()->add('error_db', 'You have already created this game.');
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if ($input['endScreenType'] == 'image') {

            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['endScreen'] = S3::storeImage($fileName, $file, 'uploads/mini_games/endscreens/', 'uploads/mini_games/thumbnail/endscreens/', 400, 400);
            
        } elseif ($input['endScreenType'] == 'breakout') {
            $input['endScreen'] = '';
        } elseif ($input['endScreenType'] == 'text') {
            $input['endScreen'] = $input['text'];
        } elseif ($input['endScreenType'] == 'video') {
            $input['endScreen'] = $input['video'];
        }
        unset($input['_token']);
        unset($input['type']);
        unset($input['text']);
        unset($input['video']);
        unset($input['image']);
        unset($input['save']);
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['key'] = self::slugify($input['title']) . '-' . $user_id;

        $input['playCode'] = Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3);
        $input['playCode'] = strtoupper($input['playCode']);
        $id = MiniGames::insertGetId($input);
        return redirect("choose-lock-types/" . $id);
    }

    public function update($id, Request $request) {

        $rules = array(
            'title' => 'required|max:100',
            'description' => 'required|max:1000',
            'gameType' => 'required',
            'endScreenType' => 'required',
        );

        $input = $request->all();

        $model = MiniGames::find($id);
        if ($input['endScreenType'] == 'text') {
            $rules['text'] = 'required';
        } elseif ($input['endScreenType'] == 'video') {
            $rules['video'] = 'required';
        } elseif ($input['endScreenType'] == 'image') {

            if ($model->endScreenType == "") {
                $rules['endScreenType'] = 'required|mimes:jpeg,bmp,png,gif';
            }
        }

        $messages = array();
        $user_id = Auth::user()->id;

        if (isset($input['hasTimer'])) {
            $input['hasTimer'] = 1;
            $input['time'] = trim($request->time);
            $rules['time'] = 'required|date_format:m:i';
            $messages = array(
                'time.required' => 'You have to enter a valid time.',
                'time.date_format' => 'The time does not match the format minutes:seconds.',
            );
        } else {
            $input['hasTimer'] = 0;
        }
        $input['user_id'] = $user_id;

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        /*
          $count = MiniGames::where('id', '!=', $id)->where('user_id', $user_id)->where('title', trim($input['title']))->count();

          if ($count > 0) {
          $validator->errors()->add('error_db', 'You have already created this game.');
          return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
          }
         */
        if ($input['endScreenType'] == 'image' && Input::file('image')) {

            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['endScreen'] = S3::storeImage($fileName, $file, 'uploads/mini_games/endscreens/', 'uploads/mini_games/endscreens/thumbnail/', 400, 400);
            S3::deleteImage($model->endScreen, 'uploads/mini_games/endscreens/', 'uploads/mini_games/endscreens/thumbnail/');
        } elseif ($input['endScreenType'] == 'breakout') {
            $input['endScreen'] = '';
        } elseif ($input['endScreenType'] == 'text') {
            $input['endScreen'] = htmlentities($input['text']);
        } elseif ($input['endScreenType'] == 'video') {
            $input['endScreen'] = htmlentities($input['video']);
        }

        if (!isset($input['save'])) {
            $input['save'] = 'next';
        }

        $saveButton = $input['save'];
        unset($input['_token']);
        unset($input['type']);
        unset($input['text']);
        unset($input['video']);
        unset($input['image']);
        unset($input['save']);

        $input['updated_at'] = date('Y-m-d H:i:s');
        $input['key'] = self::slugify($input['title']) . '-' . $user_id;
        MiniGames::where('id', $id)->update($input);

        if ($saveButton == 'exit') {
            return redirect("digital-game-finish/" . $id);
            die;
        }

        return redirect("choose-lock-types/$id");
    }

    public function chooselocktypes($game_id) {

        $data = array();
        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = url('create-digital-game');
        $breadcrumbs[1]['name'] = "Create Digital Game";

        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = "Choose Lock Types";

        $model = MiniGames::find($game_id);
        $gameLocks = GameLocks::where('game_id', $game_id)->get();

        $data['breadcrumbs'] = $breadcrumbs;
        $data['model'] = $model;
        $data['gameLocks'] = $gameLocks;
        $data['type'] = 'create';
        $data['game_id'] = $game_id;

        return view('front.mini_games.choose_lock_type', $data);
    }

    public function deletelock($id) {
        $user_id = Auth::user()->id;
        $model = GameLocks::find($id);
        $gameModel = MiniGames::where('id', $model->game_id)->where('user_id', $user_id)->count();
        //d($gameModel,1);
        if ($gameModel == 1) {
            $gameModel = GameLocks::where('id', $id)->delete();
        }
        return redirect()->back();
        //d($model, 1);
    }

    public function getlocktype(Request $request) {
        $data = array();
        $data['title'] = $request->title;
        $data['layout'] = $request->layout;
        $model = new GameLocks;
        $data['model'] = $model;
        $data['answers'] = array();
        $data['action'] = 'savelocktype';
        $data['game_id'] = $request->game_id;

        return view('front.mini_games.lock_layout', $data);
    }

    public function editlocktype(Request $request) {

        $data = array();
        $data['model'] = GameLocks::find($request->id);
        $data['layout'] = $request->layout;
        $data['title'] = $request->title;
        $data['answers'] = explode(',', $data['model']->answers);
        $data['action'] = 'updatelocktype/' . $data['model']->id;
        $data['game_id'] = $data['model']->game_id;

        array_unshift($data['answers'], $data['answers'][0]);
        unset($data['answers'][0]);

        return view('front.mini_games.lock_layout', $data);
    }

    public function updatelocktype($id, Request $request) {

        $rules = array(
            'lockType' => 'required',
            'lockSetup' => 'required',
        );

        $input = $request->all();

        $values = [NULL, FALSE, '', 0, 1];
        $input['answers'] = array_filter($input['answers'], function($value) {
            return ($value !== null && $value !== false && $value !== '');
        });

//        $rules['answers'] = 'requiredarray';
//        $message['answers.requiredarray'] = 'Please enter the answer combination.';

        $model = GameLocks::find($id);


        if ($input['lockSetup'] == 'image') {
            // d($model->lockSetup,1);
            if ($model->lockSetup == 'image' && Input::file('image')) {
                $rules['image'] = 'required|mimes:jpeg,bmp,png,gif';
            }
        } elseif ($input['lockSetup'] == 'text') {
            $rules['text'] = 'required';
        } elseif ($input['text'] == 'video') {
            $rules['videoUrl'] = 'required';
        }

        $validator = Validator::make($input, $rules);
        $response['error'] = 0;
        $response['login'] = 0;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } elseif (empty($input['answers'])) {
            $validator->errors()->add('error_answers', 'Please enter the answer combination.');
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            unset($input['_token']);
            unset($input['save_lock_add']);
            unset($input['save_lock_exit']);


            if ($input['lockSetup'] == 'image' && Input::file('image')) {
                $file = Input::file('image');
                $fileName = rand(111, 999) . time();
                $input['image'] = S3::storeImage($fileName, $file, '/uploads/gameslocks/', '/uploads/gameslocks/thumbnail/', 400, 400);
                S3::deleteImage($model->image, 'uploads/gameslocks/', 'uploads/gameslocks/thumbnail/');
            
            } elseif ($input['lockSetup'] == 'text') {
                $input['text'] = htmlentities($input['text']);
            } elseif ($input['lockSetup'] == 'video') {
                $input['videoUrl'] = htmlentities($input['videoUrl']);
            }

            $input['updated_at'] = date('Y-m-d H:i:s');
            $input['answers'] = implode(',', $input['answers']);
            $id = GameLocks::where('id', $input['id'])->update($input);
            $response['id'] = $id;
            $response['message'] = 'Lock setup updated.';
        }

        return json_encode($response);
    }

    public function finish($game_id) {

        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = url('create-digital-game');
        $breadcrumbs[1]['name'] = "Create Digital Game";

        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = "Finish";

        $model = MiniGames::find($game_id);
        $data['breadcrumbs'] = $breadcrumbs;
        $data['model'] = $model;
        $data['game_id'] = $game_id;


        return view('front.mini_games.finish', $data);
    }

    public function savelocktype(Request $request) {

        $rules = array(
            'lockType' => 'required',
            'lockSetup' => 'required',
        );

        $input = $request->all();

        $values = [NULL, FALSE, '', 0, 1];
        $input['answers'] = array_filter($input['answers'], function($value) {
            return ($value !== null && $value !== false && $value !== '');
        });

//        $rules['answers'] = 'requiredarray';
//        $message['answers.requiredarray'] = 'Please enter the answer combination.';

        if ($input['lockSetup'] == 'image') {
            $rules['image'] = 'required|mimes:jpeg,bmp,png,gif';
        } elseif ($input['lockSetup'] == 'text') {
            $rules['text'] = 'required';
        } elseif ($input['text'] == 'video') {
            $rules['videoUrl'] = 'required';
        }
        $response['error'] = 0;
        $response['login'] = 0;
        //$validator = Validator::make($request->all(), $rules,$message);
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            unset($input['_token']);

            if ($input['lockSetup'] == 'image') {
                $file = Input::file('image');
                $fileName = rand(111, 999) . time();
                $input['image'] = S3::storeImage($fileName, $file, '/uploads/gameslocks/', '/uploads/gameslocks/thumbnail/', 400, 400);
                
            
            } elseif ($input['lockSetup'] == 'text') {
                $input['text'] = htmlentities($input['text']);
            } elseif ($input['lockSetup'] == 'video') {
                $input['videoUrl'] = htmlentities($input['videoUrl']);
            }

            $input['created_at'] = date('Y-m-d H:i:s');
            $input['answers'] = implode(',', $input['answers']);

            $id = GameLocks::insertGetId($input);
            $response['id'] = $id;
            $response['message'] = 'Your lock has been added to the game.';
            // return redirect()->back();
        }
        return json_encode($response);
    }

    public function delete($game_id) {

        $user_id = Auth::user()->id;
        $affect = MiniGames::where('user_id', $user_id)->where('id', $game_id)->delete();
        if ($affect > 0) {
            GameLocks::where('game_id', $game_id)->delete();
            Library::where('game_id', $game_id)->delete();
        }
        Session::flash('success', 'Digital Game has been deleted.');
        return redirect()->back();
    }

    public function mygames() {
        $user_id = Auth::user()->id;
        $model = MiniGames::where('user_id', $user_id)->orderBy('id', 'desc')->paginate(10);

        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = "MY DIGITAL GAMES";

        $data['breadcrumbs'] = $breadcrumbs;
        $data['model'] = $model;

        return view('front.mini_games.my_games', $data);
    }

    public function gameresults($game_id) {
        $user_id = Auth::user()->id;
        $model = MiniGames::where('id', $game_id)->where('user_id', $user_id)->first();

        $breadcrumbs[0]['url'] = url('/');
        $breadcrumbs[0]['name'] = "Home";

        $breadcrumbs[1]['url'] = url('my-digital-games');
        $breadcrumbs[1]['name'] = "MY DIGITAL GAMES";

        $breadcrumbs[2]['url'] = "#_";
        $breadcrumbs[2]['name'] = $model->title;

        $data['breadcrumbs'] = $breadcrumbs;
        $data['model'] = $model;
        $data['results'] = GameResults::where('game_id', $game_id)->orderBy('id', 'desc')->paginate(15);

        return view('front.mini_games.results', $data);
    }

    public function resultsdownload($game_id) {

        $model = MiniGames::find($game_id);
        $result = GameResults::where('game_id', $game_id)->orderBy('id', 'desc')
                ->select(array(DB::raw('name as "Player Name"'), DB::raw('IF(isWon = 1, "Yes", "No") as Completed'), DB::raw('time as "Time to Complete"'), DB::raw('created_at as "Date"')))
                ->get();

        return Excel::create($model->title . " Results", function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download('csv');
    }

}
