<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\Library;
use App\MiniGames;
use App\Functions\Functions;
use Auth;
use Illuminate\Http\Request;

class LibraryController extends Controller {

    public function __construct() {
        
    }

    public function index() {

        if (Auth::user()->id) {
            $model = Library::getUserGames(Auth::user()->id);
            $breadcrumbs[0]['url'] = url('/');
            $breadcrumbs[0]['name'] = "Home";

            $breadcrumbs[2]['url'] = "#_";
            $breadcrumbs[2]['name'] = "My Library";

            $data['breadcrumbs'] = $breadcrumbs;
            $data['model'] = $model;

            return view('front.library.index', $data);
        } else {
            
        }
    }

    public function delete($game_id) {

        if (Auth::user()->id) {
            $model = Library::where('game_id', $game_id)->where('user_id', Auth::user()->id)->delete();
        }
        return redirect()->back();
    }

    public function addtolibrary(Request $request) {

        $input = $request->all();

        $rules = array(
            'id' => 'required',
        );

        $validator = Validator::make($input, $rules);




        $validator->after(function ($validator) use($input) {
            $gameModel = MiniGames::find($input['id']);
            if (!isset(Auth::user()->id)) {
                $validator->errors()->add('login_error', 'Please login first.');
            } elseif (Auth::user()->id == $gameModel->user_id) {
                $validator->errors()->add('login_error', 'You can not add you own game.');
            } else {
                $count = Library::where("user_id", Auth::user()->id)->where('game_id', $input['id'])->count();
                if ($count > 0) {
                    $validator->errors()->add('exist_error', 'You have already added this game.');
                }
            }
        });

        $response['error'] = 0;
        $response['login'] = 0;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input['user_id'] = Auth::user()->id;
            $model = new Library();
            $model->user_id = $input['user_id'];
            $model->game_id = $input['id'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();

            $response['message'] = 'This game has been added to your library';
            $response['success'] = true;
        }
        return json_encode($response);
    }

}
