<?php

namespace App\Http\Controllers;

use Auth;
use App\Tasks;
use App\Offers;
use App\Activities;
use App\Notifications;


class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data = array();
        return view('front.index', $data);
    }

    public function getTasksSummary() {
        $data = array();
        $user_id = Auth::user()->id;
        $data['bid_on'] = Offers::where('user_id', $user_id)->count();
        $data['assignee'] = Offers::where('user_id', $user_id)->where('offerStatus', 'assigned')->count();
        $data['completed'] = Offers::where('user_id', $user_id)->where('offerStatus', 'completed')->count();
        $data['open_tasks'] = Tasks::where('user_id', $user_id)->where('taskStatus', 'open')->count();
        $data['completed_tasks'] = Tasks::where('user_id', $user_id)->where('taskStatus', 'completed')->count();
        $data['assigned_tasks'] = Tasks::where('user_id', $user_id)->where('taskStatus', 'assigned')->count();
        return view('front.dashboard.ajax.tasks_summary', $data);
    }

    public function getActivities() {

        $user_id = Auth::user()->id;
        $data['model'] = Activities::where('status', 1)->where('deleted', 0)->orderBy('id','desc')->get();
        return view('front.dashboard.ajax.activities', $data);
    }

    public function getNotifications() {
        $user_id = Auth::user()->id;
        $data['model'] = Notifications::where('notify_to', $user_id)->where('status', 1)->where('deleted', 0)->orderBy('id', 'desc')->limit(50)->get();
        return view('front.dashboard.ajax.notifications', $data);
    }

}
