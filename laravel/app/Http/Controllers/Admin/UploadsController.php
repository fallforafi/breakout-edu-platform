<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use Validator,
    Input;
use Auth;
use App\Invitees;
use App\Uploads;
use App\Codes;
use App\InviteeCodes;

use App\Functions\Functions;
use Illuminate\Http\Request;

class UploadsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $model = Uploads::orderBy('id', 'desc')->get();
        return view('admin.uploads.index', compact('model'));
    }

    public function create() {
        $data = array();
        return view('admin.uploads.create', $data);
    }

    public function insert(Request $request) {

        $validator = Validator::make($request->all(), ['file' => 'required']);
//|mimes:csv
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $model = new Uploads;

        if (Input::hasFile('file')) {
            $file = Input::file('file');
            $name = Input::file('file')->getClientOriginalName();
            $destinationPath = public_path() . '/uploads/invitees/';
            $extension = $file->getClientOriginalExtension();
            $fileName = rand(111, 999) . time() . '.' . $extension;
            $image = $destinationPath . '/' . $fileName;
            $upload_success = $file->move($destinationPath, $fileName);
            $model->name = $name;
            $model->file = $fileName;
        }
        $model->created_at = date("Y-m-d H:i:s");
        $model->save();

        $path = $destinationPath . $fileName;
        $row = 1;
        $i=1;
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row == 1) {
                    $row++;
                    continue;
                }
                $name = $data[0];
                $inviteeModel = new Invitees;
                $inviteeModel->name = $name;
                $inviteeModel->upload_id = $model->id;
                $inviteeModel->email = $data[1];
                $inviteeModel->quantity = $data[2];
                $inviteeModel->created_at = date("Y-m-d H:i:s");
                $inviteeModel->save();
                $codes = array();
                
                for($j=1;$j<=$data[2];$j++){
                    $code = Functions::generateRandomString(9);
                    $modelCode = new Codes;
                    $modelCode->code = $code;
                    $modelCode->created_at = date("Y-m-d H:i:s");
                    $modelCode->save();
                    
                    $modelInviteeCodes = new InviteeCodes;
                    $modelInviteeCodes->code = $code;
                    $modelInviteeCodes->code_id = $modelCode->id;
                    $modelInviteeCodes->invitee_id = $inviteeModel->id;
                    $modelInviteeCodes->created_at = date("Y-m-d H:i:s");
                    $modelInviteeCodes->save();
                    $codes[$j] = $code;
                }
                
                $row++;
                $i++;
                $subject = view('emails.invitee.subject', compact('name', 'codes'));
                $body = view('emails.invitee.body', compact('name', 'codes'));
                //$body;
                $mail = Functions::sendEmail($inviteeModel->email, $subject, $body);
            }
            Uploads::where('id',$model->id)->update(array('total'=>$i-1));
            fclose($handle);
        }

        \Session::flash('success', 'File uploaded successfully added!');
        return redirect('admin/uploads');
    }

    public function delete($id) {
        $row = Uploads::where('id', '=', $id)->delete();
        return redirect('admin/uploads');
    }

}
