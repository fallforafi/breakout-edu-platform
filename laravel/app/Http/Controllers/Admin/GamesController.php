<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input;
use App\Games;
use App\Categories;
use App\Tools;
use App\Urls;
use App\GameCategories;
use Illuminate\Http\Request;
use Session;
use App\Aws\S3;

class GamesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        if (isset($_GET['title'])) {
            $data['title'] = $_GET['title'];
        }
        $data['page'] = $page;
        //$data['categories'] = $categories;
        return view('admin.games.index', $data);
    }

    public function listing(Request $request) {
        $data['search'] = $request->all();
        $data['model'] = Games::searchGames($data['search']);
        return view('admin.games.ajax.list', $data);
    }

    public function show() {

        $id = $_GET['node'];
        $model = Categories::where('parent_id', '=', $id)->get();
        return view('admin.categories.children', compact('model', 'id'));
    }

    public function create() {
        $data = array();
        $categories = Categories::lists('name', 'id');
        $gameCategories = array();
        $tools = Tools::where('status', 1)->get();
        $data['categories'] = $categories;
        $data['gameCategories'] = $gameCategories;
        $data['tools'] = $tools;
        $data['gameTools'] = array();
        $data['key'] = null;
        return view('admin.games.create', $data);
    }

    public function insert(Request $request) {

        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:5|max:50|unique:games',
                    'categories' => 'required',
                    'key' => 'required|unique:urls'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }


        $model = new Games;

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $model->image = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
        }

        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->infoLine1 = $request->infoLine1;
        $model->infoLine2 = $request->infoLine2;
        $model->infoLine3 = $request->infoLine3;
        $model->infoLine4 = $request->infoLine4;
        $model->linkToFiles = $request->linkToFiles;
        $model->launchTool = $request->launchTool;
        $model->questions = $request->questions;
        $model->story = $request->story;
        $model->lockCombinations = $request->lockCombinations;
        $model->instructions = $request->instructions;
        $model->videoEmbed = $request->videoEmbed;
        $model->note = $request->note;
        $model->modifications = $request->modifications;
        $model->requirements = $request->requirements;
        $model->tags = $request->tags;
        $model->created_at = date("Y-m-d H:i:s");
        $model->gameType = 'games';
        $model->forPaid = $request->forPaid;
        $model->isPublished = $request->isPublished;
        $model->save();


        $categories = $request->categories;

        foreach ($categories as $category_id) {
            $categoryModel = new GameCategories();
            $categoryModel->category_id = $category_id;
            $categoryModel->game_id = $model->id;
            $categoryModel->created_at = date("Y-m-d H:i:s");
            $categoryModel->save();
        }

        $input = array();
        $input['type_id'] = $model->id;
        $input['key'] = $request->key;
        $input['type'] = 'game';
        $url = Urls::saveUrl($input);

        \Session::flash('success', 'New access code successfully added!');
        return redirect('admin/games');
    }

    public function edit($id) {
        $model = Games::findOrFail($id);
        $categories = Categories::lists('name', 'id');
        $gameCategoriesModel = GameCategories::where('game_id', $id)->get();
        $gameCategories = array();

        foreach ($gameCategoriesModel as $gc) {
            $gameCategories[] = $gc->category_id;
        }

        $url = Urls::where('type', 'game')->where('type_id', $id)->first();
        if (!empty($url)) {
            $key = $url->key;
        } else {
            $key = null;
        }

        return view('admin.games.edit', compact('model', 'categories', 'tools', 'gameCategories', 'key', 'url'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:5|max:80',
                    'categories' => 'required',
                    'image' => 'mimes:jpeg,bmp,png,gif',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $urlCheck = Urls::where('type_id', '!=', $id)->where('type', '=', 'game')->where('key', $request->key)->get();

        if (count($urlCheck) > 0) {
            $validator->errors()->add('error_db', 'The key has already been taken.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $id = $request->id;
        $category = Games::findOrFail($id);
        $input = $request->all();


        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['image'] = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
            S3::deleteImage($category->image, 'uploads/games/', 'uploads/games/thumbnail/');
        }

        unset($input['_token']);
        unset($input['tools']);
        unset($input['key']);
        unset($input['categories']);

        if (!isset($input['forPaid'])) {
            $input['forPaid'] = 0;
        }
        //d($input,1);
        $affectedRows = Games::where('id', '=', $id)->update($input);
        $categories = $request->categories;
        GameCategories::where('game_id', $id)->delete();
        foreach ($categories as $category_id) {
            $categoryModel = new GameCategories();
            $categoryModel->category_id = $category_id;
            $categoryModel->game_id = $id;
            $categoryModel->updated_at = date("Y-m-d H:i:s");
            $categoryModel->save();
        }

        $input = array();
        $input['type_id'] = $id;
        $input['key'] = $request->key;
        $input['type'] = 'game';
        $url = Urls::saveUrl($input);

        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/games');
    }

    public function delete($id) {
        $model = Games::findOrFail($id);
        Games::where('id', '=', $id)->delete();
        GameCategories::where('game_id', '=', $id)->delete();
        S3::deleteImage($model->image, 'uploads/games/', 'uploads/games/thumbnail/');
        Urls::where('type', 'games')->where('type_id', $id)->delete();
        \Session::flash('success', 'Deleted Successfully!');
        return redirect('admin/games');
    }

    public function enable($id) {
        Games::where('id', $id)->update([
            'forPaid' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        Session::flash('success', 'Successfully Enabled!');
        return back();
    }

    public function disable($id) {
        Games::where('id', $id)->update([
            'forPaid' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        Session::flash('success', 'Successfully Disabled!');
        return back();
    }

    public function publish($id) {
        Games::where('id', $id)->update(['isPublished' => 1, 'updated_at' => date('Y-m-d')]);
        Session::flash('success', 'Game successfully published!');
        return back();
    }

    public function unpublish($id) {
        Games::where('id', $id)->update(['isPublished' => 0, 'updated_at' => date('Y-m-d')]);
        Session::flash('success', 'Game successfully unpublished!');
        return back();
    }

}
