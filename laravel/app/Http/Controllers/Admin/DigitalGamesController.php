<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\MiniGames;
use Session;
use DB;
use Illuminate\Http\Request;
use Validator,
    Input,
    Redirect;
use Intervention\Image\Facades\Image as Image;
use App\GameResults;
use Excel;
use App\Aws\S3;

class DigitalGamesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        if (isset($_GET['title'])) {
            $data['title'] = $_GET['title'];
        }
        if (isset($_GET['gameType'])) {
            $data['gameType'] = $_GET['gameType'];
        }
        $data['page'] = $page;

//        $model = MiniGames::join('users as u', 'mini_games.user_id','=','u.id')
//                        ->select('mini_games.id as id', 'mini_games.title as title', 'mini_games.description as description', 'mini_games.hasPlayCode as hasPlayCode', 'mini_games.playCode as playCode', 'mini_games.isFeatured as isFeatured', 'mini_games.key as key', 'mini_games.time as time','u.email as email','u.firstName','u.lastName')
//                        ->orderBy('mini_games.id', 'desc')->get();
//
//
//
//        $data['model'] = $model;
        return view('admin.digital-games.index', $data);
    }

    public function downloadExcel($type) {
        $search = array();
        $search['excel'] = 1;
        $result = MiniGames::searchDigitalGames($search);

        return Excel::create('games', function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download($type);
    }

    public function listing(Request $request) {
        //d($request->all(),1);
        $data['search'] = $request->all();
        $data['model'] = MiniGames::searchDigitalGames($data['search']);
        return view('admin.digital-games.ajax.list', $data);
    }

    public function gameDetail(Request $request, $id) {
        $id = $request->id;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $model = MiniGames::leftjoin('users as u', 'mini_games.user_id', '=', 'u.id')
                        ->select('mini_games.id as id', 'mini_games.title as title', 'mini_games.description as description', 'mini_games.hasPlayCode as hasPlayCode', 'mini_games.playCode as playCode', 'mini_games.isFeatured as isFeatured', 'mini_games.key as key', 'mini_games.time as time', 'mini_games.gameType as gameType', 'mini_games.created_at as created_at', 'mini_games.playCount as playCount', 'u.email as email', 'u.firstName', 'u.lastName')->where('mini_games.id', '=', $id)->first();
        $data['model'] = $model;



        $data['results'] = GameResults::where('game_id', $id)->get();
        return view('admin.digital-games.details', $data);
    }

    public function featuredUpdate(Request $request) {

        //$data['model'] = MiniGames::where('isFeatured', '=', 1)->orderBy('sortOrder','asc' )->get();
        $input = $request->all();
        foreach ($input['list'] as $order => $id) {
            //d($order,1);
            MiniGames::where('id', '=', $id)->update(array('sortOrder' => $order+1));
        }

        Session::flash('success', 'Successfully Updated!');
        // return redirect()->back();
    }

    public function featured() {

        $data['model'] = MiniGames::where('isFeatured', '=', 1)->orderBy('sortOrder', 'asc')->get();
        return view('admin.digital-games.featured', $data);
    }

    public function delete($id) {
        MiniGames::where('id', '=', $id)->delete();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function enablePlayCode(Request $request, $id) {
        DB::table('mini_games')->where('id', $id)->update([
            'hasPlayCode' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('success', 'Successfully Enabled!');
        return back();
    }

    public function disablePlayCode(Request $request, $id) {
        DB::table('mini_games')->where('id', $id)->update([
            'hasPlayCode' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('success', 'Successfully Disabled!');
        return back();
    }

    public function enableFeatured(Request $request, $id) {
        DB::table('mini_games')->where('id', $id)->update([
            'isFeatured' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('success', 'Successfully Enabled!');
        return back();
    }

    public function disableFeatured(Request $request, $id) {
        DB::table('mini_games')->where('id', $id)->update([
            'isFeatured' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('success', 'Successfully Disabled!');
        return back();
    }

    public function image($id) {

        $model = MiniGames::find($id);
        $data['model'] = $model;
        return view('admin.digital-games.image', $data);
    }

    public function saveimage(Request $request, $id) {
        $validator = Validator::make($request->all(), ['image' => 'mimes:jpeg,png,gif']);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        
        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['image'] = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
            $model = MiniGames::where('id', $id)->update($input);
        }
        Session::flash('success', 'Successfully added the image!');
        return redirect('admin/digital-games');
    }

}
