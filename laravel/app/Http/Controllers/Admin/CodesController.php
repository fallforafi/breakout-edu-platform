<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input;
use Auth;
use App\Codes;
use Illuminate\Http\Request;

class CodesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $model= Codes
                ::select('codes.id','codes.code','codes.isUsed','uc.created_at','u.firstName','u.lastName','u.email')
                ->leftJoin('user_codes as uc','uc.code_id','=', 'codes.id')
                ->leftJoin('users as u','u.id','=', 'uc.user_id')
                ->orderBy('codes.id', 'desc');
        $count = $model->count();
        $model = $model->paginate(20);
        return view('admin.codes.index', compact('model','count'));
    }

    public function show() {

        $id = $_GET['node'];
        $model = Codes::find('id', '=', $id);
        // d($model,1);
        return view('admin.codes.index', compact('model', 'id'));
    }

    public function create() {
        $data = array();
        return view('admin.codes.create', $data);
    }

    public function insert(Request $request) {
        $validator = Validator::make($request->all(), [
                    'code' => 'required|min:9|max:9|unique:codes',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $model = new Codes;
        $model->code = $request->code;
        $model->created_at = date("Y-m-d H:i:s");
        $model->save();
        
        \Session::flash('success', 'New access code successfully added!');
        return redirect('admin/codes?id=' . $request->parent_id);
    }

    public function edit($id) {
        $model = Codes::findOrFail($id);
        return view('admin.codes.edit', compact('model'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $validator = Validator::make($request->all(), [
                    'code' => 'required|min:9|max:9|unique:codes,code,' . $id,
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $id = $request->id;
        $category = Codes::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        $affectedRows = Codes::where('id', '=', $id)->update($input);
        \Session::flash('message', 'Updated Successfully!');
        return redirect('admin/codes?id=' . $id);
    }

    public function delete($id) {
        $row = Codes::where('id', '=', $id)->delete();
        return redirect('admin/codes');
    }

}
