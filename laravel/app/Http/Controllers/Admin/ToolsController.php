<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use Validator,
    Input;
use Auth;
use App\Tools;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;

class ToolsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $model = Tools::orderBy('id', 'desc')->get();
        return view('admin.tools.index', compact('model'));
    }

    public function create() {
        $data = array();
        return view('admin.tools.create', $data);
    }

    public function insert(Request $request) {

        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:5|max:50|unique:tools',
                    'url' => 'required|min:3',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }


        $model = new Tools;

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = public_path() . '/uploads/tools/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';

            $extension = $file->getClientOriginalExtension();
            $fileName = rand(111, 999) . time() . '.' . $extension;
            $image = $destinationPath . '/' . $fileName;
            $upload_success = $file->move($destinationPath, $fileName);
            $upload = Image::make($image)->resize(480, 480)->save($destinationPathThumb . $fileName);
            $model->image = $fileName;
        }

        $model->title = $request->title;
        $model->url = $request->url;
        $model->created_at = date("Y-m-d H:i:s");
        $model->save();
        \Session::flash('success', 'New access code successfully added!');
        return redirect('admin/tools');
    }

    public function edit($id) {
        $model = Tools::findOrFail($id);
        return view('admin.tools.edit', compact('model'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:3|max:50',
                    'url' => 'required|min:3',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $id = $request->id;
        $category = Tools::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = public_path() . '/uploads/tools/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';

            $extension = $file->getClientOriginalExtension();
            $fileName = rand(111, 999) . time() . '.' . $extension;
            $image = $destinationPath . '/' . $fileName;
            $upload_success = $file->move($destinationPath, $fileName);
            $upload = Image::make($image)->resize(480, 480)->save($destinationPathThumb . $fileName);
            $input['image'] = $fileName;
        }

        $affectedRows = Tools::where('id', '=', $id)->update($input);
        \Session::flash('message', 'Updated Successfully!');
        return redirect('admin/tools');
    }

    public function delete($id) {
        $row = Codes::where('id', '=', $id)->delete();
        return redirect('admin/tools');
    }

}
