<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\UserCodes;
use App\Codes;
use App\MiniGames;
use App\Role;
use DB;
use Excel;
use Validator,
    Redirect;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;

class UsersController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        if (isset($_GET['firstName'])) {
            $data['firstName'] = $_GET['firstName'];
        }
        if (isset($_GET['lastName'])) {
            $data['lastName'] = $_GET['lastName'];
        }
        if (isset($_GET['email'])) {
            $data['email'] = $_GET['email'];
        }
        if (isset($_GET['role_id'])) {
            $data['role_id'] = $_GET['role_id'];
        }
        $data['page'] = $page;

        return view('admin.users.index', $data);
    }

    public function listing(Request $request) {
        // d($request->all(),1);
        $data['search'] = $request->all();
        $data['model'] = User::searchUser($data['search']);
        return view('admin.users.ajax.list', $data);
    }

    public function userDetail(Request $request, $id) {
        $userId = $request->id;

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $data = User::where('users.id', '=', $userId)->get();

        $code = UserCodes::where('user_id', '=', $userId)
                ->join('codes as c', 'c.id', '=', 'user_codes.code_id')
                ->select('user_codes.expires', 'c.code', 'c.code')
                ->first();

        $model = MiniGames::where('user_id', '=', $userId)
                ->where('status', '=', 1)
                ->where('deleted', '=', 0)
                ->paginate(10);

        $model->setPath($id);
        return view('admin.users.details', [
            'data' => $data,
            'model' => $model,
            'code' => $code,
            'page' => $page,
        ]);
    }

    public function edit($id) {
        $user_id = $id;
        $data['user'] = User::findOrFail($user_id);
        if (isset($data['user']->dob)) {
            list($year, $month, $date) = explode('-', $data['user']->dob);
            $data['user']->date = $date;
            $data['user']->month = $month;
            $data['user']->year = $year;
        }

        $role = Role::find($data['user']->role_id);
        $data['roles'] = Role::lists('role', 'id');
        $data['role'] = $role;
        return view('admin.users.edit', $data)->with('user_id', $user_id);
    }

    public function setexpirydate($id) {

        $code = UserCodes::where('user_id', '=', $id)
                ->join('codes as c', 'c.id', '=', 'user_codes.code_id')
                ->select('user_codes.expires', 'c.code', 'c.code')
                ->first();

        $data['user'] = User::findOrFail($id);

        if (isset($code->expires)) {
            $expires = $code->expires;
        } else {
            $expires = date("Y-m-d H:is");
        }

        $date = explode(' ', $expires);
        list($year, $month, $date) = explode('-', $date[0]);

        $data['user']->date = $date;
        $data['user']->month = $month;
        $data['user']->year = $year;
        return view('admin.users.expires', $data)->with('user_id', $id);
    }

    public function updateexpirydate(Request $request, $id) {


        $code = UserCodes::where('user_id', '=', $id)
                ->join('codes as c', 'c.id', '=', 'user_codes.code_id')
                ->select('user_codes.id as user_codes_id','user_codes.expires', 'c.code', 'c.code')
                ->first();

        if (empty($code)) {
            echo "insert";
            $code = Functions::generateRandomString(9);
            $model = new Codes;
            $model->code = $code;
            $model->created_at = date("Y-m-d H:i:s");
            $model->save();


            $modelCode = new UserCodes();
            $modelCode->user_id = $id;
            $modelCode->code_id = $model->id;
            $modelCode->created_at = date('Y-m-d H:i:s');
            $modelCode->expires = $request->year.'-'.$request->month.'-'.$request->date;
            $modelCode->save();
            Codes::where('id', $model->id)->update(array('isUsed' => 1));
        } else {
            $expires=$request->year.'-'.$request->month.'-'.$request->date;
            $update=array('updated_at'=>date('Y-m-d H:i:s'),'expires'=>$expires);
            UserCodes::where('id',$code['user_codes_id'])->update($update);
            //d($code,1);
            // UserCodes::where
//            $modelCode->expires = $request->year.'-'.$request->month.'-'.$request->date.
        }
        
        $request->session()->flash('success', 'Successfully updated!');
        return back();
    }

    public function update(Request $request) {
        $user_id = $request->id;
        $user = User::findOrFail($user_id);
        $rules = array(
            'firstName' => 'required|max:50',
            'lastName' => 'required|max:50',
            'email' => 'required|min:6|email|unique:users,email,' . $user->id,
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $input = $request->all();
            array_forget($input, '_token');
            User::where('id', '=', $user_id)->update($input);
            Session::flash('success', 'Your profile has been updated.');
            return redirect()->back();
        }
    }

    public function delete($id) {
        User::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function accept(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'status' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Approved!');
        return back();
    }

    public function reject(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'status' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Disapproved!');
        return back();
    }

    public function downloadExcel($type) {

        $result = User::where("users.deleted", '=', 0)->where("users.role_id", '!=', 1);
        $result = $result->leftJoin('roles as r', 'r.id', '=', 'users.role_id');
        $result = $result->leftJoin('user_codes as uc', 'uc.user_id', '=', 'users.id');
        $result = $result->select('users.id', 'users.firstName', 'users.lastName', 'users.email', 'users.created_at', 'r.role as role', 'users.loginCount as loginCount', 'uc.expires as expires');
        $result = $result->orderBy('id', 'desc');
        $result = $result->get();



        return Excel::create('users_data', function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download($type);
    }

}
