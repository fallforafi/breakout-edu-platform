<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input;
use App\Games;
use App\Categories;
use App\Tools;
use App\Urls;
use App\GameTools;
use App\GameCategories;
use Illuminate\Http\Request;
use App\Aws\S3;

class OutsideGamesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        if (isset($_GET['title'])) {
            $data['title'] = $_GET['title'];
        }
        if (isset($_GET['category_id'])) {
            $data['category_id'] = $_GET['category_id'];
        }
        $data['page'] = $page;
        $categories = Categories::get();
        $data['categories'] = $categories;
        return view('admin.outside-games.index', $data);
    }

    public function listing(Request $request) {
        //d($request->all(),1);
        $data['search'] = $request->all();
        $data['gameTyoe'] = 'outside';
        $data['model'] = Games::searchGames($data['search']);
        return view('admin.outside-games.ajax.list', $data);
    }

    public function create() {
        $data = array();
        $categories = Categories::lists('name', 'id');
        $data['categories'] = $categories;
        $data['key'] = null;
        $data['gameCategories'] = null;

        return view('admin.outside-games.create', $data);
    }

    public function insert(Request $request) {
        //d($request->all(),1);

        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:5|max:50|unique:games',
                    'categories' => 'required',
                    'tags' => 'required',
                    'note' => 'max:500',
                    'launchTool' => 'required',
                    'key' => 'required|unique:urls'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $model = new Games;

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $model->image = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
        }

        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->tags = $request->tags;
        $model->note = $request->note;
        $model->launchTool = $request->launchTool;
        $model->gameType = 'outside';
        $model->save();

        $categories = $request->categories;

        foreach ($categories as $category_id) {
            $categoryModel = new GameCategories();
            $categoryModel->category_id = $category_id;
            $categoryModel->game_id = $model->id;
            $categoryModel->created_at = date("Y-m-d H:i:s");
            $categoryModel->save();
        }



        $input = array();
        $input['type_id'] = $model->id;
        $input['key'] = $request->key;
        $input['type'] = 'game';
        Urls::saveUrl($input);


        \Session::flash('success', 'Successfully added!');
        return redirect('admin/outside-games');
    }

    public function edit($id) {
        $model = Games::findOrFail($id);
        $categories = Categories::lists('name', 'id');
        $tools = Tools::where('status', 1)->get();
        $gameToolsModel = GameTools::where('game_id', $id)->get();
        $gameTools = array();
        foreach ($gameToolsModel as $gt) {
            $gameTools[] = $gt->tool_id;
        }

        $gameCategoriesModel = GameCategories::where('game_id', $id)->get();
        $gameCategories = array();

        foreach ($gameCategoriesModel as $gc) {
            $gameCategories[] = $gc->category_id;
        }

        $url = Urls::where('type', 'game')->where('type_id', $id)->first();
        if (!empty($url)) {
            $key = $url->key;
        } else {
            $key = null;
        }

        return view('admin.outside-games.edit', compact('model', 'gameCategories', 'categories', 'tools', 'gameTools', 'key', 'url'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|min:5|max:50|unique:games,title,' . $id,
                    'categories' => 'required',
                    'tags' => 'required',
                    'note' => 'max:500',
                    'launchTool' => 'required',
                    'key' => 'required|unique:urls,type_id,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $urlCheck = Urls::where('type_id', '!=', $id)->where('type', '=', 'game')->where('key', $request->key)->get();

        if (count($urlCheck) > 0) {
            $validator->errors()->add('error_db', 'The key has already been taken.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $id = $request->id;
        $category = Games::findOrFail($id);
        $input = $request->all();


        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['image'] = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
            S3::deleteImage($category->image, 'uploads/games/', 'uploads/games/thumbnail/');
        }

        unset($input['_token']);
        //unset($input['tools']);
        unset($input['key']);
        unset($input['categories']);

        $affectedRows = Games::where('id', '=', $id)->update($input);

        $categories = $request->categories;
        GameCategories::where('game_id', $id)->delete();
        foreach ($categories as $category_id) {
            $categoryModel = new GameCategories();
            $categoryModel->category_id = $category_id;
            $categoryModel->game_id = $id;
            $categoryModel->updated_at = date("Y-m-d H:i:s");
            $categoryModel->save();
        }


        $input = array();
        $input['type_id'] = $id;
        $input['key'] = $request->key;
        $input['type'] = 'game';
        $url = Urls::saveUrl($input);

        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/outside-games');
    }

    public function delete($id) {
        //$row = Codes::where('id', '=', $id)->delete();
        $model = Games::findOrFail($id);
        Games::where('id', '=', $id)->delete();
        GameCategories::where('game_id', '=', $id)->delete();
        S3::deleteImage($model->image, 'uploads/games/', 'uploads/games/thumbnail/');
        Urls::where('type', 'games')->where('type_id', $id)->delete();
        \Session::flash('success', 'Deleted Successfully!');
        return redirect('admin/outside-games');
    }

}
