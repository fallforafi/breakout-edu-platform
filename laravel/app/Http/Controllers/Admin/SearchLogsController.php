<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use Validator,
    Input;
use Auth;
use App\SearchLogs;
use App\Functions\Functions;
use Illuminate\Http\Request;

class SearchLogsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $model = SearchLogs::orderBy('id', 'desc')->get();
        return view('admin.searchlogs.index', compact('model'));
    }

    public function delete($id) {
        $row = SearchLogs::where('id', '=', $id)->delete();
        return redirect('admin/searchlogs');
    }

}
