<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use DB;
use Auth;
use App\Categories;
use App\Urls;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\Aws\S3;

class CategoriesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    /**
     * All Cafe's For Admin.
     */
    public function index() {

        $categories = Categories::with('children')->paginate(15);
        $data['categories'] = $categories;
        return view('admin.categories.index', $data);
    }

    public function updategames() {


        $categories = Categories::get();

        foreach ($categories as $c) {

            $sql = "SELECT TRIM(RIGHT(TRIM(concat_ws(' ', ifnull(t1.id,''), ifnull(t2.id,''), ifnull(t3.id,''), ifnull(t4.id,'') )),2)) AS leaf,t4.name FROM categories AS t1 "
                    . "LEFT JOIN categories AS t2 ON t2.parent_id = t1.id "
                    . "LEFT JOIN categories AS t3 ON t3.parent_id = t2.id "
                    . "LEFT JOIN categories AS t4 ON t4.parent_id = t3.id WHERE "
                    . "t1.id =" . $c->id . ";";
            $model = DB::select($sql);
            $arr = array();
            foreach ($model as $category) {
                $arr[] = $category->leaf;
            }
            $ca = implode(',', $arr);
            $sql = "select count(g.id) as games from games g "
                    . "left join game_categories gc on gc.game_id=g.id where g.isPublished=1 and gc.category_id in (" . $ca . ")";
            $model = DB::select($sql);

            $array['games'][$c->name] = $model[0]->games;
            $affectedRows = Categories::where('id', '=', $c->id)->update(array('games' => $model[0]->games));
        }
        header('Content-Type: application/json');
        echo json_encode($array);
        die();
    }

    public function show() {

        $id = $_GET['node'];
        $model = Categories::where('parent_id', '=', $id)->get();
        return view('admin.categories.children', compact('model', 'id'));
    }

    public function create() {
        $categories = Categories::lists('name', 'id');
        $categories[0] = 'Root Category';
        $key = null;
        return view('admin.categories.create', compact('categories', 'key'));
    }

    public function insert(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255|unique:categories',
                    'image' => 'mimes:jpeg,bmp,png,gif',
                    'key' => 'required|unique:urls'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $model = new Categories;

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $model->image = S3::storeImage($fileName, $file, 'uploads/categories/', 'uploads/categories/thumbnail/', 400, 400);
        }

        if (Input::hasFile('coverImage')) {

            $file = Input::file('coverImage');
            $fileName = rand(111, 999) . time();
            $model->coverImage = S3::storeImage($fileName, $file, 'uploads/categories/cover_images/');
        }

        $model->name = $request->name;
        $model->teaser = $request->teaser;
        $model->parent_id = $request->parent_id;
        $model->description = $request->description;
        $model->sortOrder = $request->sortOrder;
        $model->save();

        $input = array();
        $input['type_id'] = $model->id;
        $input['key'] = Functions::slugify($request->name);
        $input['type'] = 'category';
        $url = Urls::saveUrl($input);
        \Session::flash('success', 'Category Added Successfully!');
        return redirect('admin/categories');
    }

    public function edit($id) {
        $category = Categories::findOrFail($id);
        $categories = Categories::lists('name', 'id');
        $categories[0] = 'Root Category';

        $url = Urls::where('type', 'category')->where('type_id', $id)->first();
        if (!empty($url)) {
            $key = $url->key;
        } else {
            $key = null;
        }

        return view('admin.categories.edit', compact('category', 'categories', 'key', 'url'))->with('id', $id);
    }

    public function update($id, Request $request) {

        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'image' => 'mimes:jpeg,bmp,png,gif',
        ]);

        $key = Functions::slugify($request->name);
        $urlCheck = Urls::where('type_id', '!=', $id)->where('type', '=', 'category')->where('key', $key)->get();

        if (count($urlCheck) > 0) {
            $validator->errors()->add('error_db', 'The key has already been taken.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $id = $request->id;

        $category = Categories::findOrFail($id);
        $input = $request->all();

        ////

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $fileName = rand(111, 999) . time();
            $input['image'] = S3::storeImage($fileName, $file, 'uploads/categories/', 'uploads/categories/thumbnail/', 400, 400);
            S3::deleteImage($category->image, 'uploads/categories/', 'uploads/categories/thumbnail/');
        }

        if (Input::hasFile('coverImage')) {

            $file = Input::file('coverImage');
            $fileName = rand(111, 999) . time();
            $input['coverImage'] = S3::storeImage($fileName, $file, 'categories/cover_images/', 'categories/cover_images/thumbnail', 400, 400);
            S3::deleteImage($category->image, 'categories/cover_images/');
        }

        unset($input['_token']);
        unset($input['key']);
        $affectedRows = Categories::where('id', '=', $id)->update($input);

        $input = array();
        $input['type_id'] = $id;
        $input['key'] = Functions::slugify($key);
        $input['type'] = 'category';
        $url = Urls::saveUrl($input);

        \Session::flash('message', 'Updated Successfully!');
        return redirect('admin/categories');
    }

    public function sorting() {

        $data['model'] = Categories::orderBy('sortOrder', 'asc')->get();
        return view('admin.categories.sorting', $data);
    }

    public function sortingUpdate(Request $request) {

        $input = $request->all();
        foreach ($input['list'] as $order => $id) {
            //d($order,1);
            Categories::where('id', '=', $id)->update(array('sortOrder' => $order + 1));
        }

        Session::flash('success', 'Successfully Updated!');
        // return redirect()->back();
    }

    public function delete($id) {
        $category = Categories::findOrFail($id);
        Categories::where('id', '=', $id)->delete();
        S3::deleteImage($category->coverImage, 'uploads/categories/cover_images/');
        S3::deleteImage($category->image, 'uploads/categories/', 'uploads/categories/thumbnail/');
        Urls::where('type', 'category')->where('type_id', $id)->delete();
        return redirect('admin/categories');
    }

}
