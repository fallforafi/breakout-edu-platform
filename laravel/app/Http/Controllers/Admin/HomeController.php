<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Games;
use App\MiniGames;
use App\Categories;
use Illuminate\Support\Facades\Storage;
use Config;
class HomeController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //$files=Storage::allFiles();
       // d($files);
      //  die;
        $data = array();
        $data['totalUsers'] = User::where('role_id', '!=', 1)->get();
        $data['recentUsers'] = User::where('role_id', '!=', 1)->orderBy('id', 'desc')->limit(10)->get();
        $data['totalGames'] = Games::where('deleted', 0)->get();
        $data['recentGames'] = Games::where('deleted', 0)->orderBy('id', 'desc')->limit(10)->get();
        $data['totalMiniGames'] = MiniGames::get();
        $data['recentMiniGames'] = MiniGames::where('deleted', 0)->orderBy('id', 'desc')->limit(10)->get();
        
        $data['totalCategories'] = Categories::get();
        return view('admin.home', $data);
    }

}
