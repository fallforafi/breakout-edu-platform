<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\ProfileReports;
use App\Notifications;
use App\Languages;
use App\Skills;
use App\UsersTransports;
use App\Educations;
use App\Reviews;
use App\Tasks;
use App\Friends;
use App\Functions\Functions;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($id) {
        $user_id = $id;
        $login_id = Auth::user()->id;
        $mutualFriends = array();
        if ($login_id == $id) {
            $mutualFriends = Friends::getMutualFriends(Auth::user()->id, $id);
        }
        $data['mutualFriends'] = $mutualFriends;
        $data['user'] = User::findOrFail($user_id);
        $data['education'] = Educations::getEducations($user_id);
        $data['language'] = Languages::getLanguages($user_id);
        $data['skill'] = Skills::getSkills($user_id);
        $getTransport = UsersTransports::where('user_id', '=', $user_id)->get();

        $data['tasks_posted'] = Tasks::search(array('fiter_category_id' => 0, 'filter_search' => '', 'task_status' => 'open', 'user_id' => $user_id));
        $data['tasks_request'] = Tasks::search(array('fiter_category_id' => 0, 'filter_search' => '', 'task_status' => 'open', 'user_id' => $login_id));
        $data['completed'] = Offers::where('user_id', $id)->where('status', 1)->where('deleted', 0)->where('offerStatus', 'completed')->get();

        $data['transport'] = array();
        foreach ($getTransport as $value) {
            $paramss[] = $value->transport_id;
            $data['transport'] = $paramss;
        }
        $data['accounts'] = SocialAccounts::byUserId($id);
        $search['user_id'] = $id;
        $data['user_reviews'] = Reviews::get($search);

        return view('front.profile.index', $data)->with('user_id', $user_id);
    }

    public function store(Request $request) {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $rules = array(
            'type_id' => 'required',
        );
        $message = [
            'type_id.required' => 'Select any one of the posted task'
        ];
        $validator = Validator::make($input, $rules, $message);
        $validator->after(function ($validator) {
            $offers = Notifications::where('user_id', Auth::user()->id)->where('type_id', $_POST['type_id'])->get();
            if (count($offers) > 0) {
                $validator->errors()->add('quote_exist', 'You have already request a quote for this.');
            }
        });
        $response['error'] = 0;
        $response['login'] = 0;
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $task = Tasks::find($input['type_id']);
            
            $user=User::find($input['notify_to']);
            $input['created_at'] = date("Y-m-d H:i:s");
            unset($input['_token']);
            $notification['task'] = $task;
            $notification['notify_to'] = $input['notify_to'];
            $notification['notify_email'] = $user->email;
            $notification['type'] = 'task_quote';
            $notification['type_id'] = $input['type_id'];
            UsersNotifications::save($notification);
            $response['success'] = true;
        }
        return json_encode($response);
    }

    public function changecoverposition(Request $request) {

        if (Auth::user()->id == $request->user_id) {
            echo User::where('id', $request->user_id)->update(array('coverPosition' => $request->position));
        }
    }

    public function report($id) {
        $input['from'] = Auth::user()->id;
        $notification['user'] = User::find($input['from']);
//        $notification['notify_to'] = $task->user_id;
        $notification['notify_email'] = 'admin@gmail.com';
        $notification['type'] = 'report_user';
        $notification['type_id'] = '';
        $notification['name'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;
        $notification['image'] = Auth::user()->image;
        //$notification['user_id'] = $model->user_id;
        //UsersNotifications::save($notification);
        $report = ProfileReports::insertGetId([
                    'from' => $input['from'],
                    'to' => $id
        ]);
        if (isset($report)) {
            //echo 'Success';
            $subject = view('emails.' . $notification['type'] . '.subject', compact('type_id', 'type', 'data', 'notification'));
            $body = view('emails.' . $notification['type'] . '.body', compact('type_id', 'type', 'data', 'notification'));
            Functions::sendEmail($notification['notify_email'], $subject, $body);
        }
    }

}
