<?php

namespace App\Http\Controllers;

use App\Games;
use App\MiniGames;
use Input;
use Auth;
use DB;
use App\SearchLogs;

class SearchController extends Controller {

    protected $categories = array();
    protected $layout = 'layouts.search';

    public function __construct() {
        $this->categories = 1;
    }

    public function index() {

        $query = Input::get('q');
        SearchLogs::addLog($query);
        
        if ($query <> "") {

            $sql = "select g.title,g.image,g.tags,url.key,c.name from games as g "
                    . "join game_categories as gc on g.id=gc.game_id "
                    . "join categories as c on c.id=gc.category_id "
                    . "join urls as url on url.type_id=g.id "
                    . " where (g.title like '%" . $query . "%' or c.name like '%" . $query . "%' or tags like '%" . $query . "%' ) and g.isPublished=1 and url.type='game' ";


            /*
              if (isset(Auth::user()->id) && Auth::user()->role->role != 'normal') {
              $sql.="";
              } else {
              $sql.=" g.forPaid=1";
              }
             */

            $sql.=" group by g.id order by g.title;";
            $model = DB::select($sql);
            $data['model'] = $model;
            return view('front.search.ajax.list', $data);
        }
    }

    public function submit() {
        $query = Input::get('q');
//        if ($query == '') {
//            return redirect('/');
//        }
        $sql = "select g.title,g.image,g.tags,url.key,c.name from games as g "
                . "join game_categories as gc on g.id=gc.game_id "
                . "join categories as c on c.id=gc.category_id "
                . "join urls as url on url.type_id=g.id "
                . " where (g.title like '%" . $query . "%' or c.name like '%" . $query . "%' or tags like '%" . $query . "%' ) and g.isPublished=1 and url.type='game' ";


        $sql.=" group by g.id order by g.title;";
        $model = DB::select($sql);

        $data['model'] = $model;
        $data['q'] = $query;

        $miniGames = MiniGames::select('mini_games.title', 'mini_games.key')
                ->where('mini_games.title', 'like', '%' . $query . '%')
                ->where('mini_games.deleted', 0)->where('mini_games.status', 1)
->where('mini_games.isFeatured', 1)
                ->orderBy('mini_games.title', 'asc')
                ->get();
        if (isset(Auth::user()->id) && Auth::user()->role->role == 'paid') {
            $data['miniGames'] = $miniGames;
        } else {
            $data['miniGames'] = Null;
        }
        $data['miniGames']=array();
        return view('front.search.index', $data);
//return redirect('game/' . $model->key);
    }

}
