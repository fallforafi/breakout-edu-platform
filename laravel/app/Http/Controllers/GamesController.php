<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Categories;
use App\Games;
use App\Urls;
use App\GameTools;
use Auth;
use App\Functions\Functions;

class GamesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function game($key) {

        $url = Urls::where('key', $key)->where('type', 'game')->first();
        $game = Games::find($url->type_id);

        if (isset(Auth::user()->id) && Auth::user()->role->role == 'user' && $game->forPaid == 1) {
            return redirect('accesscode');
        }
        $breadcrumbs[0]['url'] = url('categories');
        $breadcrumbs[0]['name'] = "Subject Pack";
        $i = 1;
        $parents = array();
        if (isset($_SERVER['HTTP_REFERER'])) {
            $p = explode('/', $_SERVER['HTTP_REFERER']);
            $p = end($p);
            if (strpos($p, "?")) {
                $p = substr($p, 0, strpos($p, "?"));
            }

            
            if ($p == "query" || $p == "") {
                
            } else {
                $urlCategory = Urls::where('key', $p)->where('type', 'category')->first();
                $category = Categories::getCategoryGames($urlCategory->type_id);
                $data['category'] = $category[0];
                $arr = Categories::getAllCategories();
                $categories = Functions::categoriesArray($arr);
                $parent_id = $category[0]->parent_id;
                $parents = Functions::getParents($parent_id, $categories);
                foreach ($parents as $parent) {
                    $breadcrumbs[$i]['url'] = url('') . "/category/" . $parent['key'];
                    $breadcrumbs[$i]['name'] = $parent['name'];
                    $i++;
                }
                $breadcrumbs[$i]['url'] = url('') . "/category/" . $category[0]->key;
                $breadcrumbs[$i]['name'] = $category[0]->name;
                
            }
        }
        
        $gameToolsModel = GameTools::where('game_id', $game->id)->get();
        $data['tools'] = $gameToolsModel;
        $data['url'] = $url;
        $data['game'] = $game;
        $i++;
        $breadcrumbs[$i]['url'] = "#_";
        $breadcrumbs[$i]['name'] = $game->title;
        $data['breadcrumbs'] = $breadcrumbs;

        if ($game->gameType == 'outside') {
            return view('front.outside-games.index', $data);
        } else {
            return view('front.games.index', $data);
        }
    }

    public function error() {
        return view('front.common.non_paid_error');
    }

}
