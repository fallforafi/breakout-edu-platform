<?php

namespace App\Http\Controllers;

use DB;
use App\Categories;
use App\Content;
use App\MiniGames;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\Codes;
use Auth;


class HomeController extends Controller {

    protected $categories = array();
    protected $layout = 'layouts.search';

    public function __construct() {
        $this->categories = 1;
    }

    public function track() {
        $data = array();
        $model = new Codes();
        $model->code = Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3) . '-' . Functions::generateRandomString(3);
        $model->created_at = date("Y-m-d H:i:s");
        $model->save();
        // return view('front.codes.index', $data);
    }

    public function featured() {


        $featuredMiniGames = MiniGames::
                select("mini_games.id", "mini_games.title", "mini_games.key", "mini_games.image", "mini_games.id", DB::raw('count(gl.id) as locks'))
                ->where('mini_games.isFeatured', 1)
                ->where('mini_games.status', 1)
                ->where('mini_games.deleted', 0)
                ->join('game_locks as gl', 'mini_games.id', '=', 'gl.game_id')
                ->orderBy('mini_games.sortOrder', 'asc')
                ->groupBy('mini_games.id')
                ->get();
        $data['featuredMiniGames'] = $featuredMiniGames;
        return view('front.featured', $data);
    }

    public function categories() {

        $categories = Categories::getCategoryGames('', 0);
        foreach ($categories as $c) {
            $cids[] = $c->id;
        }

        $data['cids'] = $cids;

        $data['categories'] = $categories;
        return view('front.categories', $data);
    }

    public function getgames(Request $request) {
        
        
        //$array['games'] = 2;
        //header('Content-Type: application/json');
        //echo json_encode($array);
        //die;
        $sql = "SELECT TRIM(RIGHT(TRIM(concat_ws(' ', ifnull(t1.id,''), ifnull(t2.id,''), ifnull(t3.id,''), ifnull(t4.id,'') )),2)) AS leaf,t4.name FROM categories AS t1 "
                . "LEFT JOIN categories AS t2 ON t2.parent_id = t1.id "
                . "LEFT JOIN categories AS t3 ON t3.parent_id = t2.id "
                . "LEFT JOIN categories AS t4 ON t4.parent_id = t3.id WHERE "
                . "t1.id =" . $request->id . ";";
        $model = DB::select($sql);
        foreach ($model as $category) {
            $arr[] = $category->leaf;
        }
        $c = implode(',', $arr);
        $sql = "select count(g.id) as games from games g "
                . "left join game_categories gc on gc.game_id=g.id where g.isPublished=1 and gc.category_id in (" . $c . ")";
        $model = DB::select($sql);
        $array['games'] = $model[0]->games;
        header('Content-Type: application/json');
        echo json_encode($array);
        die();
    }

    public function index() {
        $data = array();

        $categories = Categories::getCategoryGames('', 0);
        $cids = array();
        foreach ($categories as $c) {
            $cids[] = $c->id;
        }

        $data['cids'] = $cids;


        $miniGames = array();
       
        if (isset(Auth::user()->id)) {
            $user_id = Auth::user()->id;
            $miniGames = MiniGames::
                    select("mini_games.id", "mini_games.title", "mini_games.key", "mini_games.id", DB::raw('count(gl.id) as locks'))
                    ->where('mini_games.status', 1)
                    ->where('mini_games.deleted', 0)
                    ->where('mini_games.user_id', $user_id)
                    ->join('game_locks as gl', 'mini_games.id', '=', 'gl.game_id')
                    ->orderBy('mini_games.id', 'desc')
                    ->groupBy('mini_games.id')
                    ->limit(6)
                    ->get();
        }

        $featuredMiniGames = MiniGames::
                select("mini_games.id", "mini_games.title", "mini_games.key", "mini_games.image", "mini_games.id", DB::raw('count(gl.id) as locks'))
                ->where('mini_games.isFeatured', 1)
                ->where('mini_games.status', 1)
                ->where('mini_games.deleted', 0)
                ->join('game_locks as gl', 'mini_games.id', '=', 'gl.game_id')
                ->orderBy('mini_games.sortOrder', 'asc')
                ->groupBy('mini_games.id')
                ->limit(8)
                ->get();
        $data['categories'] = $categories;
        $data['miniGames'] = $miniGames;
        $data['featuredMiniGames'] = $featuredMiniGames;
        $data['cids'] = $cids;

        return view('front.index', $data);
    }

    public function share($id) {
        $model = MiniGames::find($id);
        $data['model'] = $model;
        return view('front.mini_games.includes.share', $data);
    }

    public function page() {

        $model = Content::where('code', 'home')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function aboutus() {
        $model = Content::where('code', 'aboutus')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function faqs() {
        $model = Content::where('code', 'faq')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function privacy() {
        $model = Content::where('code', 'privacy')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function terms() {
        $model = Content::where('code', 'terms')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function messagePost(Request $request) {
        $validation = array('name' => 'required|max:30',
            'email' => 'required|email|max:30',
            'captcha' => 'required|captcha',
            'message' => 'required|min:6|max:200');

        $messages = [
            'captcha' => 'The :attribute field is invalid.',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $guestbook = new Guestbook;
        $guestbook->name = $request->name;
        $guestbook->email = $request->email;
        $guestbook->message = $request->message;
        $guestbook->save();
        return redirect('guestbook')->withInput();
    }

}
