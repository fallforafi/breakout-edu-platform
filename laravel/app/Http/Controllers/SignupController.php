<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\UserCodes;
use App\Codes;
use Auth;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mbarwick83\TwitterApi\TwitterApi;
use Session;
use Abraham\TwitterOAuth\TwitterOAuth;

class SignupController extends Controller {

    use AuthenticatesAndRegistersUsers;

    protected $loginPath = 'login';

    public function __construct(Guard $auth, Registrar $registrar, TwitterApi $twitter) {
        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->twitter = $twitter;
        $this->middleware('guest', ['except' => 'success']);
    }

    public function login() {
        return view('front.customers.login');
    }

    public function signupform() {
        return view('front.customers.signupform');
    }

    public function loginform() {
        return view('front.customers.login_form');
    }

    public function register() {
        $pathInfo = (explode("/", $_SERVER['REQUEST_URI']));
        $view = 'front.customers.register_' . end($pathInfo);
        return view($view, compact('specialities'));
    }

    public function fblogin(Request $request) {
        session_start();
        $user = User::where('email', $request->email)->first();
        $response = array();
        $count = count($user);
        if ($count == 0) {
            $confirmation_code = str_random(40);
            $user = new User;
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->role_id = 2;
            $user->joinFrom = $request->joinFrom;
            $user->isConfirmed = 1;
            $user->confirmationCode = $confirmation_code;
            $user->status = 1;
            $user->password = 1;
            $user->save();
        }
        $response['login'] = false;
        $response['redirect']='home';
        if (Auth::loginUsingId($user->id)) {

            $sessionId = session_id();
            $response['login'] = true;
            self::loginCount();
            $response['id'] = $user->id;
            $redirect = UserCodes::checkUser($user->id);

            /*
              if($count==0){
              $response['redirect'] = 'accesscode';
              }else{
              
              }
             */
        }
        $response['redirect'] = $redirect;
        echo json_encode($response);
    }

    public function twitter() {
        //$user_id = Auth::user()->id;
        $url = $this->twitter->authorizeUrl();
        return redirect($url);
    }

    public function twittercallback(Request $request) {

        $token = $request->oauth_token;
        $verify = $request->oauth_verifier;

        //$user_id = Auth::user()->id;
        if (isset($request->oauth_token) && Session::get('oauth_token') !== $request->oauth_token) {
            Session::forget('oauth_token');
            Session::forget('oauth_token_secret');
            abort(404);
        }

        $twitter = new TwitterApi(Session::get('oauth_token'), Session::get('oauth_token_secret'));
        $accessToken = $twitter->accessToken($request->oauth_verifier);
        $connection = new TwitterOAuth(env('TWITTER_CONSUMER_KEY'), env('TWITTER_CONSUMER_SECRET'), $accessToken['oauth_token'], $accessToken['oauth_token_secret']);

        $data = $connection->get("account/verify_credentials", ['include_email' => 'true']);
        $names = explode(' ', $data->name);
        $firstName = $names[0];

        if (isset($names[1])) {
            $lastName = $names[1];
        } else {
            $lastName = ' ';
        }

        $user = User::where('email', $data->email)->first();
        $response = array();
        if (count($user) == 0) {
            $confirmation_code = str_random(40);
            $user = new User;
            $user->firstName = $firstName;
            $user->lastName = $lastName;
            $user->email = $data->email;
            $user->role_id = 2;
            $user->joinFrom = 'twitter';
            $user->remember_token = bcrypt(time());
            $user->isConfirmed = 1;
            $user->confirmationCode = $confirmation_code;
            $user->status = 1;
            $user->password = 1;
            $user->save();
        }
        $response['login'] = false;

        if (Auth::loginUsingId($user->id)) {

            $sessionId = session_id();
            $response['login'] = true;
            $response['id'] = $user->id;
            self::loginCount();
            return redirect('/');
        }
        return redirect('/');
    }

    public function signup() {
        return view('front.customers.signup');
    }

    public function forgot_password() {
        return view('front.customers.forgot');
    }

    public static function loginCount() {

        $user_id = Auth::user()->id;
        User::where('id', $user_id)->increment('loginCount');
    }

    public function postLogin(Request $request) {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            session_start();
            self::loginCount();
            $role = Auth::user()->role->role;
            if ($role == 'admin') {
                return redirect('admin');
            }

            $user_id = Auth::user()->id;
            $redirect = UserCodes::checkUser($user_id);
            return redirect()->intended($redirect);
        }

        return redirect($this->loginPath())
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }

    public function store(Request $request) {

        $validation = array(
            'firstName' => 'required|max:20',
            'lastName' => 'required|max:20',
            'title' => 'required|max:6',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6',
            'code' => 'min:9|max:9',
        );
        $validator = Validator::make($request->all(), $validation);
        $response['error'] = 0;


        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        }

        $validCode = 0;

        if ($request->code != "") {
            $code = Codes::where('code', $request->code)->where('status', 1)->first();

            if (count($code) == 0) {
                $response['error'] = 1;
                $validator->errors()->add('invalid_code', 'Invalid code.');
            } else if ($code->isUsed == 1) {
                $validator->errors()->add('invalid_code', 'This code is already used.');
                //return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
            } else {
                $validCode = 1;
            }
        }

        if ($request->ajax() == 1) {
            $response['token'] = csrf_token();
            echo json_encode($response);
            die;
        }

        if ($response['error'] == 0) {
            $confirmation_code = str_random(40);
            $user = new User;
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->role_id = $request->role_id;
            $user->title = $request->title;
            $user->password = bcrypt($request->password);
            $user->status = 1;
            $user->confirmationCode = $confirmation_code;
            $user->remember_token = bcrypt(time());
            $user->save();

            $subject = view('emails.confirm_email.subject');
            $body = view('emails.confirm_email.body', compact('confirmation_code'));
            Functions::sendEmail($request->email, $subject, $body);

            if ($validCode == 1) {
                $code = UserCodes::insertUserCode($user, $code);
            }
            $credentials = $request->only('email', 'password');
            if ($this->auth->attempt($credentials, 1, 1)) {
                $response['login'] = 1;
                session_start();

                $user_id = Auth::user()->id;
                $redirect = UserCodes::checkUser($user_id);
                Session::flash('success', 'Thanks for signing up!');
                return redirect()->intended($redirect);
                //return redirect('accesscode');
            }
            $response['valid_code'] = $validCode;
        } else {
            return redirect('register')->withInput($request->all())->withErrors($validator->errors());
        }
        return redirect('/');
    }

    public function confirmEmail($confirmation_code) {
        if (!$confirmation_code) {
            return 'Error! Confirmation Key missing.';
        }
        $user = User::where('confirmationCode', $confirmation_code)->first();
        if (!$user) {
            return 'Error! Confirmation Key missing.';
        }
        $user->isConfirmed = 1;
        $user->save();
        return redirect('register/success/' . $user->id);
    }

    public function success($id) {
        $this->middleware('auth');
        $user = User::findOrFail($id);
        $data['user'] = $user;
        return view('front.customers.register_success', $data);
    }

}
