<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Categories;
use App\Games;
use App\Urls;
use App\Functions\Functions;

class CategoriesController extends Controller {

    public $p = array();

    public function __construct() {
        $this->middleware('auth');
    }

    public function category($key) {

        $url = Urls::where('key', $key)->where('type', 'category')->first();
        $games = Games::getGames(array('category_id' => $url->type_id,'isPublished' => 1));

        $arr = Categories::getAllCategories();
        $categories = Functions::categoriesArray($arr);
        $category = Categories::getCategoryGames($url->type_id);
        $parent_id = $category[0]->parent_id;
        $parents = Functions::getParents($parent_id, $categories);
        $cids = array();
        $childCategories = Categories::leftJoin('urls as ul', 'ul.type_id', '=', 'categories.id')
                ->select('categories.*', 'ul.key')
                ->where('ul.type', 'category')
                ->where('categories.parent_id', $url->type_id)
                ->orderBy('categories.sortOrder', 'asc')
                ->get();
        if (count($childCategories) > 0) {
            foreach ($childCategories as $value) {
                $childCategoriesGames[] = Categories::getCategoryGames($value->id);
            }
            foreach ($childCategoriesGames as $c) {
                $cids[] = $c[0]->id;
            }
        } else {
            $childCategoriesGames = array();
        }

        $data['cids'] = $cids;
        $data['url'] = $url;
        $data['category'] = $category;
        $data['games'] = $games;
        $data['parents'] = $parents;
        $data['childCategoriesGames'] = $childCategoriesGames;

        return view('front.categories.index', $data);
    }

}
