<?php

namespace App\Http\Controllers;

use Auth;
use App\Codes;
use App\User;
use App\Role;
use App\UserCodes;
use Illuminate\Http\Request;
use Validator;
use App\Functions\Functions;

class CodesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        
        $role=Role::find(Auth::user()->role_id);
        
        if($role->role == 'paid' || $role->role == 'admin'){
            return redirect('/');
            die;
        }
        $data = array();
        return view('front.codes.index', $data);
    }

    public function check(Request $request) {
        $validation = array(
            'code' => 'required|min:9|max:9',
        );

        $validator = Validator::make($request->all(), $validation);
        $data = array();
        
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $code = Codes::where('code', $request->code)->where('status', 1)->first();
        if (count($code) == 0) {
            $validator->errors()->add('invalid_code', 'Invalid code.');
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        if ($code->isUsed == 1) {
            $validator->errors()->add('invalid_code', 'This code is already used.');
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $code = UserCodes::insertUserCode($user, $code);
        return redirect()->intended('register/success/' . $user_id);
    }

}
