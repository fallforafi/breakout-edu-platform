<?php

namespace App\Http\Controllers;

use Session;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use Illuminate\Http\Request;
use App\ShopifyOrders;
use App\Functions\Functions;
use App\Codes;

class SubscriptionController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        
    }

    public function track(Request $request) {
        $count = ShopifyOrders::where('shopify_order_id',$request->id)->count();
        if ($count == 0) {
            $model = new ShopifyOrders();
            $model->shopify_order_id = $request->id;
            $model->orderCount = $request->count;
            $model->email = $request->email;
            $model->save();
            
            for ($i = 1; $i <= $request->count; $i++) {
                $code = Functions::generateRandomString(9);
                $modelCode = new Codes;
                $modelCode->code = $code;
                $modelCode->created_at = date("Y-m-d H:i:s");
                $modelCode->save();
                $codes[$i] = $code;
            }

            $subject = view('emails.codes.subject', compact('codes'));
            $body = view('emails.codes.body', compact('codes'));
            //echo $request->email;
            echo $mail = Functions::sendEmail($request->email, $subject, $body);
        }
    }

}
