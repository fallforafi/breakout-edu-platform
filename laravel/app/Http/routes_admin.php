
<?php

Route::group(
        array('prefix' => 'admin'), function() {
    $admin = "Admin\\";

    Route::get('/', $admin . 'HomeController@index');
    Route::get('/users', $admin . 'UsersController@index');
    Route::get('/user/{id}', $admin . 'UsersController@userDetail');
    Route::get('/user/edit/{id}', $admin . 'UsersController@edit');
    Route::post('/user/update', $admin . 'UsersController@update');
    Route::get('users/listing', $admin . 'UsersController@listing');

//    Route::get('importExport', 'UsersController@importExport');
    Route::get('/download/{type}', $admin . 'UsersController@downloadExcel');
    Route::post('/importExcel', $admin . 'UsersController@importExcel');

    Route::get('/admin-users', $admin . 'AdminUsersController@index');
    Route::get('/admin-user/{id}', $admin . 'AdminUsersController@userDetail');
    Route::get('/admin-users/add', $admin . 'AdminUsersController@create');
    Route::post('/admin-users/store', $admin . 'AdminUsersController@store');
    Route::get('/user/approve/{id}', $admin . 'UsersController@accept');
    Route::get('/user/disapprove/{id}', $admin . 'UsersController@reject');
    Route::get('user/edit/{id}', $admin . 'UsersController@edit');
    Route::get('user/delete/{id}', $admin . 'UsersController@delete');
    Route::get('user/setexpirydate/{id}', $admin . 'UsersController@setexpirydate');
    Route::post('user/updateexpirydate/{id}', $admin . 'UsersController@updateexpirydate');


    Route::get('categories', $admin . 'CategoriesController@index');
    Route::get('categories/create', $admin . 'CategoriesController@create');
    Route::get('categories/createSubcat', $admin . 'CategoriesController@create_sub_cat');
    Route::post('categories/storeSubcat', $admin . 'CategoriesController@store_sub_cat');
    Route::post('categories/insert', $admin . 'CategoriesController@insert');
    Route::get('categories/delete/{id}', $admin . 'CategoriesController@delete');
    Route::get('categories/edit/{id}', $admin . 'CategoriesController@edit');
    Route::get('categories/show', $admin . 'CategoriesController@show');
    Route::post('categories/update/{id}', $admin . 'CategoriesController@update');
    Route::get('categories/set/featured/update', $admin . 'DigitalGamesController@featuredUpdate');

    Route::get('categories/sorting/update', $admin . 'CategoriesController@sortingUpdate');
    Route::get('categories/sorting', $admin . 'CategoriesController@sorting');

    Route::get('categories/updategames', $admin . 'CategoriesController@updategames');
    
    Route::get('content', $admin . 'ContentController@index');
    Route::get('content/create', $admin . 'ContentController@create');
    Route::post('content/insert', $admin . 'ContentController@insert');
    Route::get('content/edit/{id}', $admin . 'ContentController@edit');
    Route::post('content/update/{id}', $admin . 'ContentController@update');
    Route::get('content/delete/{id}', $admin . 'ContentController@delete');

    Route::get('contactus', $admin . 'ContactusController@index');
    Route::get('contactusdetail/{id}', $admin . 'ContactusController@detail');

    Route::get('blog/categories', $admin . 'BlogCategoriesController@index');
    Route::get('blog/categories/create', $admin . 'BlogCategoriesController@create');
    Route::post('blog/categories/insert', $admin . 'BlogCategoriesController@insert');
    Route::get('blog/categories/delete/{id}', $admin . 'BlogCategoriesController@delete');
    Route::get('blog/categories/edit/{id}', $admin . 'BlogCategoriesController@edit');
    Route::post('blog/categories/update/{id}', $admin . 'BlogCategoriesController@update');

    Route::get('blog/posts', $admin . 'BlogPostsController@index');
    Route::get('blog/posts/create', $admin . 'BlogPostsController@create');
    Route::post('blog/posts/insert', $admin . 'BlogPostsController@insert');
    Route::get('blog/posts/delete/{id}', $admin . 'BlogPostsController@delete');
    Route::get('blog/posts/edit/{id}', $admin . 'BlogPostsController@edit');
    Route::post('blog/posts/update/{id}', $admin . 'BlogPostsController@update');

    Route::get('codes', $admin . 'CodesController@index');
    Route::get('codes/create', $admin . 'CodesController@create');
    Route::post('codes/insert', $admin . 'CodesController@insert');
    Route::get('codes/edit/{id}', $admin . 'CodesController@edit');
    Route::post('codes/update/{id}', $admin . 'CodesController@update');
    Route::get('codes/delete/{id}', $admin . 'CodesController@delete');

    Route::get('games', $admin . 'GamesController@index');
    Route::get('games/listing', $admin . 'GamesController@listing');
    Route::get('games/create', $admin . 'GamesController@create');
    Route::post('games/insert', $admin . 'GamesController@insert');
    Route::get('games/edit/{id}', $admin . 'GamesController@edit');
    Route::post('games/update/{id}', $admin . 'GamesController@update');
    Route::get('games/delete/{id}', $admin . 'GamesController@delete');
    Route::get('game/for-paid/enable/{id}', $admin . 'GamesController@enable');
    Route::get('game/for-paid/disable/{id}', $admin . 'GamesController@disable');
    Route::get('game/publish/{id}', $admin . 'GamesController@publish');
    Route::get('game/unpublish/{id}', $admin . 'GamesController@unpublish');
    
    Route::get('digital-games', $admin . 'DigitalGamesController@index');
    Route::get('digital-games/download/{type}', $admin . 'DigitalGamesController@downloadExcel');

    Route::get('digital-games/listing', $admin . 'DigitalGamesController@listing');
    Route::get('digital-game/delete/{id}', $admin . 'DigitalGamesController@delete');
    Route::get('digital-game/{id}', $admin . 'DigitalGamesController@gameDetail');
    Route::get('digital-game/image/{id}', $admin . 'DigitalGamesController@image');
    Route::post('digital-game/saveimage/{id}', $admin . 'DigitalGamesController@saveimage');
    Route::get('digital-game/playcode/enable/{id}', $admin . 'DigitalGamesController@enablePlayCode');
    Route::get('digital-game/playcode/disable/{id}', $admin . 'DigitalGamesController@disablePlayCode');
    Route::get('digital-game/featured/enable/{id}', $admin . 'DigitalGamesController@enableFeatured');
    Route::get('digital-game/featured/disable/{id}', $admin . 'DigitalGamesController@disableFeatured');
    Route::get('digital-game/set/featured/update', $admin . 'DigitalGamesController@featuredUpdate');
    Route::get('digital-game/set/featured', $admin . 'DigitalGamesController@featured');
    Route::get('outside-games', $admin . 'OutsideGamesController@index');
    Route::get('outside-games/listing', $admin . 'OutsideGamesController@listing');
    Route::get('outside-game/create', $admin . 'OutsideGamesController@create');
    Route::post('outside-game/insert', $admin . 'OutsideGamesController@insert');
    Route::get('outside-game/edit/{id}', $admin . 'OutsideGamesController@edit');
    Route::post('outside-game/update/{id}', $admin . 'OutsideGamesController@update');
    Route::get('outside-game/delete/{id}', $admin . 'OutsideGamesController@delete');
    //Route::get('outside-game/{id}', $admin . 'OutsideGamesController@gameDetail');

    Route::get('tools', $admin . 'ToolsController@index');
    Route::get('tools/create', $admin . 'ToolsController@create');
    Route::post('tools/insert', $admin . 'ToolsController@insert');
    Route::get('tools/edit/{id}', $admin . 'ToolsController@edit');
    Route::post('tools/update/{id}', $admin . 'ToolsController@update');
    Route::get('tools/delete/{id}', $admin . 'ToolsController@delete');

    Route::get('uploads', $admin . 'UploadsController@index');
    Route::get('uploads/create', $admin . 'UploadsController@create');
    Route::post('uploads/insert', $admin . 'UploadsController@insert');
    Route::get('uploads/delete/{id}', $admin . 'UploadsController@delete');
    
    Route::get('searchlogs', $admin . 'SearchLogsController@index');
    Route::get('searchlogs/delete/{id}', $admin . 'SearchLogsController@delete');
    
}
);
