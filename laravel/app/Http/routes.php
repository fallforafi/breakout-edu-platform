<?php
Route::get('/', 'HomeController@index');
Route::get('track', 'HomeController@track');
Route::get('home', 'HomeController@index');
Route::get('about-us', 'HomeController@aboutus');
Route::get('contact-us', 'ContactusController@index');
Route::get('terms', 'HomeController@terms');
Route::get('share/{id}', 'HomeController@share');
Route::get('get-games', 'HomeController@getgames');

Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController']);
Route::post('contact-send', 'ContactusController@store');
Route::get('page/{code}', 'PageController@view');

Route::get('categories', 'HomeController@categories');
Route::get('featured', 'HomeController@featured');

Route::get('search', 'SearchController@index');
Route::get('query', 'SearchController@submit');

Route::get('category/{key}', 'CategoriesController@category');
Route::get('game/{key}', 'GamesController@game');
Route::get('non-paid-user', 'GamesController@error');

Route::get('forgot-password', 'SignupController@forgot_password');
Route::post('reset', 'SignupController@reset_password');
Route::get('page/{code}', 'PageController@view');
Route::get('register/success/{id}', 'SignupController@success');
Route::get('register/verify/{confirmation_code}', 'SignupController@confirmEmail');

Route::get('dashboard', 'CustomersController@index');
Route::get('changepassword', 'CustomersController@changepassword');
Route::post('postchangepassword', 'CustomersController@postchangepassword');
Route::get('profile/edit', 'CustomersController@editprofile');
Route::get('profile', 'CustomersController@profile');
Route::post('updateprofile', 'CustomersController@updateprofile');

Route::get('login', 'SignupController@login');
Route::get('signup', 'SignupController@signup');
Route::post('register', 'SignupController@store');
Route::get('twitter/login', 'SignupController@twitter');
Route::get('twitter/callback', 'SignupController@twittercallback');

Route::post('postLogin', 'SignupController@postLogin');
Route::get('fblogin', 'SignupController@fblogin');
Route::get('signupform', 'SignupController@signupform');
Route::get('loginform', 'SignupController@loginform');

Route::get('accesscode', 'CodesController@index');
Route::post('checkcode', 'CodesController@check');

Route::get('create-digital-game', 'MiniGamesController@create');
Route::post('game/save', 'MiniGamesController@save');
Route::post('game/update/{id}', 'MiniGamesController@update');
Route::get('choose-lock-types/{id}', 'MiniGamesController@chooselocktypes');
Route::get('get-lock-layout', 'MiniGamesController@getlocktype');
Route::get('delete-lock/{id}', 'MiniGamesController@deletelock');
Route::post('game/savelocktype', 'MiniGamesController@savelocktype');
Route::get('edit-lock-layout', 'MiniGamesController@editlocktype');
Route::post('game/updatelocktype/{id}', 'MiniGamesController@updatelocktype');
Route::post('game/copy/{id}', 'MiniGamesController@copy');
Route::get('edit-digital-game/{id}', 'MiniGamesController@edit');
Route::get('digital-game-finish/{game_id}', 'MiniGamesController@finish');
Route::get('my-digital-games', 'MiniGamesController@mygames');
Route::get('my-digital-games/results/{game_id}', 'MiniGamesController@gameresults');
Route::get('game/digital/delete/{game_id}', 'MiniGamesController@delete');
Route::get('game/results-download/{game_id}', 'MiniGamesController@resultsdownload');

Route::get('check-answer', 'PlayController@checkanswer');
Route::get('game/digital/{slug}', 'PlayController@play');
Route::get('game/embed/{slug}', 'PlayController@embed');
Route::get('play-lock-layout', 'PlayController@locklayout');
Route::get('game/iframe/{slug}', 'PlayController@iframe');

Route::get('thank-you', 'PlayController@thanks');
Route::get('fail', 'PlayController@fail');
Route::get('game/digital/view/{game_id}', 'PlayController@view');
Route::get('game/digital/end/{game_id}', 'PlayController@end');
Route::post('game/digital/checkcode', 'PlayController@checkcode');
Route::get('subscription/track', 'SubscriptionController@track');

include("routes_admin.php");