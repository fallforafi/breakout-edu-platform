<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiniGames extends Model {

    protected $table = 'mini_games';

    public static function searchDigitalGames($search) {

        $result = self::where("mini_games.deleted", '=', 0);

        if (isset($search['title']) && $search['title'] != "") {
            $title = $search['title'];
            $result = $result->where('mini_games.title', 'LIKE', "%" . $title . "%");
        }
        if (isset($search['gameType']) && $search['gameType'] != "") {
            $gameType = $search['gameType'];
            $result = $result->where('mini_games.gameType', '=', $gameType);
        }

        $result = $result->leftjoin('users as u', 'mini_games.user_id', '=', 'u.id')
                ->select('mini_games.id as id', 'mini_games.title as title', 'mini_games.description as description', 'mini_games.hasPlayCode as hasPlayCode', 'mini_games.playCode as playCode', 'mini_games.isFeatured as isFeatured', 'mini_games.key as key', 'mini_games.playCount as playCount', 'mini_games.time as time', 'u.email as email', 'u.firstName', 'u.lastName');

        $result = $result->orderBy('id', 'desc');

        if (isset($search['excel'])) {
            $result = $result->get();
        } else {
            $result = $result->paginate(20);
            $result->setPath('digital-games');
        }

        return $result;
    }

}
