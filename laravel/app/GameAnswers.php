<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameAnswers extends Model {

    protected $table = 'game_answers';

}
