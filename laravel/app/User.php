<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get the phone record associated with the user.
     */
    public function role() {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function bank_details() {
        return $this->hasOne('App\BankDetails');
    }

    public static function searchUser($search) {

        $result = User::where("users.deleted", '=', 0);
        $result=$result->leftJoin('roles as r', 'r.id', '=', 'users.role_id');
        $result=$result->leftJoin('user_codes as uc', 'uc.user_id', '=', 'users.id');
        $result=$result->select('users.id', 'users.firstName', 'users.lastName', 'users.email', 'users.role_id', 'users.created_at', 'r.role as role', 'users.loginCount as loginCount', 'uc.expires as expires');
        
        

        if (isset($search['firstName']) && $search['firstName'] != "") {
            $firstName = $search['firstName'];
            $result = $result->where('users.firstName', 'LIKE', "%" . $firstName . "%");
        }

        if (isset($search['lastName']) && $search['lastName'] != "") {
            $lastName = $search['lastName'];
            $result = $result->where('users.lastName', 'LIKE', "%" . $lastName . "%");
        }
        if (isset($search['email']) && $search['email'] != "") {
            $email = $search['email'];
            $result = $result->where('users.email', 'LIKE', "%" . $email . "%");
        }
        if (isset($search['role_id']) && $search['role_id'] != "") {
            $role_id = $search['role_id'];
            $result = $result->where('users.role_id', '=', $role_id);
        }

        if(isset(Auth::user()->id) && Auth::user()->id!=1)
        {
            $result=$result->where("users.role_id", '!=', 1);
        } 
        
        $result = $result->orderBy('id', 'desc');
        $result = $result->paginate(30);
        $result->setPath('users');
        return $result;
    }

}
