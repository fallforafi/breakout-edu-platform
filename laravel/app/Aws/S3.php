<?php

namespace App\Aws;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;

class S3 {

    public static function storeImage($fileName, $file, $destination, $destinationThumb = "", $width = 400, $height = 400) {
        $extension = $file->getClientOriginalExtension();
        $realPath = $file->getRealPath();
        $fileName = $fileName . '.' . $extension;
        Storage::put($destination . $fileName, file_get_contents($realPath), 'public');

        if ($destinationThumb != "") {
            $image = Image::make($realPath);
            $upload = $image->resize($width, $height)->encode($extension);
            Storage::put($destinationThumb . $fileName, (string) $upload, 'public');
        }
        return $fileName;
    }

    public static function deleteImage($fileName, $destination, $destinationThumb = "") {

        if ($fileName != "") {
            if (Storage::has($destination . $fileName)) {
                Storage::delete($destination . $fileName);
            }

            if ($destinationThumb != "") {
                if (Storage::has($destinationThumb . $fileName)) {
                    Storage::delete($destinationThumb . $fileName);
                }
            }
        }
    }
    
    public static function copy($from, $to) {
        if (Storage::has($from))
        {
            Storage::copy($from,$to);
        }
    }
}