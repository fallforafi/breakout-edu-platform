<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameResults extends Model {

    protected $table = 'game_results';

}
