<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesLocksTable extends Migration {

    public function up() {
        Schema::dropIfExists('game_locks');
        Schema::create('game_locks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->string('lockType');
            $table->string('lockSetup');
            $table->string('answers');
            $table->string('image');
            $table->string('text');
            $table->text('videoUrl');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('game_locks');
    }

}
