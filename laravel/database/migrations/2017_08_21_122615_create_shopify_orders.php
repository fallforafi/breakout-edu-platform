<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyOrders extends Migration {

    public function up() {
        Schema::dropIfExists('shopify_orders');
        Schema::create('shopify_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shopify_order_id');
            $table->integer('orderCount');
            $table->string('email');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('shopify_orders');
    }

}
