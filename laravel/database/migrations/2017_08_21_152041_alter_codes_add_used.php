<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCodesAddUsed extends Migration {

    public function up() {
        Schema::table('codes', function ($table) {
            $table->integer('isUsed')->default(0);
        });
    }

}
