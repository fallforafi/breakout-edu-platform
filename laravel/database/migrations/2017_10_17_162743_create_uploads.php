<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploads extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('uploads');
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('name');
            $table->integer('errors')->default(0);
            $table->integer('total')->default(0);
            $table->integer('import')->default(0);
            $table->integer('duplicates')->default(0);
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('uploads');
    }

}
