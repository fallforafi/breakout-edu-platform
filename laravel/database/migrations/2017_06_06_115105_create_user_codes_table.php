<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCodesTable extends Migration
{
        public function up() {
        Schema::dropIfExists('user_codes');

        Schema::create('user_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('code_id');
            $table->dateTime('expires');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('user_codes');
    }
}
