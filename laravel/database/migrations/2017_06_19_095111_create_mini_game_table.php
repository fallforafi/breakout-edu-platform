<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiniGameTable extends Migration {

    public function up() {
        Schema::dropIfExists('mini_games');
        Schema::create('mini_games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->text('description');
            $table->integer('hasTimer')->default(0);
            $table->string('time')->nullable();
            $table->string('gameType');
            $table->string('key');
            $table->integer('hasPlayCode')->default(1);
            $table->string('playCode');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('mini_games');
    }

//
}
