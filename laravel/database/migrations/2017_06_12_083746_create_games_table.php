<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration {

    public function up() {
        Schema::dropIfExists('games');

        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('infoLine1');
            $table->string('infoLine2');
            $table->string('infoLine3')->nullable();
            $table->string('infoLine4')->nullable();
            $table->string('image');
            $table->string('story');
            $table->text('lockCombinations');
            $table->text('instructions');
            $table->text('linkToFiles');

            $table->text('videoEmbed');
            $table->text('questions');
            $table->text('modifications')->nullable();
            $table->text('requirements');
            $table->text('note')->nullable();

            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('games');
    }

}
