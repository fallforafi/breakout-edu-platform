<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTable extends Migration {

    public function up() {
        Schema::dropIfExists('library');
        Schema::create('library', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->string('user_id');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('library');
    }

}
