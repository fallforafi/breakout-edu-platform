<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameAnswers extends Migration {

    public function up() {
        Schema::dropIfExists('game_answers');
        Schema::create('game_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('result_id');
            $table->integer('lock_id');
            $table->string('answer')->nullable();
            $table->string('correctAnswer')->nullable();
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('game_answers');
    }

}
