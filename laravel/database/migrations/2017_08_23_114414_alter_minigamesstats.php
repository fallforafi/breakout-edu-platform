<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMinigamesstats extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::table('mini_games', function ($table) {
            $table->integer('playCount')->default(0)->nullable();
        });
        
        
    }

    public function down() {
        //
    }

}
