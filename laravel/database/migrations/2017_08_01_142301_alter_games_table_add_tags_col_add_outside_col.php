<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGamesTableAddTagsColAddOutsideCol extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('games', function (Blueprint $table) {
            $table->tinyInteger('outside')->default(0);
            $table->string('tags')->nullable();

            $table->string('title')->nullable()->change();
            $table->string('infoLine1')->nullable()->change();
            $table->string('infoLine2')->nullable()->change();
            $table->string('infoLine3')->nullable()->change();
            $table->string('infoLine4')->nullable()->change();
            $table->text('story')->nullable()->change();
            $table->text('lockCombinations')->nullable()->change();
            $table->text('instructions')->nullable()->change();
            $table->text('linkToFiles')->nullable()->change();
            $table->text('videoEmbed')->nullable()->change();
            $table->text('questions')->nullable()->change();
            $table->text('requirements')->nullable()->change();
            $table->text('note')->nullable()->change();
            $table->string('image')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
